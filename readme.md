# Budapest type theory group

Part of Department of Programming Languages and Compilers, [Faculty of Informatics](https://www.inf.elte.hu), [Eötvös Loránd University](https://www.elte.hu).

## Members

* Viktor Bense PhD student
* [Rafaël Bocquet](https://rafaelbocquet.gitlab.io) PhD student
* Viktor Csimma BSc student
* [Ambrus Kaposi](https://akaposi.github.io) associate professor
* Péter Zsolt Korpa MSc student
* Márton Petes MSc student
* Bálint Bence Török BSc student
* Szumi Xie PhD student
* Zhenyun Yin BSc student

## Past members

* [András Kovács](https://andraskovacs.github.io) was PhD student and lecturer, now postdoc in Gothenburg
* István Donkó PhD student, now in industry
* Tamás Végh PhD student, now in industry
* Bálint Kocsis BSc student, now PhD student in Nijmegen
* Márk Széles BSc student, now PhD student in Nijmegen
* [Nicolai Kraus](https://nicolaikraus.github.io) postdoc, now professor in Nottingham
* [Péter Diviánszky](https://people.inf.elte.hu/divip) assistant professor, now in industry
* [Péter Podlovics](https://github.com/Anabra) MSc student, now in industry
* Ádám Abonyi-Tóth BSc student, now in industry

## Research areas

* Metatheory of Martin-Löf type theory
* New type theories and type systems
* Classes of inductive types and translations between them, generalised algebraic theories, higher order abstract syntax
* Implementations of type theory
* Homotopy type theory
* Formalisation in type theory
* Dependently typed programming


# Budapest type theory seminar

Everyone is welcome!

Time: every Thursday 12:00--14:00 Budapest time, [ELTE Lágymányos South building](http://umap.openstreetmap.fr/en/map/lagymanyos-south-building-elte-deli-tomb_83451#16/47.4725/19.0626) room 2-512.

Sometimes it is a joint (online) meeting with the Nottingham type theory café. E.g. we had joint online meetings between 2021-02-15 and 2021-06-09.

Language: English by default. Hungarian only if every participant speaks Hungarian.

[Greenboard photos](https://goo.gl/photos/iUg6fMv5woJ6vU3X6) (links to specific dates below).

[Mailing list](https://groups.google.com/forum/#!forum/tipuselmelet) - here we discuss and announce the topics of the seminars. Write an email to Ambrus if you want to be added.

### Recommended literature

* [Bocquet--Kaposi--Kovács. Introduction to the metatheory of type theory seminar series](https://www.youtube.com/playlist?list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU) (videos, notes: 
[1](notes/seminar20200915_TT-intro-01-basics_of_dependent_type_theory.txt)
[2](notes/seminar20200922_TT-intro-02-cwf.txt)
[3](notes/seminar20200929_TT-intro-03-standard_model.agda)
[4](notes/seminar20201006_TT-intro-04-ordinals.txt)
[5](notes/seminar20201013_TT-intro-05-identity_types.agda)
[6](notes/seminar20201020_TT-intro-06-family_model.agda)
[7exe](notes/seminar20201027_TT-intro-07-exercises.txt)
[8](notes/seminar20201103_TT-intro-08-presheaf_models.txt)
[8b](notes/seminar20201103_TT-intro-08-presheaf_models_extra_notes.txt)
[9](notes/seminar20201110_TT-intro-09-yoneda_embedding.txt)
[10](notes/seminar20201117_TT-intro-10-two_level_type_theory.txt)
[11](notes/seminar20201124_TT-intro-11-metatheoretic_properties.txt)
[12](notes/seminar20201201_TT-intro-12-normalisation.txt)
[13](notes/seminar20201208_TT-intro-13-groupoid-model.txt))
* [Ambrus Kaposi. Type systems course notes](https://bitbucket.org/akaposi/typesystems/raw/master/src/main.pdf)
* [Thorsten Altenkirch. Naive Type Theory](http://www.cs.nott.ac.uk/~psztxa/publ/fomus19.pdf)
* [Homotopy Type Theory book](http://saunders.phil.cmu.edu/book/hott-online.pdf) (especially Chapter 1 Type theory)
* [Egbert Rijke. Introduction to Homotopy Type Theory](https://arxiv.org/pdf/2212.11082.pdf)
* [Mike Shulman. Homotopy type theory: the logic of space](https://arxiv.org/pdf/1703.03007.pdf)
* [Martin Hofmann. Syntax and Semantics of Dependent Types](https://www.tcs.ifi.lmu.de/mitarbeiter/martin-hofmann/pdfs/syntaxandsemanticsof-dependenttypes.pdf)
* [Kaposi Ambrus. Bevezetés a homotópia-típuselméletbe](https://akaposi.github.io/hott_bevezeto.pdf) (magyar)
* [Philip Wadler. Programming Language Foundations in Agda](https://plfa.github.io/)
* [Benjamin C. Pierce et al. Software Foundations](https://softwarefoundations.cis.upenn.edu/)
* [Adam Chlipala. Certified Programming with Dependent Types](http://adam.chlipala.net/cpdt/)

### Proof assistants

* [Agda](https://wiki.portal.chalmers.se/agda/)
* [Coq](https://coq.inria.fr/)
* [Lean](https://leanprover.github.io/)
* [Idris](https://www.idris-lang.org/)
* [Isabelle/HOL](https://isabelle.in.tum.de/)

### Schedule

* 2025-??-?? Awodey book 7 Functors and naturality (Janet)
* 2025-??-?? Awodey book 6 Exponentials (Gábor)
* 2025-04-03 
* 2025-03-27 
* 2025-03-20 Péter Diviánszky. The Csip compiler
* 2025-03-17 14.00-16.00 Room 0-817 (Dudich Endre). Sergei Soloviev. Asymmetry in Semantic Games. [Abstract](notes/seminar20250317_sergei.txt)
* 2025-03-13 Awodey book 5 Limits and colimits (Szumi)
* 2025-03-06 Ambrus Kaposi. Type theory vs. set theory
* 2025-02-27 Ambrus Kaposi. Prefascist strictification of CwFs.
* 2025-02-20 Ambrus Kaposi. Call by value lambda calculus algebraically. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOG7Mi-qnede3HcHHd_KHb3rRR4zncmbBBqwqHO?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2025-02-13 Awodey book 4c Groups and categories (Viktor). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNMjTAB7CAiPuc07H3pde_cterco5eiLK32WAyk?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)   
  Category without products with exponentials. [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMKc5I9fgBprTEdPDNUSm6olP7XG3Uo7NZXGIlg?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2025-01-13 Szumi Xie. Second-order generalized algebraic theories. [Abstract](https://conferences.renyi.hu/uploads/conference/46/Szumi_Xie_abstract.pdf)   
  Ambrus Kaposi. Type theory as a SOGAT. [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPGUPFmZgKMUBqODymR5nFekU7aypEiDXJsZpJt?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2025-01-06 András Kovács. Runtime code generation with dependent types. [Github repo](https://github.com/AndrasKovacs/dtt-rtcg)
* 2024-12-20 Christmas type theory workshop   
  Location: D-1-817   
  Likely program:   
  10.00--10.45 Szumi Xie: Tutorial on filling squares in Cubial Agda. [Agda file](notes/seminar20241220_szumi.agda)   
  10.45--11.30 András Kovács: Yoneda LF. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20241220_kovacs_andras.pdf)   
  11.30--12.15 Márk Széles: Introduction to string diagrams for probability. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20241220_szeles_mark.pdf)   
  12.15--15.00 lunch at [Science Park Étterem](https://maps.app.goo.gl/ZxgYjfz5DSu5csQ49), then coffee at [TTK Étterem](https://maps.app.goo.gl/kwRGeZPJeXaAsWS48)   
  15.00--15.45 Bálint Kocsis: Complete Test Suites for Automata in Monoidal Closed Categories. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20241220_kocsis_balint.pdf)   
  15.45--16.30 Péter Diviánszky: Bidirectional parsing of Agda-like syntax. [Slides](notes/seminar20241220_divip.txt)   
* 2024-12-09 Awodey book 4b Groups and categories (Viktor)
* 2024-12-02 Awodey book 4a Groups and categories (Viktor). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipP0mAjm77_2lAtazoy5kheKT7rUIs3yNwi8BGTm?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-11-25 Awodey book 3c Duality (Ádám). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipN5FtqnwBEBKdYfmiL2LeAf7iPpKIkZLDEo4t9-?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-11-18 Awodey book 3b Duality (Ádám). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipN8f8bYNkGdQZuVVTvG5vT-i9Mv5STHsMliz-Ey?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-11-11 Awodey book 3a Duality (Ádám). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPLax_srejj0OHubQTWikkxRydVvn1rr-YXXSOH?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-11-04 Awodey book 2 Abstract structures (Gergő). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOTVyoY-Hun-rdevFPhlLGKtpOi-iiyzVGKBCEO?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-10-21 Awodey book 1b Categories (Ambrus). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMKq6B5Hggjfwc8OOvqkHCWyGmRdnLGKBNWrzSy?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-10-14 Awodey book 1a Categories (Peti). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNaobX85byd_JQAweEGxJlXOIrRvENGyHygBimW?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-10-07 Ambrus Kaposi. SOGAT -> GAT translation. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOQzMiRr6L583IXJTnbq7msL2eLm08T6mpw-mvQ?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://drops.dagstuhl.de/storage/00lipics/lipics-vol299-fscd2024/LIPIcs.FSCD.2024.10/LIPIcs.FSCD.2024.10.pdf)
* 2024-09-30 Balázs Kőműves. Succinct proofs of arbitrary computations. [Slides](https://raw.githubusercontent.com/bkomuves/slides/refs/heads/master/computer_science/zkvm_talk_2024.pdf)
* 2024-09-23 Szumi Xie. Type theory with implicit complexity. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMXt7c1MUAR5mc57II1Kk3NokVFyHLI5C54nf6m?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-09-16 Zoltán Kocsis. The Probability of Excluded Middle. [Abstract](notes/seminar20240916_kocsis_abstract.txt) | [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMSu-VYH0SeW1UoBrW_R0Ey5Z_LJR1YcBXfOQg2?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://arxiv.org/pdf/2110.11515)
* 2024-09-09 Ambrus Kaposi. CCC és STLC ekvivalenciája. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOEHKMv7WpXBhJfeT5NggxYqFLVGDz0t8kiAcZ_?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-09-02 Loïc Pujet. A new universe construction for setoids. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240902_loic_setoid_universe.pdf)
* 2024-08-26 René Gazzari. A New Approach to Constructive Equality. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240828_rene_gazzari.pdf)
* 2024-07-24 András Kovács. Stream fusion using two-level type theory. [Paper](http://andraskovacs.github.io/pdfs/2ltt_icfp24.pdf) | [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPfxMFbhLQHlvfD0utcQz306ZSn81jMVNZvQn_V?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2024-07-01 Lorenzo Sauras. On the representation of Kalmár functions as arithmetic terms. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240701_lorenzo_sauras.pdf)
* 2024-06-24 Jacob Neumann. Directed type theory. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240627_jacob_directed_tt.pdf)
* 2024-06-17 Thorsten Altenkirch. Higher containers. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240617_thorsten_higher_containers.pdf)
* 2024-06-10 (no seminar, TYPES in Copenhagen)
* 2024-06-03 Pre-TYPES talks.  
  Viktor Bense: Strict syntax of type theory via alpha-normalisation   
  Ambrus Kaposi: Internal relational parametricity, without an interval   
  Szumi Xie: Type theory in type theory using single substitutions
* 2024-05-27 
* 2024-05-23 Gergely Székely. Logic for relativity theory. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240523.pdf)
* 2024-05-16 Ambrus Kaposi. Single substitutions. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240516_ambrus_single_substitution.pdf)
* 2024-05-09 Ambrus Kaposi. Metaprogramming through two-level type theory. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240509_2ltt_metaprog.pdf)
* 2024-05-02 Szumi Xie. Normalisation by evaluation vs. big-step normalisation. [Agda file](notes/seminar20240502_szumi_nbe.agda)
* 2024-04-25 Bálint Kocsis. Normalisation and gluing. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240425_balint_kocsis_normalisation.pdf)
* 2024-04-18 Gábor Sági. A quick survey of the basics of algebraic logic -- part 1. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240418_gabor_sagi_algebraic_logic.pdf)  
  Zalán Molnár. A quick survey of the basics of algebraic logic -- part 2. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240418_zalan_molnar_algebraic_logic.pdf)
* 2024-04-11 (no seminar, MGS)
* 2024-03-21 Szumi Xie. Optics in Haskell. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240321_szumi_optics.pdf)
* 2024-03-14 Rafaël Bocquet. The cubical set model using internal techniques. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240314_rafael_cubical.pdf)
* 2024-03-07 Szumi Xie. SOGAT -> GAT translation. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240307_szumi_sogat.pdf) | [Paper](https://drops.dagstuhl.de/storage/00lipics/lipics-vol299-fscd2024/LIPIcs.FSCD.2024.10/LIPIcs.FSCD.2024.10.pdf)
* 2024-02-29 no seminar, at 17.30, Rafaël gives HoTTeST seminar, see [here](https://www.math.uwo.ca/faculty/kapulkin/seminars/hottest.html).
* 2024-02-22 Balázs Kőműves. Ellipses and elliptic curves, what's the deal. [Slides](https://raw.githubusercontent.com/bkomuves/slides/master/mathematics/elliptic_curves_2024.pdf)
* 2024-02-15 Discussing schedule of seminars and W-types. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240215_w_types.pdf)
* 2024-01-12 [World Logic Day in Pécs](https://conferences.renyi.hu/wld6)
* 2024-01-10 January type theory workshop  
  Location: D-1-817  
  Program:  
  10.15--11.00 Rafael Bocquet: Normalisation for a theory with definitional groups [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240110_rafael_normalisation_for_strict_monoids.pdf)  
  11.00--11.45 Viktor Bense: How to write Agda code  
  12.15--14.00 lunch at [Science Park Étterem](https://maps.app.goo.gl/ZxgYjfz5DSu5csQ49)  
  14.00--14.45 Ambrus Kaposi: Internal parametricity, without an interval [Paper](https://dl.acm.org/doi/pdf/10.1145/3632920)  
  14.45--15.30 Márk Széles: Towards a probabilistic logic with updating [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240110_szeles_mark_probabilistic_logic.pdf)  
  15.30--15.45 Szumi Xie: Normalisation for a type theory with impredicative Set [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20240110_szumi_impredicative_set.pdf)  
* 2023-12-14 PhD defence of Richárd Szalay, room D-2-214
* 2023-12-07 Presheaf models, law of excluded middle, prefascist sets
* 2023-11-30 QIIRTs
* 2023-11-23 Csimma Viktor. Creating an agda2hs-compatible representation of Krebbers–Spitters reals
* 2023-11-16 Kovács András. TBA
* 2023-11-09 [Riehl book](https://emilyriehl.github.io/files/context.pdf) Section 1.4
* 2023-10-19 [Riehl book](https://emilyriehl.github.io/files/context.pdf) Section 1.3
* 2023-09-28 [Riehl book](https://emilyriehl.github.io/files/context.pdf) Section 1.2
* 2023-09-21 [Riehl book](https://emilyriehl.github.io/files/context.pdf) Section 1.1
* 2023-09-14 Substitution normal forms. [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPAXH-jZk_EFahZZK-_pN8F8KSk0hi5OCbrA1y1?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2023-08-23 Márk Széles. A synthetic approach to probability theory. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20230823_szeles_mark.pdf)
* 2023-08-16 Ambrus Kaposi. A type theory with internal parametricity. [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMzLQzvu15EQBjLr6A5Yt5sbgyJow9RJMlaFeSe?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2023-08-02 
* 2023-07-12 Ambrus Kaposi. Internal parametrcity, without an interval. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOhfPWjqR7rARwlIzZRF8RAoVYV_gTDAERRFU59?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://arxiv.org/pdf/2307.06448)
* 2023-06-28 FSCD preparation talks [Paper 1](https://drops.dagstuhl.de/opus/volltexte/2023/18002/pdf/LIPIcs-FSCD-2023-18.pdf) | [Paper 2](https://drops.dagstuhl.de/opus/volltexte/2023/18008/pdf/LIPIcs-FSCD-2023-24.pdf) | [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20230628_fscd_prep.pdf)
* 2023-06-21 Thorsten Altenkirch. Bushes and higher containers. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20230621_thorsten_bushes.pdf)
* 2023-06-14 (no seminar, TYPES in Valencia)
* 2023-06-07 Canonicity proofs for type theory
* 2023-05-31 Random discussion
* 2023-05-24 Yorgo Chamoun. On automata theory
* 2023-05-17 András Cubical test presentation
* 2023-05-10 
* 2023-05-03 Diviánszky Péter. A type checker for dependent types
* 2023-04-19 Mátyás Szokoli. Erlang type inference
* 2023-04-12 Rafaël Bocquet. Sconing test presentation (half an hour)
* 2023-04-05 András: II.15. Image factorisation [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-03-29 András: II.14. Propositional Truncation [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-03-22 Ambrus: II.13. Funext [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-03-16 Rafaël: II.12. H-levels [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20230316_h-levels.pdf) | [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-03-09 András: II.11. Fundamental theorem of Id type [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-03-02 Ambrus. Single substitution calculus
* 2023-02-23 Rafaël: II.10. Contractible types [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-02-16 Borka: II.9. Equivalence [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-02-09 Higher equalities [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20230207_higher_equalities.pdf)  
  Egbert Rijke's book, section I.6 [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-02-02 Plan talks at department workshop  
* 2023-01-26 Egbert Rijke's book, section I.5 [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20230126_id.pdf) | [Book](https://arxiv.org/pdf/2212.11082.pdf)
* 2023-01-19 Plans for next semester; propositional proof irrelevance for setoid hell QIITs. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20230119_plans.pdf)
* 2023-01-12 András Kovács. Theory of Signatures for higher inductive inductive recursive types. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMRiNFtyfI4IqmQv3-_rBjLKNgRLm9J45zk0bCx?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2023-01-05 Type theory and first order logic as second order generalised algebraic theories. [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNwBUt93Wj1ANuFnUh8LkduB_6AWe9UIbZSjNaK?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2022-12-15 Ambrus Kaposi. Reedy fibrant presheaves
* 2022-12-09 András Kovács PhD defence
* 2022-12-05 Discussion about setoid type theory metatheory
* 2022-11-28 Rafaël Bocquet. Relative induction principles
* 2022-11-21 Balázs Kőműves. Introduction to Zero-Knowledge Proof systems. [Slides](https://github.com/bkomuves/slides/blob/master/computer_science/zk_proofs_intro_2022.pdf)
* 2022-11-14 Tamás Végh. Combinators. [Formalisation](https://bitbucket.org/akaposi/combinator)
* 2022-11-07 Rafaël Bocquet. External univalence
* 2022-10-28 SeTT Implementation Day. [Github project](https://github.com/AndrasKovacs/sett)
* 2022-10-24 András Kovács. Theory of Signatures. [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPe4eo7eoQZcPAvlkxBxsfZ6ODMX6MezmGychak?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2022-10-17 Marcin Grzybowski. Visualizing compositions from Cubical Agda | [Github account](https://github.com/marcinjangrzybowski)
* 2022-10-10 Ambrus Kaposi. QIIRTs. [Notes](notes/seminar20221010_qiirt.txt) | [Agda file](notes/seminar20221010_qiirt.txt)
* 2022-10-03 Rafaël Bocquet. W-Types. [Notes](notes/seminar20221003.agda)
* 2022-09-26 István Donkó. Introduction to CwF with Bool,Π
* 2022-09-19 Topics for the semester. [Notes](notes/seminar20220919_topics.txt)
* 2022-09-12 Canonicity for type theory. [Notes](notes/seminar20220912_canonicity.txt)
* 2022-09-06 SeTT implementation day. [Github project](https://github.com/AndrasKovacs/sett)
* 2022-08-29 SeTT implementation day. [Github project](https://github.com/AndrasKovacs/sett)
* 2022-08-17 François-René ÐVB Rideau. Object Orientation, Functionally; First class implementations. [Paper](http://fare.tunes.org/files/cs/poof.pdf) | [Paper sources and video](https://github.com/metareflection/poof) | [Faré's blog](https://ngnghm.github.io) | [Faré's thesis draft](http://fare.tunes.org/tmp/phd/thesis.pdf)
* 2022-08-01 SeTT implementation day. [Github project](https://github.com/AndrasKovacs/sett)
* 2022-07-18 András Kovács. Staging and partially static data. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20220719_staging_partially_static.pdf)
* 2022-07-15 SeTT implementation day [Github project](https://github.com/AndrasKovacs/sett)
* 2022-07-08 SeTT implementation day [Github project](https://github.com/AndrasKovacs/sett)
* 2022-06-13 Ambrus Kaposi. Strictification of a category. [Digital whiteboard](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20220613_strictification_of_a_category.pdf)
* 2022-05-30 Studying category theory. [Thorsten's notes](http://www.cs.nott.ac.uk/~psztxa/mgs.2019) and [slides](https://www.cs.nott.ac.uk/~psztxa/mgs.2009/cats.pdf)
* 2022-05-23 Meeting with András Simon and Zoltán Molnár at BME
* 2022-05-16 Thorsten Altenkirch. The axiom of choice and variations. [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipM2pShbYoCORv4Px8SHxuLVsXdjOXDoymShPpHq?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2022-05-09 Ambrus Kaposi. Combinator logic and lambda calculus.
* 2022-05-02 Studying category theory. [Thorsten's notes](http://www.cs.nott.ac.uk/~psztxa/mgs.2019) and [slides](https://www.cs.nott.ac.uk/~psztxa/mgs.2009/cats.pdf)
* 2022-04-11 Studying category theory. [Thorsten's notes](http://www.cs.nott.ac.uk/~psztxa/mgs.2019) and [slides](https://www.cs.nott.ac.uk/~psztxa/mgs.2009/cats.pdf)
* 2022-03-28 
* 2022-03-21 List of possible topics. The axiom of choice in type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMZ-UX83s8sLQBrLCnF353u-SUyb1MwJGqcIRcP?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2022-03-14 
* 2022-03-07 Ambrus Kaposi. Higher observational type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNKCuR971hZoTFM-RRs-dRo2K5pzUcdMjptf5pZ?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2022-03-02 Ulrik Buchholtz (joint meeting with the Nottingham type theory café)
* 2022-02-28 
* 2022-02-21 Ambrus Kaposi. Mutual definition of presheaves and type theory. [Notes](notes/seminar20220221.txt)
* 2022-02-14 Thorsten Altenkirch. Higher order containers. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPBuQM1YybF5AIvHgoKLx-z9KIFCG0_3kB_rY0b?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2022-02-07 András Kovács. Cubical type theory
* 2022-01-31 András Kovács. More on staged compilation
* 2022-01-26 Rafaël Bocquet. Strictification of weakly stable type-theoretic structures using generic contexts. [Notes](notes/seminar20220126.txt) | [Paper](https://arxiv.org/abs/2111.10862) (joint meeting with the Nottingham type theory café)
* 2022-01-24 András Kovács. Staged compilation and two level type theory. [Notes](notes/seminar20220124_staging_2ltt.txt)
* 2022-01-10 Discussion (online)
* 2021-12-06 Study [this](https://github.com/tcampion/Semisimplicial)
* 2021-12-01 Thorsten Altenkirch. Münchhausen type theory. [Notes](notes/seminar20211201_munchhausen_type_theory.txt) (joint meeting with the Nottingham type theory café)
* 2021-11-29 Péter Bereczky. Core Erlang formalisation [Github project](https://github.com/harp-project)
* 2021-11-08 Ambrus Kaposi. Inductive types: injectivity, disjointness of constructors, decidability of equality. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOogDZliscc6qsm45O3H3TviaY9RGf80obcGoPH?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)    
  Rafaël Bocquet. A generalisation of the local usiverses method. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPx5Lv0sN8qTjwo-ngSSCAbu4CO-1u8a8tvMdYa?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2021-10-27 Ambrus Kaposi. Relative displayed models. [Notes](notes/seminar20211027_relative_displayed_models.txt) (joint meeting with the Nottingham type theory café)
* 2021-10-25 Péter Diviánszky. A dependently typed assembly language. [Website](https://divip.hu)
* 2021-10-18 Guilherme Silva. Formalising Bitcoin in Agda. [Github repo](https://github.com/guilhermehas/crypto-agda), [another Github repo](https://github.com/guilhermehas/crypto-agda-unified)
* 2021-09-27 András Kovács. Simply typed CwF. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPF0DJSpAmccaqE4JexNQyvX87QujVN4mUpm6YT?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2021-09-20 Rafaël Bocquet. Coherence theorems for type theory
* 2021-09-13 Ambrus Kaposi. Normalisation for SK combinator calculus. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNg20F4z20Em1sj5z2ha_zqez7oSRyBSjqRisp7?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Notes](notes/seminar20210914_sk_norm.txt)
* 2021-09-06 Discussion of topics for this semester. Ambrus Kaposi. Levels of abstraction when defining programming languages. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNR3hbRQRp8l51-72uXxOSPwQETo4wmMLxaNuPa?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2021-06-18 [Developments in Computer Science conference](http://dcs.elte.hu), session on type theory:    
  Invited talk. Nicolai Kraus. Wellfounded and Extensional Ordinals in Homotopy Type Theory. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/dcs_nicolai.pdf)    
  Rafaël Bocquet. Univalent transport in type theory. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/dcs_rafael.pdf)    
  István Donkó. Properties of Setoid Type Theory. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/dcs_istvan.pdf)    
  András Kovács. Staged Compilation and Generativity. [Notes](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/dcs_andras.txt)   
  Zongpu Xie. A model of type theory supporting quotient inductive-inductive types. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/dcs_szumi.pdf)    
* 2021-06-09 Thorsten Altenkirch. A telescope calculus. [Notes](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20210609_thorsten_telescope_calculus.pdf)
* 2021-06-02 
* 2021-05-26 Nicolai Kraus. Riehl-Shulman type theory for infinity categories. [Notes](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20210525_riehl_shulman.pdf)
* 2021-05-19 Discussing topics for Agda Implementors Meeting. [List](notes/seminar20210519_aim_list.txt)
* 2021-05-12 
* 2021-05-05 Egbert Rijke. New universal properties of the truncations
* 2021-04-28 Discussion on e.g. free algebraic structures. [Notes](notes/seminar20210428_carrier.txt)
* 2021-04-21 Hugo Moeneclaey. Parametricity and Semi-Cubical Types
* 2021-04-14
* 2021-04-07 Thorsten Altenkirch. The CwF of containers
* 2021-03-31 Fredrik Nordvall Forsberg. Quantitative type theory. [Notes](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20210331_quantitative.pdf)
* 2021-03-24 Filippo Sestini. Reducing infinitary inductive-inductive types to inductive types.
* 2021-03-17 
* 2021-03-10 Rafaël Bocquet. Induction principles for type theories, internally to presheaf categories. [Notes](notes/seminar20210310_induction_principles.txt) | [Paper](https://arxiv.org/pdf/2102.11649.pdf)
* 2021-03-03 András Kovács. Universes. [Notes](notes/seminar20210303_universes.agda) | [Paper](https://arxiv.org/pdf/2103.00223.pdf)
* 2021-02-15 Thorsten Altenkirch. A new theory of signatures for describing inductive-inductive types.
* 2021-02-08 Brandon Hewer. An efficient universe of finite sets in Agda. [Github repo](https://github.com/brandonhewer/Operads-HoTT)
* 2021-02-01 New ideas about QIITs. [Whiteboard](notes/seminar20210201.png)
* 2021-01-25 Rafaël Bocquet. Metatheoretic proofs internally to presheaf categories [Notes](notes/seminar20210125.txt)
* 2021-01-11 András Kovács. What is a type theory with inductive-inductive types? [Notes](notes/seminar20210111.txt)
* 2020-12-10 András Kovács. Substitutions, renamings, weakenings
* 2020-12-08 Rafaël Bocquet. TT intro 13 groupoid model. [Video](https://www.youtube.com/watch?v=AyMMAHaHyFc&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=10&t=1023s) | [Notes](notes/seminar20201208_TT-intro-13-groupoid-model.txt)
* 2020-12-01 András Kovács. TT intro 12 normalisation. [Video](https://www.youtube.com/watch?v=jEDZz6yFXCM&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=9) | [Notes](notes/seminar20201201_TT-intro-12-normalisation.txt)
* 2020-11-24 Ambrus Kaposi. TT intro 11 metatheoretic properties. [Video](https://www.youtube.com/watch?v=xWMTMN6CVhc&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=8) | [Notes](notes/seminar20201124_TT-intro-11-metatheoretic_properties.txt)
* 2020-11-19 Gregory Morse. Snake in Agda. [Github repo](https://github.com/GregoryMorse/agda-snake)
* 2020-11-17 Rafaël Bocquet. TT intro 10 two level type theory. [Video](https://www.youtube.com/watch?v=oKjLn2McXqU&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=7) | [Notes](notes/seminar20201117_TT-intro-10-two_level_type_theory.txt)
* 2020-11-10 András Kovács. TT intro 09 Yoneda embedding. [Video](https://www.youtube.com/watch?v=a3Wy4g1SDNc&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=8) | [Notes](notes/seminar20201110_TT-intro-09-yoneda_embedding.txt)
* 2020-11-05 András Kovács. Universes
* 2020-11-03 Ambrus Kaposi. TT intro 08 presheaf models. [Video](https://www.youtube.com/watch?v=LUJwtvRECe4&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=6) | [Notes](notes/seminar20201103_TT-intro-08-presheaf_models.txt) | [Extra notes](notes/seminar20201103_TT-intro-08-presheaf_models_extra_notes.txt)
* 2020-10-27 Ambrus Kaposi. TT intro 07 [Exercises](notes/seminar20201027_TT-intro-07-exercises.txt)
* 2020-10-20 Rafaël Bocquet. TT intro 06 family model. [Video](https://www.youtube.com/watch?v=8JD4MgARxCo&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=5) | [Notes](notes/seminar20201020_TT-intro-06-family_model.agda)
* 2020-10-15 András Kovács. Setoid type theory. [Notes](notes/seminar20201015.txt)
* 2020-10-13 András Kovács. TT intro 05 identity types. [Video](https://www.youtube.com/watch?v=f2sto4t5ynk&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=5) | [Notes](notes/seminar20201013_TT-intro-05-identity_types.agda)
* 2020-10-06 Ambrus Kaposi. TT intro 04 universes. [Video](https://www.youtube.com/watch?v=R-pA75dJrUw&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=4) | [Notes](notes/seminar20201006_TT-intro-04-ordinals.txt)
* 2020-10-01 Rafaël Bocquet. A variant of Brunerie's type theoretic definition of ∞-groupoids. [Notes](notes/seminar20201001.txt)
* 2020-09-29 Rafaël Bocquet. TT intro 03 standard model. [Video](https://www.youtube.com/watch?v=vMEF1JdCPjs&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=3) | [Notes](notes/seminar20200929_TT-intro-03-standard_model.agda)
* 2020-09-24 András Kovács. Setoid type theory implementation. [Notes](notes/seminar20200924.txt)
* 2020-09-22 András Kovács. TT intro 02 cwf. [Video](https://www.youtube.com/watch?v=IptOgpA4xms&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=2) | [Notes](notes/seminar20200922_TT-intro-02-cwf.txt)
* 2020-09-17 András Kovács. A game semantics of MLTT. [Formalisation](https://github.com/AndrasKovacs/polynomial-model/blob/master/Games.agda)
* 2020-09-15 Ambrus Kaposi. TT intro 01 basics of dependent type theory. [Video](https://www.youtube.com/watch?v=gar4Zw308aY&list=PLb_d4YUzq4lMgnFwBY4CJuHF911WWiocU&index=1) | [Notes](notes/seminar20200915_TT-intro-01-basics_of_dependent_type_theory.txt)
* 2020-07-28 András Kovács. Staged compilation and two-level type theory. [Implementation](https://github.com/AndrasKovacs/implicit-fun-elaboration/tree/staging/fcif)
* 2020-07-07 Dániel A. Nagy. Seedling, A Programming Language for Everyone and Everything. [Github repo](https://github.com/nagydani/seedling)
* 2020-06-30 Zoltán A. Kocsis. Internal set theory. [Notes txt](notes/seminar20200630.txt) | [Notes png](notes/seminar20200630.png) | [PhD thesis](https://www.research.manchester.ac.uk/portal/en/theses/development-of-group-theory-in-the-language-of-internal-set-theory(2f333784-a511-4c85-a7e2-812cc14e6067).html) | [Agda code for the thesis](https://github.com/zaklogician/agda-ist-algebra)
* 2020-06-23 András Kovács. The container model of type theory. [Agda file](notes/seminar20200623_container.agda)   
  Ambrus Kaposi. Reduction of inductive-inductive types to indexed inductive types. [Agda file](notes/seminar20200623_red.agda)
* 2020-06-16 Thorsten Altenkirch. Integers as a higher inductive type. [Paper](https://arxiv.org/pdf/2007.00167.pdf) | [LICS talk](https://www.youtube.com/watch?v=Fov95A2bGDI)   
  Paolo Capriotti. Dimensions in physics
* 2020-06-09 Logic in Agda. [Agda repo](https://bitbucket.org/akaposi/logic/)
* 2020-06-02 Discussion on random topics
* 2020-05-26 Paolo Capriotti. The simplicial set model of HoTT. [Notes](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20200526.pdf)
* 2020-05-19 András Kovács. Elaboration with first-class implicit function types. [Paper](https://github.com/AndrasKovacs/icfp20sub/raw/master/paper.pdf) | [Notes](notes/seminar20200519.txt)
* 2020-05-12 Discussion on logical predicates/relations and simplicial/cubical type theories. [Notes](notes/seminar20200512.txt)
* 2020-05-04 Rafaël Bocquet. Presentation of the paper "Multi-modal type theory". [Notes](notes/seminar20200505.txt) | [Paper1](https://jozefg.github.io/papers/multimodal-dependent-type-theory.pdf) | [Paper2](https://jozefg.github.io/papers/type-theory-a-la-mode.pdf)
* 2020-04-28 Thorsten Altenkirch. Higher categories in homotopy type theory. [Notes](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20200428.pdf)
* 2020-04-21 Rafaël Bocquet. Presentation of the paper "A Higher Structure Identity Principle" by Ahrens et al. [Notes](notes/seminar20200421.txt) | [Paper](https://arxiv.org/pdf/2004.06572.pdf)
* 2020-04-14 András Kovács. An implementation of setoid type theory. [Github repo](https://github.com/AndrasKovacs/setoidtt-proto)
* 2020-04-07 Rafaël Bocquet. Conservativity of type theory theory with strict computation rules over weak type theory. [Notes](notes/seminar20200407.txt)
* 2020-03-31 Péter Diviánszky. A very fast lambda calculus interpreter based on combinatory logic. [Haskell implementation](https://bitbucket.org/akaposi/tipuselmelet/src/master/notes/Director.hs)
* 2020-03-24 Christian Sattler. Strict presheaf models. [Paper 1](https://www.xn--pdrot-bsa.fr/articles/markov.pdf) | [Paper 2](https://pujet.fr/pdf/presheaf_translation.pdf)
* 2020-03-17 Post-TYPES talks.  
  Nicolai Kraus. An Induction Principle for Cycles. [Abstract](https://types2020.di.unito.it/abstracts/BookOfAbstractsTYPES2020.pdf#page=143) | [Slides](https://nicolaikraus.github.io/docs/nicolaikraus_munichonline.pdf)  
  Filippo Sestini. Constructing a universe for the setoid model. [Abstract](https://types2020.di.unito.it/abstracts/BookOfAbstractsTYPES2020.pdf#page=10)  
  Rafaël Bocquet. Metatheoretic proofs internally to presheaf categories. [Abstract](https://types2020.di.unito.it/abstracts/BookOfAbstractsTYPES2020.pdf#page=146)
* 2020-03-10 Ambrus Kaposi. A point-free definition of hProp and using it to define the setoid model. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOmfd37OuLuzrHX2U-2PzkxbALgd16n2YT59Fac?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2020-03-03 András Kovács. Evaluation efficiency issues in cubical type theory
* 2020-02-18 Pre-TYPES talks.  
  András Kovács. Generalizations of Quotient Inductive-Inductive Types. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNrYrmJG9d1_VcEN95Cbp-jDiu7jYc9zxvTF5Ds?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Abstract](https://types2020.di.unito.it/abstracts/BookOfAbstractsTYPES2020.pdf#page=32)  
  Zongpu Xie. A model of type theory with QIITs. [Abstract](https://types2020.di.unito.it/abstracts/BookOfAbstractsTYPES2020.pdf#page=23)  
  Ambrus Kaposi. Shallow embedding of type theory is morally correct. [Abstract](https://types2020.di.unito.it/abstracts/BookOfAbstractsTYPES2020.pdf#page=76)  
  Rafaël Bocquet. Metatheoretic proofs internally to presheaf categories. [Abstract](https://types2020.di.unito.it/abstracts/BookOfAbstractsTYPES2020.pdf#page=146)
* 2020-02-11 András Kovács. Type checking. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPUDQfF51CUbMaOUktKbmEbBCY4YlSujyG5dv24?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2020-02-03 Christian Sattler. Filtered colimits.
* 2020-01-27 Ádám Révész. LambdaKube: kubernetes based cloud native functional programming language design and implementation with tools.
* 2020-01-20 
* 2020-01-13 Ambrus Kaposi. Substitution laws for free! Signatures for type theories. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPukNd5xLvEQr_tyYt_Oj7QG57P5FYH0AVlNaQ4?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2020-01-07 Paolo Capriotti. Sketches and inductive types.
* 2019-12-16 András Kovács. Introduction to type theory for proof theorists (iii). [Agda file](notes/seminar20191216.agda)
* 2019-12-09 Ambrus Kaposi. Introduction to type theory for proof theorists (ii). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMM9AvYaP3RdrytK0FRc2tpAxmYGCAhSucFmBYe?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-12-02 Derivation of Frobenius J from J. [Formalisation](https://github.com/akaposi/hiit-signatures/blob/master/formalization/FrobeniusJDeriv.agda)
* 2019-11-25 Ambrus Kaposi. Introduction to type theory for proof theorists (i). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPRpD70lQ4d-WAoebmz3U3kTArHKQ6uWiNNcyAY?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Simple type theory](https://bitbucket.org/akaposi/tipuselmelet/src/master/STT.txt) | [Dependent type theory](http://drops.dagstuhl.de/opus/volltexte/2019/10532/pdf/LIPIcs-FSCD-2019-25.pdf#page=4)
* 2019-11-18 Ambrus Kaposi. Presheaf models, showing that LEM is not provable. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMrkfucSI5ih145MuYmexdwc2n8SLuT7ECCqDUz?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-11-11 Rafaël Bocquet. Slice models of type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPMAle9kssEfH3uh-KcaV5rYMxmL91wge1IgDaX?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-11-04 András Kovács. Large numbers in type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPHgZNYV1N9vSnbuexec1lJ-uJnriqsNcDnXllt?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-10-28 Discussing "Implementing a Modal Dependent Type Theory". [Paper](http://www.jonmsterling.com/pdfs/modal-mltt.pdf)
* 2019-10-21 Rafaël Bocquet. Nonstandard models of type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPpDZ4mmAgfmsVBJa1lLiyDngizG6ct84zrDKkw?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-10-14 Discussing "A Language Feature to Unbundle Data at Will". [Paper](https://alhassy.github.io/next-700-module-systems/papers/gpce19_a_language_feature_to_unbundle_data_at_will.pdf) | [Website](https://alhassy.github.io/next-700-module-systems/prototype/package-former.html)
* 2019-10-07 András Kovács. Type inference. [Github repo](https://github.com/AndrasKovacs/elaboration-zoo/tree/master/experimental/poly-instantiation)
* 2019-09-30 Discussion about ICFP experiences
* 2019-09-23 Secret Information. Characterisation of definability for dependent type theory. 
* 2019-09-16 Revisiting definability for simple type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMB2VpYGTInAVfQC3cj7JRY94f5MSxfa2ybMyve?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-09-09 Nicolai Kraus. Higher type theory
* 2019-09-02 Ambrus Kaposi. Transformations between classes of inductive types. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipP-1QjeJ-npSKTdj9SgzhZdlfUvRD0VvXpV3QCT?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)  
  András Kovács. Extra constructions on QIITs: coproducts, natural number objects, etc. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPZHyloBLjZmuy9r52wWYKrLJBdx_XElau1D0U0?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper on basic constructions on QIITs](https://akaposi.github.io/finitaryqiit.pdf)
* 2019-08-12 Discussion on higher semi type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipM6msrxdTzx4HY6sJyH4iMHUf6EN7JoYCEe-sGy?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-07-29 Ambrus Kaposi. All inductive-inductive types (IITs) are reducible to two-sorted IITs, this also gives sort equalities. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPStnI_AF2TjyPFpMWEBKdvbK9s8PbjSGP0Bi9O?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-07-22 András Kovács. A failed construction of a coherent syntax. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOwOf1St6teSQa5ZKuP5M-Axv13YF-eyczQGsc6?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)  
  Ambrus Kaposi. Type theory without identities is enough. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPlifiWkV6_g4LzCU4w2zngc_UfCp5Ud0i-i178?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-07-15 Rafaël Bocquet. Coherence for two-level type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOAXaAgKP-Tb3j2cgGnHy4cHP5pIoANIzvmJ-vG?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-07-08 Thorsten Altenkirch. Directed type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipM8Pw50wObhDp7_7LrhfwfxYEuMEl8sFKM3WB7H?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-07-01 Joseph Helfer. First order homotopical logic
* 2019-06-24 Ambrus Kaposi. Setoidification of setoid type theory is injective. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPHkdTdW_buoMc8HyRYge-r8a_H7HByqfaGLf2o?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-06-17 Mihály Makkai. Generalised sketches. Paper [part 1](https://www.sciencedirect.com/science/article/pii/S0022404996000072), [part 2](https://www.sciencedirect.com/science/article/pii/S0022404996000084), [part 3](https://www.sciencedirect.com/science/article/pii/S0022404996000096)
* 2019-06-03 Mihály Makkai. Generalised sketches. Paper [part 1](https://www.sciencedirect.com/science/article/pii/S0022404996000072), [part 2](https://www.sciencedirect.com/science/article/pii/S0022404996000084), [part 3](https://www.sciencedirect.com/science/article/pii/S0022404996000096)
* 2019-05-27 Dániel Lukács. Linear temporal Logic. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPReA1V6pWxabnI4vycj0-Xa8J3QIMS5kUmaqg2?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-05-20 András Kovács. The category of algebras for single sorted algebraic theories without equalities. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMDi1qrmfNGAVUzXlNkpl8SVPkqhKw5eQNVmDuq?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-05-13 Nicolai Kraus. Deep and shallow embeddings of groups. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMgdJ6PW5YPOuMRUPAk71l2hjFRo8S7OfsubCdH?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-05-06 Studying "Call-by-need is clairvoyant call-by-value" by Hackett and Hutton. [Paper](http://www.cs.nott.ac.uk/~pszgmh/clairvoyant.pdf)
* 2019-04-29 Ambrus Kaposi. Shallow embedding is morally correct. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMGqggxHkqo2ZIYdjeIb0c5ZQlGGPnST702u1tr?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://bitbucket.org/akaposi/shallow/raw/master/paper/paper.pdf)
* 2019-04-15 Studying Mike Shulman's "Linear logic for constructive mathematics". [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMH5VAfM0KmtIfk93LwYfmoxNheMtqcvfCILe19?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://arxiv.org/pdf/1805.07518)
* 2019-04-08 Studying Mike Shulman's "Linear logic for constructive mathematics". [Paper](https://arxiv.org/pdf/1805.07518)
* 2019-04-01 Thorsten's directed type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNvGudzqBKS1VKbO2yvaFMp7hsY_qSvgaXL1QEi?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Some slides](http://www.cs.nott.ac.uk/~psztxa/talks/syco-3.pdf)
* 2019-03-25 András Kovács. Initiality project. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipP4aHE9KfR3AJPsIm6RryEFZognpkG6yCb6quXO?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-03-18 Open discussion including Péter Diviánszky
* 2019-03-11 Simon Boulier. Template Coq
* 2019-03-04 Gábor Lehel. Rust for Haskellers. [Slides](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/seminar20190304.pdf) | [Source of slides](notes/seminar20190304.html)
* 2019-02-25 Ambrus Kaposi. Shallow embedding is morally correct. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNLZq_9jBS-h9yZhaczViXrXceDtuFLbnqpWcXl?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-02-18 Studying Conor McBride's Outrageous but Meaningful Coincidences paper. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPbI50yARxA9jBr5zihbCaB5AEhmKBSkFWq1n-Z?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.379.3169&rep=rep1&type=pdf)
* 2019-02-11 Ambrus Kaposi. Setoid model -> translation -> type theory. Canonicity from gluing. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMYw4L2v91-4LHwhfMoTe-PUPIAK1ys4PGFMES-?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Setoid type theory paper](https://bitbucket.org/akaposi/setoid/raw/master/fscd2019.pdf) | [Gluing paper](https://bitbucket.org/akaposi/gluing/raw/master/p.pdf)
* 2019-02-04 Ambrus Kaposi. Introduction to the algebraic syntax of type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOw6ujv_b9jzNrcXQpFXG4Xwx-H52tl9eKDrzdD?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2019-01-28 Dávid Németh. Introduction to the K framework. Zsolt Tabi. Agile methods.
* 2019-01-21 Jakob von Raumer. Path Spaces of Higher Inductive Types in Homotopy Type Theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMd5CdZ-MtPbBrLbpCgE_eRE5U9Sw2u42_w6F6_?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://arxiv.org/abs/1901.06022)
* 2019-01-14 Ambrus Kaposi. Algebraic programming language theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMD9srEfvHpoV9GHb3It1nlI2gSezDZVg83vYd_?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Slides for another talk on the same topic](https://akaposi.github.io/pres_bugyi.pdf)
* 2019-01-07 Who did what in the Christmas break. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMqWXotukMQOjHp8P7chGIdDR6YGgDiUlIS7Twd?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2018-12-17 Ambrus Kaposi. Setoid type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPfGIZ8I6uXS5f60yHkKyE8eF0h_oGIzfSPcAnR?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://bitbucket.org/akaposi/setoid/raw/master/fscd2019.pdf)
* 2018-12-10 András Kovács. Efficient type checker for dependent types, part 2. [GitHub repo](https://github.com/AndrasKovacs/smalltt)
* 2018-12-03 András Kovács. Efficient type checker for dependent types, part 1. [GitHub repo](https://github.com/AndrasKovacs/smalltt)
* 2018-11-26 Finite models of type theory. [Some formalisation](https://bitbucket.org/akaposi/tt-in-tt/src/HEAD/FiniteModels)
* 2018-11-19 Ambrus Kaposi. Syntax of type theory and a trivial model. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNoduzAh4b6zJOcRzCgQUxpu6Yc4JOLg9lNz7lI?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Leaflet](https://bitbucket.org/akaposi/tipuselmelet/raw/HEAD/notes/syntax.pdf) | [Source code of leaflet](notes/syntax.tex)
* 2018-11-12 Péter Diviánszky. What is a composable interactive program? [GitHub repo](https://github.com/divipp/frp_agda)
* 2018-11-05 Péter Diviánszky. What is an interactive program? [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOEJ5cwP5v4iCemY0C87NQI79RvmOxw2qB_B1Bk?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [GitHub repo](https://github.com/divipp/frp_agda)
* 2018-10-29 Ambrus Kaposi. Setoid type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipORnMMmuHS3QKIrfUQ4V51rHyvM-QgQBp0t6AB0?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://bitbucket.org/akaposi/setoid/raw/master/fscd2019.pdf)
* 2018-10-15 Ambrus Kaposi. Canonicity for simple type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNTg9m7VWn8KwWzljcWOYQxr_fFUnpPV_BQ-Ssc?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2018-10-08 Gergő Érdi. Generic Description of Well-Scoped, Well-Typed Syntaxes. [Paper](https://arxiv.org/abs/1804.00119) | [GitHub repo](https://github.com/gergoerdi/generic-syntax)
* 2018-10-01 Recursion, induction, injectivity and disjointness of constructors. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOSnbaf-bJ0cYJ-N9glq3yBSuLczwGbB0ESDIBx?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Source code](notes/seminar20181001.agda)
* 2018-09-24 András Kovács. Agda intro to haskellers. [Source code](notes/seminar20180924.agda)
* 2018-09-17 Universe levels, sets as trees, Russell paradox, HITs. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNKl-RswAh4jMfmhtYAAoZcIiZuMxRA4afwPjQf?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2018-09-10 Intro to Lambda calculus and variants of type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOa7vlbzuhi_SnQ1RZgm8h_UXp50gZxsyKYDpqP?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2018-09-03 List of possible topics, interaction nets. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPAqjkbzvqKq0JO2qTt4gBt_mI_0m4hprWynF30?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2018-08-28 Thorsten Altenkirch. Coherent type theory
* 2018-06-26 Constructing QIITs. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNK_IQVd4yw3Lah9drEkDAvG2wpbVjVMBVa_ZvW?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://bitbucket.org/akaposi/finitaryqiit/raw/HEAD/paper.pdf) | [Bitbucket repo](https://bitbucket.org/akaposi/finitaryqiit)
* 2018-04-13 Intrinsic syntax of type theory with generalise. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNQX0JG0PlY9DCCuyyyCTo8bE_WWWnfkWvxhKYW?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2018-02-27 András Kovács. Closure conversion for dependent types. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPaMQxkxg7kmqr4LEVU3AIHKmfRiLgK_n2Zix5H?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [TYPES slides](https://www.dropbox.com/sh/shr2fpv4r08pvj0/AAAsi-JpYRdiKgtJngfk0enea/Jun20?dl=0&preview=Kovacs(abstract%235).pdf) | [TYPES abstract](https://types2018.projj.eu/wp-content/uploads/2018/06/BookOfAbstractsTYPES2018.pdf#page=56)
* 2018-02-13 András Kovács. Inductive types from functors given by I-K-etc. combinators. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNueMKGR4QBINodyatxcqGYjWZ4gjsqUTff-n4M?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-12-11 Elimination principles for inductive types with negative constructors. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMZzXSvVTGjLgPLYx7lNfOF7ZxQOOKwjIKGgnny?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-12-04 András Kovács. Elaboration. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipO1vBo6Z5X7TVqQwF3q6ywhXKyZkm_2Jq79fQHd?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-11-27 Quotient by group action. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOIJ41MjoqnjVGfvVix5j4izXJENqgOawJ-3NH3?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-11-20 Algebra morphisms for natural numbers, groups, logic, type theory.. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOv4I7mxutUjhFxTfhSfIvCKLcqNfn1FiTDrQ1Q?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-10-23 A syntax for inductive-inductive types. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPd_tXjtkzMG5jthKkUB_HQnhKdKh1Eur1aS35N?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-10-02 The meta-metatheory of inductive types. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMuSD4Hyr9Gw_vKYMP4UHEL_rqb1pOvy7KujW9l?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-09-25 Type theory intro. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOH0-9iK9yBsuvHmeSIWzL773SAze4EM5nho31R?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-09-11 András Kovács. Generalised generic trie. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMQPAMu38QZo9gNN7IyRbxAB-E9mM9wA7qyULH4?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-08-01 Circle, propositional truncation. There are no coproducts in lazy languages. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPhHyXwIzRg0bS08-E8c4XzqTLvje6bdJxqlBMT?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-06-13 Conversion checking for type theory (Coquand's algorithm). [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPtypqzmRZ8e_xIvGIQiVwefSLtudAgIGefdgK0?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-06-06 Equality reflection implies UIP and other stuff. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNMcFBjyT-MXLWZMlyaRYfI3BBmZP4GkmCroApu?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-05-22 Deriving types of elimination principles. [Photo](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPa3uiBeSmVd8_HWSNMZmCiWlzto23ZoLYQbVPl?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-05-16 Eliminator from recursor and uniqueness for natural numbers. Conversion checking for type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNQGOgNMOwOuUuj5sFXElfrvV6Ue7Skrh9jEBTZ?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-05-02 STRef. Fibrational setoid type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNlJAA2pXjzrLyWBLlqs_HPXRHjeW-SgylMCy1l?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-04-04 Deriving types of elimination principles. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMHL3MdOxiqfhccJpOkLd6Lxi5NXi0Kq2SEG12L?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-03-28 Deriving types of elimination principles. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMRnNkNcN0I9l-DBadLIo026fZWFvmArbqzr7bN?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-03-21 Deriving types of elimination principles. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMkpd1B0_8z6hvRL_kpSLm3bg5OLxz7DwZYGrnR?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-03-14 Deriving types of elimination principles. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPMTZdS8dDEcbIQ9N2fEbdXXxnNy8Q6b6Och-L4?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-03-07 Type checking for dependent types producing intrinsic syntax. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNMz9Qt78gnMNHk9sD-FlCbfaDlJ91UCLyiM1Vq?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-03-02 Deriving types of elimination principles. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOWmp3IpbpDy8ZRSeW8AuwJVEH2vztKJYvIo8ff?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-02-21 Induction-induction, induction-recursion, initial algebra semantics of inductive types, construcing initial algebra using quotients. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipO2-5tz71I3zMFd8t8ECcbRiaXhbap9DA7-K6Wa?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-02-14 Constructing initial algebras using quotients. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMFuoSMOi9z5BdcFS7YwMMJ9HujP0izUXfQq9jv?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-02-07 Minimal cubical type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOstB3VzEyHhAUPOIaA2tPtj4rkAwx2qDWhl_Pd?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-01-24 Logical predicates with universes a la Russell or Tarski? Category with families. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOwpyocCjKcJvDbjqO5bFeNI97AnNK6r5-sgL2Y?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-01-17 External and internal parametricity, cubes. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipP6_mcMCk1Ek3HUMZUZ9WFuAOU0CuARnRxcZofw?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2017-01-03 Eliminator for nested lists. Deriving types of elimination principles. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPAK77UtDLrrCRg4igD6ac2rxY771XP1xFW-YS0?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-12-20 Logical predicates. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMnPOripyAMtVnkIBf9F-rzPmqmRWvESL2UY78C?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-12-13 András Kovács. Pattern unification. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOCCl4w-so0ycaTmWm7BjNpBBxj3Fo9PHTVbgLG?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-12-06 András Kovács. Unification. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPRpp7fYPfO0lIu9r7ck7dwA_uX-MYeRGDBTfwh?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-11-15 Ambrus Kaposi. Categories, functors from positive and negative type expressions. At Bolyai Kollégium. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMNbezG3nOpHNlTBZ4GWRF0C-yvGXHPenshkETo?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-11-08 Ambrus Kaposi. Category theory and computer science. At Bolyai Kollégium. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOGyuzBksZajmLe2FmltrU44BRZfPGO1WRCvuX1?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-10-18 Péter Diviánszky. A calculus for implicit arguments. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOL_I9Bxa0GKkYdOW99K_1bfV6RqgMrhU4v63Ag?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-10-13 Ambrus Kaposi. W-types. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipP2GA1j01_gyUIv-TyEvYcRMoDwGXKcJVOZH3BX?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-10-03 Ambrus Kaposi. Normalisation for simple type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNz67QL-vA9_PlpdaOtK3p-Ke2H_B5GYappENkL?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-09-20 Yoneda lemma. Categorical gluing. Operational semantics for lazy evaluation.  [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOjYdWj6sC2X6uSsvA7qpGGdlhe8UOLuKxJ5DJY?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-09-12 Studying the CCHM paper on cubical type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipMISuI2hV3FyrJlBxPtGNDfC3g1diQvPJRmuW3r?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://arxiv.org/abs/1611.02108)
* 2016-09-05 Studying the CCHM paper on cubical type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOIvhw7V0KjqdoP6TI_MJVkSeHOa1h-dadtihwN?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://arxiv.org/abs/1611.02108)
* 2016-09-01 Studying the CCHM paper on cubical type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNsCHBbEE5Bqm-n_wfohpfSstkMEFD-JLZV2Tj4?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://arxiv.org/abs/1611.02108)
* 2016-08-22 Studying the CCHM paper on cubical type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipO4z2VQkIDnCx7T2DipBU-QMntT8oPYpRem52a7?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://arxiv.org/abs/1611.02108)
* 2016-07-04 András Kovács. Push-enter, eval-apply. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPBsgB17FIppboy0hJEwSZug5dhpsQvaq-4o2Wm?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-06-27 Péter Diviánszky. Implementing functional languages. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPdAmSgsLgCkJaCrg1d85PF9f5SITnfZqSm9pWl?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-06-13 András Kovács. Presentation of "Unifiers as Equivalences" paper by Cockx, Devriese, Piessens. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPmA9rOsKLb1qw9BkJjT9TRPlIwXKDcR3sxXPiW?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR) | [Paper](https://jesper.sikanda.be/files/unifiers-as-equivalences.pdf)
* 2016-06-06 Péter Diviánszky. An operational semantics for dependent types. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipN55hXiE_vP3Mn4BFT1IEpoUAajU_HIGA3GGG76?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-05-16 Ambrus Kaposi. Intrinsic syntac of simple type theory, standard model, normal forms. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPFTUhNscnuaxUEwehKAtztGpHVXYIrgh-HjpIx?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-05-09 Balázs Kőműves. Topology and type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipP-k99cV0qcxxAfUE48w55D1v0WSgxZxpYdjbfC?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-05-02 Balázs Kőműves. Topology. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipOJce7e7geGG79fRsOWc9J48EH9NRNB2-A_Md82?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-04-25 Ambrus Kaposi. Quotient types, intrinsic syntax for type theory. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipPUdQcp15CMbBMogALlFepIu5m-OGxhD12EQ8IJ?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-04-18 List of possible topics, simple type theory, intrinsic syntax. [Photos](https://photos.google.com/share/AF1QipO7b06B5YyAfULZd_GUSaLmXCCjpUv4-Ejd_abRuZ9-74rIkqtQ-QXgA7gOD1FlWQ/photo/AF1QipNLDOL3a3nLfmIdEzwWly6tmWd6qvJu1K6vjHgj?key=V1c4Vk9mRVh0RGZncF8wOGRvT3IwbVMzTWdRQWJR)
* 2016-04-10 Ambrus Kaposi. Logical relations and identity types.
