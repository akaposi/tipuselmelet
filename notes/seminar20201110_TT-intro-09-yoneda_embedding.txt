

-- Presheaf model cont, Yoneda embedding, U, Π
--------------------------------------------------------------------------------

Assume a category C:

  C       : Cat
  |C|     : Set   -- objects
  C(I, J) : Set   -- morphisms
  _∘_, id, etc.

We defined a model of CwF, s.t. Con := PSh C

  (Γ : Con)
    |Γ|  : |C| → Set               -- family of sets
    _⟨_⟩ : |Γ|i → C(j,i) → |Γ|j    -- family of functions
    ∘    : γ⟨f∘g⟩ = γ⟨f⟩⟨g⟩        -- ∘ is mapped to (∘ (function))
    id   : γ⟨id⟩ = γ               -- id is mapped to (id (function)

  PSh C = functor Cᵒᵖ → Set
   - Cᵒᵖ is C but morphisms are flipped
   - Set is the category of sets & functions
   - Cᵒᵖ → _ means we have a contravariant functor
   - C(j, i) is mapped to a function from |Γ|i → |Γ|j

-- dependent presheaf
  (A : Ty Γ)
    |A|  : {i : |C|} → |Γ| i → Set         -- |A| : ((i : |C|) × |Γ| i) → Set
    _⟨_⟩ : |A| {i} γ → (f : C(j, i)) → |A| {j} (γ⟨f⟩)
    ∘    : α⟨f∘g⟩ = α⟨f⟩⟨g⟩
    id   : α⟨id⟩ = α


--------------------------------------------------------------------------------
-- Category of elements (sigma of components of a presheaf)
∫ : PSh C → Cat

  ∫Γ : Cat
    |∫Γ|                = (i : |C|) × |Γ|i
    ∫Γ((i, γ), (j, γ')) = (f : C(i, j)) × (γ'⟨f⟩ = γ)

  Ty Γ = PSh ∫Γ
--------------------------------------------------------------------------------

-- natural transformation from Γ to Δ
(σ : Sub Γ Δ)
  |σ|  : {i} → |Γ|i → |Δ|i
  _⟨_⟩ : (|σ| γ)⟨f⟩ = |σ| (γ⟨f⟩)    -- mapping commutes with restriction

-- "dependent" natural transformation
(t : Tm Γ A)
  |t|  : (γ : |Γ|i) → |A|γ
  _⟨_⟩ : (|t| γ)⟨f⟩ = |t| (γ⟨f⟩)


-- constant
--------------------------------------------------------------------------------

Constant presheaf (does not depend on C)

  KPSh : Set → Con
  |KPSh A| i = A
  α ⟨f⟩      = α

KTy views every context as a closed type (in any other context).

  KTy : Con → {Γ : Con} → Ty Γ
  |KTy Δ {Γ}|ᵢ γ := |Δ| i
  δ⟨f⟩           := δ⟨f⟩

  (lamK, appK) : Tm Γ (KTy Δ) ~ Sub Γ Δ


+ Rest of CwF:

  ∙ : Con
  ∙ = KPSh ⊤

  id  : Sub Γ Γ
  _∘_ :

  _[_] : (A : Ty Δ) → Sub Γ Δ → Ty Γ
    |A[σ]| {i} γ                   = |A| (|σ|γ)
    (α : |A| (|σ|γ)) ⟨f : C(i, j)⟩ = α⟨f⟩ : |A| ((|σ|γ)⟨f⟩)
                                          : |A| (|σ|(γ⟨f⟩))

  Γ ▶ A : Con
    |Γ ▶ A| i = (γ : |Γ|i) × |A| γ
    (γ, α)⟨f⟩ = (γ⟨f⟩, α⟨f⟩)

  p : Sub (Γ ▶ A) Γ
  q : Tm Γ (A[p])
  _,_ : (σ : Sub Γ Δ) → Tm Γ A → Sub Γ (Δ ▶ A)


-- Yoneda embedding/lemma
--------------------------------------------------------------------------------

In any C category, we have C(_, _) : |C| → |C| → Set

product of categories: _×_ : Cat → Cat → Cat

  |C × D|                 := |C| × |D|
  (C×D)((i, i'), (j, j')) := C(i, j) × D(i', j')
  (f, f') ∘ (g, g')       := (f ∘ g, f' ∘ g')
  id                      := (id, id)

Hom functor:

  Hom: Cᵒᵖ × C → Set
  |Hom|(i, j) := C(i, j)
  h ⟨f, g⟩    := g ∘ h ∘ f       -- given h : C(i, j)  f : C(i', i)   g : C(j, j')

Yoneda embeddings:

  Hom        : Cᵒᵖ × C → Set
  (currying) : Cᵒᵖ → C → Set     (covariant Yoneda embedding) (intutive FP examples)
  (flip)     : C → Cᵒᵖ → Set     (contravariant Yoneda embedding) (relevant to us)

Action of embedding on objects

  y : |C| → Con
  |yi| j = C(j, i)    -- yi : C morphisms into i
  f ⟨g⟩ = f ∘ g       -- f : C(j,i)   g : C(k, j)

Action on morphisms

  y : C(i, j) → Sub yi yj           -- convert morphism into i to morphism into j
  |yf| {k} (g : C(k, i)) : C (k, j)
                        = f ∘ g
    + naturality

Homework: show that y is a functor from C to Con:

  |yf| id      = id
  |yf| (g ∘ h) = |yf| g ∘ |yf| h


--------------------------------------------------------------------------------

y is bijection on morphisms ("fully faithful")
  (full           : surjection on morphisms)
  (faithful       : injection on morphisms )
  (fully faithful : bijection on morphisms )

  (what about action on objects? surjective/injective up to isomorphism of objects ("essentially surj/inj"))
     F : C → D
     i : |C|,   |F| i : |D|
     essentially surjective : ∀ (j : |D|) exists i : |C| s.t. |F|i ~ j

Definition: isomorphism in some (C : Cat):
  i ~ j iff there are (f : C(i, j)) and (g : C(j, i)) s.t. f∘g = id and g∘f = id

Examples:
  - isomorphism of sets:   A ~ B   pair functions
  - isormophism of Cons:   Γ ~ Δ   pair of substitutions

Definition of y being a bijection on morphisms:

  C(i, j) ~ Sub yi yj

Whenever F : C → D  fully faithful, we may call F an embedding.
F can be viewed as a subcategory of D, which contains some objects of D, but all morphisms
between objects.

Yoneda lemma : Sub yi Γ ~ |Γ|i

  to : Sub yi Γ → |Γ|i
  to σ = |σ| idᵢ
     idᵢ : C(i, i)
         : |yi|ᵢ

  from : |Γ|i → Sub yi Γ
  |from γ| (f : C(j, i)) : |Γ| j
                         := γ⟨f⟩
    γ    : |Γ|i
    γ⟨f⟩ : |Γ|j

    -- naturality
    |from γ|(f⟨g⟩) = (|from γ|f)⟨g⟩
    |from γ|(f⟨g⟩) = |from γ|(f ∘ g) = γ⟨f∘g⟩
    (|from γ|f)⟨g⟩ = γ⟨f⟩⟨g⟩         = γ⟨f∘g⟩

    -- homework to check/redo
    from (to σ) = σ
    |from (to σ)| f = |σ| f
    |from (|σ| idᵢ)| f = |σ| f
    (|σ|idᵢ)⟨f⟩ = |σ| f
    |σ|(idᵢ⟨f⟩) = |σ| f
    |σ|(idᵢ ∘ f) = |σ| f
    |σ|f = |σ| f

    to (from γ) = γ
    |from γ| id = γ
    |from γ| id = γ
    γ⟨id⟩ = γ
    γ = γ

We have : Sub yi Γ ~ |Γ|i
To show: C(i, j) ~ Sub yi yj
   - choose Γ to be yj
   - Sub yi yj ~ |yj|i ~ C(i, j)

--------------------------------------------------------------------------------

Fam model

     f
  0 --> 1

  Sub y0 A ~ |A| 0
  Sub y1 A ~ |A| 1
  yf : Sub y0 y1

--------------------------------------------------------------------------------

In Haskell: C=Hask is category of (Haskell) types and functions

Contravariant Yoneda:

  Contravariant f => (forall b. (b -> a) -> f b) ~ f a
    (left side is automatically a natural transformation)

Covariant Yoneda:

  Functor f => (forall b. (a -> b) -> f b) ~ f a

Example:

  Let f = Id (identity functor)
  (forall j. (i -> j) -> j) ~ i   (Church encoding of identity functor)

Hom in Haskell:

  newtype Reader a b = Reader (a -> b)

--------------------------------------------------------------------------------

  Nat : Ty Γ
  Nat = KTy (KPSh ℕ)    -- KPSh ℕ : Con
                        -- KTy (KPSh ℕ) : Ty Γ

  + zero, suc, NatInd, β

Including any A : Set as a type in the model

  A : Set
  KTy (KPSh A) : Ty Γ

Moreover, we know the following:

                       KTy                  Yoneda             KPSh
  Tm yi (KTy (KPSh A))  ~    Sub yi (KPSh A)  ~    |KPSh A| i   ~    A

Representability:
  Γ : Con is representable iff there is some i s.t. Γ ~ yi

Is ∙ : Con representable?

We need terminal object in C:

  ∙ᶜ : |C|
  εᶜ : C(i, ∙ᶜ)    (unique)

If we have this, then

   y∙ᶜ ~ ∙

because

   |y∙ᶜ|i = C(i, ∙ᶜ) ~ ⊤
   |∙|i = ⊤

y preserves terminal objects (up to iso). With this, we also have

  Tm ∙ (KTy (KPSh A)) ~ Tm y∙ᶜ (KTy (KPSh A)) ~ A

in particular: Tm ∙ Nat ~ ℕ

If C is CwF: Tyᶜ, Tmᶜ, _▶ᶜ_, pᶜ, qᶜ, _,ᶜ_

   y (i ▶ᶜ α) ~ y i ▶ y α          (need: yoneda embedding on C types!)

Hence, every Con s.t. it's given as an iterated _▶_ of representable types, is
itself representable.


-- Universe (Hofmann-Streicher universe)
--------------------------------------------------------------------------------

Previously : Coquand universe:

  (El, c) : Tm Γ U ~ Ty Γ

If we have KTy, then it's enough to define U as Con:

  (El, c) : Sub Γ U ~ Ty Γ

We can recover usual U as KTy U

  U : Con                      Yoneda             requirement
  |U|i := Ty yi          |U|i    ~     Sub yi U       ~       Ty yi

  (a : |U|i)⟨f : C(j, i)⟩ : |U|j
  (a : Ty yi)⟨f⟩ :=  a[yf]         -- type substitution in PSh model

    f  : C(j, i)
    yf : Sub yj yi

  El (σ : Sub Γ U) : Ty Γ
  |El σ|ᵢ γ := ||σ|ᵢ γ|ᵢ idᵢ

  (α : ||σ|ᵢ γ|ᵢ idᵢ) ⟨f : C(j, i)⟩ : ||σ|ⱼ (γ⟨f⟩)|ⱼ idⱼ
                                    : |(|σ|ᵢ γ) ⟨f⟩|ⱼ idⱼ
  				    : |(|σ|ᵢ γ) [yf]|ⱼ idⱼ
  				    : |(|σ|ᵢ γ)|ⱼ (|yf| idⱼ)
  				    : |(|σ|ᵢ γ)|ⱼ (f ∘ idⱼ)
  				    : |(|σ|ᵢ γ)|ⱼ f

    α⟨f⟩ : ||σ|ᵢ γ|ⱼ (idᵢ⟨f⟩)
         : ||σ|ᵢ γ|ⱼ (idᵢ ∘ f)
         : ||σ|ᵢ γ|ⱼ f

    α⟨f⟩ := α⟨f⟩

  -- homework:
  c : Ty Γ → Sub Γ U


-- Exponential
--------------------------------------------------------------------------------

Exponential object: simpler version of function types.
We get exponentials in PSh model by application of Yoneda.
The definition for exponentials serves as template/inspiration for Π.

Categorical product (homework to define):
  _×_ : Con → Con → Con
  π₁  : Sub (Γ × Δ) Γ
  π₂  : Sub (Γ × Δ) Δ
  _,_ : Sub Γ Δ → Sub Γ Σ → Sub Γ (Δ × Σ)
  + β, η

Exponential:
  _⇒_ : Con → Con → Con
  (app, lam) : Sub (Γ × Δ) Σ ~ Sub Γ (Δ ⇒ Σ)

Compare defining iso of type-theoretic non-dependent functions:

  Tm (Γ ▶ A) B ~ Tm Γ (A ⇒ B)

Definition in PSh C:

  _⇒_ : Con → Con → Con                       Yoneda               defining iso
  |Δ ⇒ Σ|i := Sub (yi × Δ) Σ   --  |Δ ⇒ Σ|i    ~    Sub yi (Δ ⇒ Σ)      ~       Sub (yi × Δ) Σ

  -- homework: define _⟨_⟩ for Δ ⇒ Σ, app, lam, β, η

Note: Sub (yi × Δ) Σ is a Kripke function space + naturality

  |Sub (yi × Δ) Σ| : ∀ {j} → C(j, i) → |Δ|j → |Σ|j



-- Pi
--------------------------------------------------------------------------------


  Π : (A : Ty Γ) → Ty (Γ ▶ A) → Ty Γ
  |Π A B|ᵢ γ := ?

Recall |Δ ⇒ Σ|i := Sub (yi × Δ) Σ

Let's define Π the same way, but with Tm/Ty instead of Sub/Con

  |Π A B|ᵢ γ := Tm (yi ▶ A[fromY γ]) B[fromY γ, q]

     where (fromY : |Γ|i → Sub yi Γ) is a component of Yoneda lemma

  -- homework: define all other components of Π
