\documentclass{article}

\usepackage[margin=1.5cm,landscape]{geometry}
\usepackage{proof}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{pdflscape}
\usepackage{multirow,bigdelim}
\usepackage{hyperref}
\usepackage{multicol}
\input{abbrevs.tex}

\allowdisplaybreaks

\begin{document}

\thispagestyle{empty}

% We define type theory as a generalised algebraic theory (or quotient
% inductive-inductive type) in an extensional metatheory. This is an
% algebraic presentation of (predicative) CwFs with extra structure. The
% $_i$, $_j$ indices are metatheoretic universe levels.

\begin{multicols}{3}
  \noindent
  \section*{Type theory}
  \begin{alignat*}{5}
  & \Con_{\blank} && :\,\, && \N\ra\Set \\
  & \Ty_{\blank}  && : && \N\ra\Con_i\ra\Set \\
  & \Tms && : && \Con_i\ra\Con_j\ra\Set \\
  & \Tm  && : && (\Gamma:\Con_i)\ra\Ty_j\,\Gamma\ra\Set \\
  & \heading{Substitution calculus} \\
  & \cdot && : && \Con_0 \\
  & \blank\ext\blank && : && (\Gamma:\Con_i)\ra\Ty_j\,\Gamma\ra\Con_{\max{i}{j}} \\
  & \blank[\blank] && : && \Ty_j\,\Delta\ra\Tms\,\Gamma\,\Delta\ra\Ty_j\,\Gamma \\
  & \id && : && \Tms\,\Gamma\,\Gamma \\
  & \blank\circ\blank && : && \Tms\,\Theta\,\Delta\ra\Tms\,\Gamma\,\Theta\ra\Tms\,\Gamma\,\Delta \\
  & \epsilon && : && \Tms\,\Gamma\,\cdot \\
  & \blank,\blank && : && (\sigma:\Tms\,\Gamma\,\Delta)\ra\Tm\,\Gamma\,(A[\sigma])\ra \\
  & && && \Tms\,\Gamma\,(\Delta\ext A) \\
  & \pi_1 && : && \Tms\,\Gamma\,(\Delta\ext A) \ra \Tms\,\Gamma\,\Delta \\
  & \pi_2 && : && (\sigma:\Tms\,\Gamma\,(\Delta\ext A))\ra\Tm\,\Gamma\,(A[\pi_1\,\sigma]) \\
  & \blank[\blank] && : && \Tm\,\Delta\,A\ra(\sigma:\Tms\,\Gamma\,\Delta)\ra\Tm\,\Gamma\,(A[\sigma]) \\
  & [\id] && : && A[\id] = A \\
  & [\circ] && : && A[\sigma \circ \delta] = A[\sigma][\delta] \\
  & \ass && : && (\sigma \circ \delta) \circ \nu = \sigma \circ (\delta \circ \nu) \\
  & \idl && : && \id\circ\sigma = \sigma \\
  & \idr && : && \sigma\circ\id = \sigma \\
  & {\cdot\eta} && : && (\sigma : \Tms\,\Gamma\,\cdot) = \epsilon \\
  & {\ext\beta_1} && : && \pi_1\,(\sigma, t) = \sigma \\
  & {\ext\beta_2} && : && \pi_2\,(\sigma, t) = t \\
  & {\ext\eta} && : && (\pi_1\,\sigma, \pi_2\,\sigma) = \sigma \\
  & {,\circ} && : && (\sigma, t)\circ\delta = (\sigma\circ\delta, t[\delta]) \\
  & \heading{Function space} \\
  & \Pi && : && (A:\Ty_i\,\Gamma)\ra\Ty_j\,(\Gamma\ext A)\ra\Ty_{\max{i}{j}}\,\Gamma \\
  & \lam && : && \Tm\,(\Gamma\ext A)\,B\ra\Tm\,\Gamma\,(\Pi\,A\,B) \\
  & \app && : && \Tm\,\Gamma\,(\Pi\,A\,B)\ra\Tm\,(\Gamma\ext A)\,B \\
  & {\Pi\beta} && : && \app\,(\lam\,t) = t \\
  & {\Pi\eta} && : && \lam\,(\app\,t) = t \\
  & {\Pi[]} && : && (\Pi\,A\,B)[\sigma] = \Pi\,(A[\sigma])\,(B[\sigma^\uparrow]) \\
  & {\lam[]} && : && (\lam\,t)[\sigma] = \lam\,(t[\sigma^\uparrow]) \\
  & \heading{Sigma} \\
  & \Sigma && : && (A:\Ty_i\,\Gamma)\ra\Ty_j\,(\Gamma\ext A)\ra\Ty_{\max{i}{j}}\,\Gamma \\
  & \blank,\blank && : && (u:\Tm\,\Gamma\,A)\ra\Tm\,\Gamma\,(B[\lb u\rb])\ra \\
  & && && \Tm\,\Gamma\,(\Sigma\,A\,B) \\
  & \proj_1 && : && \Tm\,\Gamma\,(\Sigma\,A\,B)\ra\Tm\,\Gamma\,A \\
  & \proj_2 && : && (t:\Tm\,\Gamma\,(\Sigma\,A\,B))\ra\Tm\,\Gamma\,(B[\lb \proj_1\,u\rb]) \\
  & {\Sigma\beta}_1 && : && \proj_1\,(u,v) = u \\
  & {\Sigma\beta}_2 && : && \proj_2\,(u,v) = v \\
  & {\Sigma\eta} && : && (\proj_1\,t, \proj_2\,t) = t \\
  & {\Sigma[]} && : && (\Sigma\,A\,B)[\sigma] = \Sigma\,(A[\sigma])\,(B[\sigma^\uparrow]) \\
  & {,[]} && : && (u,v)[\sigma] = (u[\sigma], v[\sigma]) \\
  & \heading{Unit} \\
  & \top && : && \Ty_0\,\Gamma \\
  & \tt && : && \Tm\,\Gamma\,\top \\
  & {\top\eta} && : && (t:\Tm\,\Gamma\,\top) = \tt \\
  & {\top[]} && : && \top[\sigma] = \top \\
  & {\tt[]} && : && \tt[\sigma] = \tt \\
  & \heading{Empty} \\
  & \bot && : && \Ty_0\,\Gamma \\
  & \exfalso && : && \Tm\,\Gamma\,\bot\ra\Tm\,\Gamma\,C \\
  & {\bot[]} && : && \bot[\sigma] = \bot \\
  & {\exfalso[]} && : && (\exfalso\,t)[\sigma] = \exfalso\,(t[\sigma]) \\
  & \heading{Sum} \\
  & \blank+\blank && : && \Ty_i\,\Gamma\ra\Ty_j\,\Gamma\ra\Ty_{\max{i}{j}}\,\Gamma \\
  & \inj_1 && : && \Tm\,\Gamma\,A\ra\Tm\,\Gamma\,(A+B) \\
  & \inj_2 && : && \Tm\,\Gamma\,B\ra\Tm\,\Gamma\,(A+B) \\
  & \case && : && (P:\Ty_i\,(\Gamma\ext A+B))\ra \\
  & && && \Tm\,(\Gamma\ext A)\,(P[\wk,\inj_1\,\vz])\ra \\
  & && && \Tm\,(\Gamma\ext B)\,(P[\wk,\inj_2\,\vz])\ra \\
  & && && (t:\Tm\,\Gamma\,(A+B))\ra \Tm\,\Gamma\,(P[\lb t\rb]) \\
  & {{+}\beta}_1 && : && \case\,P\,u\,v\,(\inj_1\,t) = u[\lb t\rb] \\
  & {{+}\beta}_2 && : && \case\,P\,u\,v\,(\inj_2\,t) = v[\lb t\rb] \\
  & {{+}[]} && : && (A+B)[\sigma] = A[\sigma] + B[\sigma] \\
  & {\inj_1[]} && : && (\inj_1\,t)[\sigma] = \inj_1\,(t[\sigma]) \\
  & {\inj_2[]} && : && (\inj_2\,t)[\sigma] = \inj_2\,(t[\sigma]) \\
  & {\case[]} && : && (\case\,P\,u\,v\,t)[\sigma] = \\
  & && && \case\,(P[\sigma^\uparrow])\,(u[\sigma^\uparrow])\,(v[\sigma^\uparrow])\,(t[\sigma]) \\
  & \heading{Coquand universes} \\
  & \U_{\blank} && : && (i:\N)\ra\Ty_{i+1}\,\Gamma \\
  & \El && : && \Tm\,\Gamma\,\U_i \ra \Ty_i\,\Gamma \\
  & \c && : && \Ty_i\,\Gamma\ra\Tm\,\Gamma\,\U_i \\
  & {\U\beta} && : && \El\,(\c\,A) = A \\
  & {\U\eta} && : && \c\,(\El\,a) = a \\
  & {\U[]} && : && \U_i[\sigma] = \U_i \\
  & {\El[]} && : && (\El\,a)[\sigma] = \El\,(a[\sigma]) \\
  & \heading{Booleans} \\
  & \Bool && : && \Ty_0 \\
  & \true && : && \Tm\,\Gamma\,\Bool \\
  & \false && : && \Tm\,\Gamma\,\Bool \\
  & \IF && : && (P:\Ty_i\,(\Gamma\ext\Bool))\ra\Tm\,\Gamma\,(P[\lb\true\rb])\ra \\
  & && && \Tm\,\Gamma\,(P[\lb\false\rb])\ra(t:\Tm\,\Gamma\,\Bool)\ra \\
  & && && \Tm\,\Gamma\,(P[\lb t\rb]) \\
  & {\Bool\beta}_\true && : && \IF\,P\,u\,v\,\true = u \\
  & {\Bool\beta}_\false && : && \IF\,P\,u\,v\,\false = v \\
  & {\Bool[]} && : && \Bool[\sigma] = \Bool \\
  & {\true[]} && : && \true[\sigma] = \true \\
  & {\false[]} && : && \false[\sigma] = \false \\
  & {\IF[]} && : && (\IF\,P\,u\,v\,t)[\sigma] = \IF\,(P[\sigma^\uparrow])\,(u[\sigma])\,(v[\sigma])\,(t[\sigma]) \\
  & \heading{Natural numbers} \\
  & \Nat && : && \Ty_0 \\
  & \zero && : && \Tm\,\Gamma\,\Nat \\
  & \suc && : && \Tm\,\Gamma\,\Nat\ra\Tm\,\Gamma\,\Nat \\
  & \ind && : && (P:\Ty_i\,(\Gamma\ext\Nat))\ra\Tm\,\Gamma\,(P[\lb\zero\rb])\ra \\
  & && && \Tm\,(\Gamma\ext\Nat\rhd P)\,(P[(\wk,\suc\,\vz)\circ\wk])\ra \\
  & && && (t:\Tm\,\Gamma\,\Nat)\ra \Tm\,\Gamma\,(P[\lb t\rb]) \\
  & {\Nat\beta}_\zero && : && \ind\,P\,u\,v\,\zero = u \\
  & {\Nat\beta}_\suc && : && \ind\,P\,u\,v\,(\suc\,t) = v[\lb t\rb,\ind\,P\,u\,v\,t] \\
  & {\Nat[]} && : && \Nat[\sigma] = \Nat \\
  & {\zero[]} && : && \zero[\sigma] = \zero \\
  & {\suc[]} && : && (\suc\,t)[\sigma] = \suc\,(t[\sigma]) \\
  & {\ind[]} && : && (\ind\,P\,u\,v\,t)[\sigma] = \\
  & && && \ind\,(P[\sigma^\uparrow])\,(u[\sigma])\,(v[\sigma])\,(t[{\sigma^\uparrow}^\uparrow]) \\
  & \heading{Identity} \\
  & \Id && : && (A:\Ty_i\,\Gamma)\ra\Tm\,\Gamma\,A\ra\Tm\,\Gamma\,A\ra\Ty_i\,\Gamma \\
  & \refl && : && (u : \Tm\,\Gamma\,A) \ra \Tm\,\Gamma\,(\Id\,A\,u\,u) \\
  & \J && : && (P : \Ty_i\,(\Gamma\ext A\ext \Id\,(A[\wk])\,(u[\wk])\,\vz))\ra \\
  & && && \Tm\,\Gamma\,(P[\id,u,\refl\,(u[\wk])])\ra \\
  & && && (e:\Tm\,\Gamma\,(\Id\,A\,u\,v))\ra \\
  & && && \Tm\,\Gamma\,(P[\id,v,e[\wk]]) \\
  & {\Id\beta} && : && \J\,P\,w\,(\refl\,u) = w \\
  & {\Id[]} && : && (\Id\,A\,u\,v)[\sigma] = \Id\,(A[\sigma])\,(u[\sigma])\,(v[\sigma]) \\
  & {\refl[]} && : && (\refl\,u)[\sigma] = \refl\,(u[\sigma]) \\
  & {\J[]} && : && (\J\,P\,w\,e)[\sigma] = \J\,(P[{\sigma^\uparrow}^\uparrow])\,(w[\sigma])\,(e[\sigma]) \\
  & \heading{Abbreviations} \\
  & \wk && : && \Tms\,(\Gamma\ext A)\,\Gamma := \pi_1\,\id \\
  & \vz && : && \Tm\,(\Gamma\ext A)\,(A[\wk]) := \pi_2\,\id \\
  & \vs && && (t:\Tm\,\Gamma\,A):\Tm\,(\Gamma\ext B)\,(A[\wk]) := t[\wk] \\
  & \lb\blank\rb && && (t:\Tm\,\Gamma\,A):\Tms\,\Gamma\,(\Gamma\ext A) := (\id, t) \\
  & \blank^\uparrow && && (\sigma:\Tms\,\Gamma\,\Delta):\Tms\,(\Gamma\ext A[\sigma])\,(\Delta\ext A) := \\
  & && && (\sigma\circ\wk, \vz) \\
  & [\id] && : && t[\id] = t \\
  & [\circ] && : && t[\sigma\circ\delta] = t[\sigma][\delta] \\
  & {\pi_1\circ} && : && (\pi_1\,\sigma)\circ\delta = \pi_1\,(\sigma\circ\delta) \\
  & {\pi_2[]} && : && (\pi_2\,\sigma)[\delta] = \pi_2\,(\sigma\circ\delta) \\
  & {\app[]} && : && (\app\,t)[\sigma^\uparrow] = \app\,(t[\sigma]) \\
  & \blank\Ra\blank && : && (A:\Ty_i\,\Gamma)(B:\Ty_j\,\Gamma)\ra\Ty_{\max{i}{j}}\,\Gamma := \\
  & && && \Pi\,A\,(B[\wk]) \\
  & \blank\oldapp\blank && : && (t:\Tm\,\Gamma\,(\Pi\,A\,B))(u : \Tm\,\Gamma\,A): \\
  & && && \Tm\,\Gamma\,(B[\lb u\rb]) := (\app\,t)[\lb u \rb] \\
  & {{\oldapp}\beta} && : && (\lam\,t)\oldapp u = t[\lb u\rb] \\
  & {{\oldapp}\eta} && : && \lam\,(t[\wk]\oldapp \vz) = t \\
  & \blank\times\blank && : && (A:\Ty_i\,\Gamma)(B:\Ty_j\,\Gamma)\ra\Ty_{\max{i}{j}}\,\Gamma := \\
  & && && \Sigma\,A\,(B[\wk]) \\
  & {\proj_1[]} && : && (\proj_1\,t)[\sigma] = \proj_1\,(t[\sigma]) \\
  & {\proj_2[]} && : && (\proj_2\,t)[\sigma] = \proj_2\,(t[\sigma]) \\
  & {\c[]} && : && (\c\,A)[\sigma] = \c\,(A[\sigma]) \\
\end{alignat*}
\end{multicols}

\end{document}
