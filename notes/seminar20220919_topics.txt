invited speakers:
- Marcin on crypto things in Agda
- Mike Shulman

away:
- Andras away 9-16 Oct
- Ambrus away 18-23 Oct
- Tamas 6 Oct

topics:
- introductory?
  - CwF,Bool,Π, working with the syntax, using De Bruijn for calculations (Isti)
  - W types (Rafael)
  - ToS description of inductive types (Andras) - 1st chapter of his thesis
- ongoing research:
  - Tamas: combinators, towards semantics of SOGATs using combinators
  - Rafael's paper on external univalence
  - Rafael: relative induction principles new developments - nice description of internal sections
  - Isti: later on canonicity for SeTT
- we would like to learn about:
  - category-theoretic presentations of algebraic theories, locally presentable categories, Adamek, functorial semantics
    - Taichi's infinity-presentable theories

not that important to talk about:
- Ambrus: Reedy fibrant presheaves
- ...
