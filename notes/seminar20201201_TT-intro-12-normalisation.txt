

-- Normalization proof | Setoid model of TT
-- Normalization proof (canonicity vs. norm for simple type theory, something about DTT)

Simple type theory with _⇒_ and Bool

Canonicity:
  - every (Tm ∙ Bool) is either True or False
  - by η-expansion every (Tm ∙ (A ⇒ B)) is a lambda

-- (logical predicate model)
(Γ : Con)ᴹ  : Sub ∙ Γ → Set                              -- predicate on closing substs
(A : Ty Γ)ᴹ : (σ : Sub ∙ Γ) → Γᴹ σ → Tm ∙ (A[σ]) → Set   -- predicate on closed terms (depending on context)


In simple TT : evaluate terms in the standard model, then go back to syntax

   ("canonicity" by evaluation)

                  eval in Set model                (true ↦ true, false ↦ false)
   (t : Tm ∙ Bool)       -->          (tᴹ : Bool)              -->              Tm ∙ Bool


   + prove that syntactic terms are related to semantic values (dependent model of Simple TT)

   _≈_ : Tm ∙ A → Aᴹ → Set
   _≈_ {A = Bool}  t tᴹ = if tᴹ then (t = true) else (t = false)    --
   _≈_ {A = A ⇒ B} f fᴹ = ∀ {t tᴹ} → t ≈ tᴹ → app f t ≈ fᴹ tᴹ       -- preservation of relation

   _≈_ : {Γ} → Sub ∙ Γ → Γᴹ → Set  -- pointwise relatedness

   -- for terms:         prove that terms preserve _≈_
   -- for substitutions: prove -||-


   (t : Tm ∙ Bool) --> tᴹ : Bool

    t ≈ tᴹ (this says that t must be true or false)

   t  : Tm Γ A
   tᴹ : Γᴹ → Aᴹ

   Tm≈ : (t : Tm Γ A) (σ : Sub ∙ Γ)(σᴹ : Γᴹ) → σ ≈ σᴹ → t[σ] ≈ tᴹ σᴹ


Normalization for simple TT: (normalization by evaluation)

                 eval in a PSh model                   quote
   (t : Tm Γ A)        --->             (tᴹ : Aᴹ Γ)     --->       Nf Γ A

   (intuitively: why can't we quote from Set-model values to normal forms?)
   (reason: we need a source of fresh variables, to go under binders)

   f : (Aᴹ → Bᴹ)                f : Bool → Bool    (only call with true/false and get true/false)
   goal : Nf Γ (A ⇒ B)                             (call f with a fresh variable and get back true/false/neutral bool)

   lam (? : Nf (Γ, A) B)


   -- base category: category of syntactic renamings (or embeddings)

   -- renaming from Γ to Δ (Ren Γ Δ) is a Sub Γ Δ, s.t. it only contains variables (list of variables)
   -- embedding (σ : Emb Γ Δ) if we get Δ by dropping zero or more entry from Γ

       -- Γ = (Bool, Nat, Bool)
       -- Δ = (Nat, Bool)
       -- Emb Γ Δ (drop first entry from Γ)

   -- categories of renamings and embeddings (sub-categories of Sub)

   -- model : PSh(REN)               Conᴹ     := PSh REN
                                     Tyᴹ Γ    := dependent PSh over Γ
				     Subᴹ Γ Δ := natural transformations
				     Tmᴹ Γ A  := dependent natural transformations

   (A : Ty)ᴹ      : PSh(REN)      -- Conᴹ
   (Γ : Con)ᴹ     : PSh(REN)      -- Conᴹ
   (t : Tm Γ A)ᴹ  : Nat(Γᴹ, Aᴹ)   -- Subᴹ Γᴹ Aᴹ
   (σ : Sub Γ Δ)ᴹ : Nat(Γᴹ, Δᴹ)   -- Subᴹ Γᴹ Δᴹ

   (A ⇒ B)ᴹ : PShExp Aᴹ Bᴹ

   -- we have to decide on the interpretation of Bool

   -- have an empty base type
   (ι : Ty)ᴹ : PSh(REN)
   |ι| Γ := Nf Γ ι
   x⟨σ⟩  := x[σ]


   2. I can just define quote/unquote function by (mutual) recursion on syntactic types:

   quote   : ∀ {A : Type} → Subᴹ Aᴹ (Nf _ A)
   unquote : ∀ {A : Type} → Subᴹ (Ne _ A) Aᴹ

   3. Prove completeness of normalization

   _≈_ : between syntax and semantics

   (+ stability (by induction on terms) )

   -- Boolᴹ : PSh(REN)      (for every Γ I get a Set, s.t. I can restrict along any (σ : Ren Γ Δ))  (I get renamings of semantic values as well!)
   -- |Boolᴹ| Γ                     := Nf Γ Bool
   -- (bᴹ : Nf Δ Bool)⟨σ : Ren Γ Δ⟩ := bᴹ[σ]                  (for any A : Type,  Nf _ A : PSh(REN))

Dependent TT:

-- One dependent model over syntax: normalization + completeness at the same time

- try to define P model
- _ᴹ is interpretation into the PSh(REN) model of dependent type theory
- we also need a variant of Yoneda embedding

    (PSh(Syn) → PSh(REN)) : defined by using that every renaming is a substitution
  y : Syn → PSh(REN)

  |yΓ| Δ := Sub Δ Γ
  σ⟨δ⟩   := σ ∘ δ      (every renaming is also a substitution)

(Γ : Con)ᴾ     : Tyᴹ (yΓ)                        -- predicate over substitutions into Γ
(A : Ty Γ)ᴾ    : Tyᴹ (yΓ ▶ Γᴾ ▶ yA[p])           -- predicate over terms of A
(σ : Sub Γ Δ)ᴾ : Tmᴹ (yΓ ▶ Γᴾ) (Δᴾ[σ])           -- σ preserves predicates
(t : Tm Γ A)ᴾ  : Tmᴹ                             -- t preserves predicates

  -- unfolding all Tyᴹ/Tmᴹ is ugly
  -- it's ugly even if we don't unfold it!

-- "internal language of the presheaf model" (only using closed types, universe, closed terms from (PSh(REN)))
-- (no de Bruijn indices, "normal" type theoretic notation)
-- (→ is presheaf →, Set is universe  in PSh(REN))
(Γ : Con)ᴾ     : yΓ → Set
(A : Ty Γ)ᴾ    : (γ : yΓ) → Γᴾ γ → yA γ → Set
(σ : Sub Γ Δ)ᴾ : (γ : yΓ)(γᴾ : Γᴾ γ) → Δᴾ (yσ γ)

-- even more advanced: Rafael/Christian Sattler/Taichi Uemura (we don't even mention contexts and substitutions (most of the time))
--                     (internal languages + Higher Order Abstract Syntax)


-- Setoid model of type theory
--------------------------------------------------------------------------------
