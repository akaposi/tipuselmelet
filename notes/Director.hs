{-# OPTIONS_GHC -fllvm -O2 #-}

{-# language BlockArguments, LambdaCase, RebindableSyntax, OverloadedStrings #-}
{-# language PatternSynonyms, ViewPatterns, PatternGuards, BangPatterns #-}
{-# language NoMonomorphismRestriction, FlexibleInstances, FlexibleContexts, ExtendedDefaultRules #-}
{-# language TypeFamilies #-} -- to allow equational constraints a ~ b

import Prelude hiding ((>>=))
import qualified Prelude ((>>=))
import Data.String (fromString)
import System.Environment (getArgs)
import Debug.Trace (trace)
--import Text.Pretty.Simple (pPrint)

-------------------------------------------------------------------------- Benchmarks

-- integer square root
iSqrt :: Int -> Int
iSqrt x = round (sqrt (fromIntegral x))

-- reference implementation of the main benchmark
nthPrimeReference :: Int -> Int
nthPrimeReference n = (2: primes) !! n
  where
    primes = 3:
        [n | n <- [5,7..]
           , let rt = iSqrt n
           , all (\x -> 0 /= mod n x) (takeWhile (\p -> p <= rt) primes)
        ]

-- implementation without Prelude definitions (but using Prelude's Bool and Int)
-- competitor #1:  the GHCi interpretation speed of this
nthPrime n = ix n (2: primes)
  where
    ite True  t _ = t
    ite False _ f = f

    foldr c n = f where
        f [] = n
        f (x:xs) = c x (f xs)

    comp f g x = f (g x)

    and a b = ite a b False
    or  a b = ite a True b
    not a   = ite a False True

    filter    f = foldr (\x xs -> ite (f x) (x: xs) xs) []
    takeWhile f = foldr (\x xs -> ite (f x) (x: xs) []) []
    all       f = foldr (comp and f) True

    fromThen a = ft where
        ft b = b : ft (a + b)

    ix i (x:xs) = ite (i == 0) x (ix (i - 1) xs)

    primes :: [Int]
    primes = 3:
        filter
            (\n -> (\rt ->
                    all (\x -> not (0 == mod n x))
                        (takeWhile (\p -> or (p < rt) (p == rt)) primes)
                   ) (iSqrt n)
            )
            (fromThen 2 5)

(>>=) = let_    --  (let_ x f) is essentially equal to (f x)

-- implementation with director strings (see later)
-- competitor #2:  the GHC interpretation speed of this
nthPrimeDirector n = do
    true        <- \t _ -> t
    false       <- \_ f -> f
    nil         <- \n c -> n
    cons        <- \x xs n c -> c ⎵ x ⎵ xs

    foldr       <- \c n -> Y ⎵ \f xs -> xs ⎵ n ⎵$ \x xs -> c ⎵ x ⎵$ f ⎵ xs
    comp        <- \f g c -> f ⎵ (g ⎵ c)
    and         <- \a b -> a ⎵ b ⎵ false
    or          <- \a b -> a ⎵ true ⎵ b
    not         <- \a -> a ⎵ false ⎵ true
    filter      <- \f -> foldr ⎵ (\x xs -> f ⎵ x ⎵ (cons ⎵ x ⎵ xs) ⎵ xs) ⎵ nil
    takeWhile   <- \f -> foldr ⎵ (\x xs -> f ⎵ x ⎵ (cons ⎵ x ⎵ xs) ⎵ nil) ⎵ nil
    all         <- \f -> foldr ⎵ (comp ⎵ and ⎵ f) ⎵ true
    fromThen    <- \a -> Y ⎵ \ft b -> cons ⎵ b ⎵$ ft ⎵$ a + b
    ix          <- Y ⎵ \ix i xs -> xs ⎵ Y ⎵$ \x xs -> Eq ⎵ 0 ⎵ i ⎵ x ⎵$ ix ⎵ (i - 1) ⎵ xs
    primes      <- Y ⎵ \primes -> cons ⎵ 3 ⎵$
                filter
                    ⎵ (\n -> do
                        rt <- Sqrt ⎵ n
                        all ⎵ (\x -> not ⎵$ Eq ⎵ 0 ⎵$ Mod ⎵ n ⎵ x)
                          ⎵$ takeWhile ⎵ (\p -> or ⎵ (Lt ⎵ p ⎵ rt) ⎵ (Eq ⎵ p ⎵ rt)) ⎵ primes
                      )
                    ⎵$ fromThen ⎵ 2 ⎵ 5
    ix ⎵ n ⎵$ cons ⎵ 2 ⎵ primes

data Stream = SCons Int Stream

-- implementation fused by hand
-- to factor out the effect of the inlining optimizations
nthPrimeFused n = ix n (SCons 2 primes)
  where
    ix n (SCons x xs)
        | n == 0    = x
        | otherwise = ix (n - 1) xs

    primes = SCons 3 (go 5)

    go n = f primes
      where
        sn = iSqrt n
        f (SCons p ps)
            | p > sn       = SCons n (go (n + 2))
            | mod n p == 0 = go (n + 2)
            | otherwise    = f ps

nthPrimeFusedDirector n = ix ⎵ n ⎵$ scons ⎵ 2 ⎵ primes
  where
    true  t _ = t
    false _ f = f
    ix = Y ⎵ \ix n xs -> xs ⎵ \x xs -> Eq ⎵ 0 ⎵ n ⎵ x ⎵$ ix ⎵ (n - 1) ⎵ xs
    scons x xs = \c -> c ⎵ x ⎵ xs
    primes =
        Y ⎵ \primes -> scons ⎵ 3 ⎵$
            Y ⎵ do \go n -> do sn <- Sqrt ⎵ n
                               Y ⎵ do \f s -> s ⎵$ \p ps -> Le ⎵ p ⎵ sn
                                                                ⎵ (Eq ⎵ 0 ⎵ (Mod ⎵ n ⎵ p) ⎵ false ⎵$ f ⎵ ps)
                                                                ⎵ true
                                   ⎵ primes ⎵ (scons ⎵ n) ⎵ id ⎵ (go ⎵$ 2 + n)
                ⎵ 5

-----------------------------------------
-- runtime
-----------------------------------------
--             | 10000 | 100000 | 1000000
-- primes
-- #1 GHCi     | 0.77s | 18.9s  | 519s
-- #2 director | 0.13s |  3.15s |  83.5s
---- GHC       | 0.016 |  0.29s |   7.46s
-- primes fused by hand
---- GHCi      | 0.31s |  7.28s | 196s     
---- director  | 0.07s |  1.75s |  48.5s
---- GHC       | 0.012 |  0.20s |   4.97s
-----------------------------------------

--------------------------------------------------------------
-- memory allocations   Mb: max   Gb: all
--------------------------------------------------------------
--             | 10000          | 100000        | 1000000
-- primes 
-- #1 GHCi     |   ?     0.77Gb |   ?    19.2Gb |   ?    503Gb
-- #2 director | 1.01Mb  0.57Gb | 6.48Mb 14.2Gb | 82.3Mb 373Gb
---- GHC       | 0.075   0.040  | 3.11Mb  1.0Gb | 33.4Mb  28Gb
-- primes fused by hand
---- GHCi      |   ?     0.19Gb |   ?     4.6Gb |   ?    117Gb
---- director  | 0.081   0.278  | 4.06Mb  7.22  | 52.4Mb 195Gb
---- GHC       | 0.044   0.0010 | 4.13Mb  0.010 | 23.9Mb 0.104
--------------------------------------------------------------



-------------------------------------------------------------------------- Implementation

--------------------- combinatory logic terms with director strings

-- director strings
data DString
    = J !DString
    | B !DString
    | C !DString
    | S !DString
    | A
        deriving (Eq)

{- Semantics of director strings

⟦J e⟧ = \a b c -> ⟦e⟧  a     b
⟦B e⟧ = \a b c -> ⟦e⟧  a    (b c)
⟦C e⟧ = \a b c -> ⟦e⟧ (a c)  b
⟦S e⟧ = \a b c -> ⟦e⟧ (a c) (b c)
⟦A⟧    = \a b   ->     a     b

-}

-- terms
data Tm
    = Ap Nf{-cached-} !DString !Tm Tm
    | I

{- Semantics of terms

⟦Ap s a b⟧ = ⟦s⟧ ⟦a⟧ ⟦b⟧
⟦I⟧ = \a -> a

So

   ⟦Ap s e1 e2⟧ = \v1 v2 ... vN -> ⟦e1⟧ u1 u2 ... uM (⟦e2⟧ w1 w2 ... wK)

where
  u1, ..., uM  and  w1, ..., wK  are sublist of  v1, ..., vN
-}

    -- optimized Ints
    | Int {-# UNPACK #-} !Int
    | Eq | Lt | Le | Add | Sub | Mul | Abs | Mod | Sgn | Sqrt

    -- other optimizations
    | Y      -- optimized (Ap _ (S A) I I) (Ap _ (C (B A)) I (Ap _ (S A) I I))
    | Inline -- same as I, tags terms for inlining
    | Jit    -- same as I, recompiles its argument during evaluation
        deriving (Eq)

-- normal forms, used for optimization
-- the source of the code is needed for recompilation during evaluation
-- arg1 in closures is strict because it is always needed 
data Nf
    = NInt {-# UNPACK #-} !Int
    | N0 !Tm{-source-} !(Nf -> Nf){-code-}
    | N1 !Tm{-source-} !(Nf -> Nf -> Nf){-code-} !Nf{-arg1-}
    | N2 !Tm{-source-} !(Nf -> Nf -> Nf -> Nf){-code-} !Nf{-arg1-} Nf{-arg2-}


--------------------- arity of director strings

{-
   ⟦Ap s e1 e2⟧ = \v1 v2 ... vN -> ⟦e1⟧ u1 u2 ... uM (⟦e2⟧ w1 w2 ... wK)

  N = arity s
  M = arityLeft s
  K = arityRight s
-}

arity A = 0
arity (J x) = 1 + arity x
arity (B x) = 1 + arity x
arity (C x) = 1 + arity x
arity (S x) = 1 + arity x

arityLeft A = 0
arityLeft (J x) = arityLeft x
arityLeft (B x) = arityLeft x
arityLeft (C x) = 1 + arityLeft x
arityLeft (S x) = 1 + arityLeft x

arityRight A = 0
arityRight (J x) = arityRight x
arityRight (B x) = 1 + arityRight x
arityRight (C x) = arityRight x
arityRight (S x) = 1 + arityRight x

--------------------- Show instances

instance Show DString where
    show A = "⎵"
    show x = f x where
        f    A  = ""
        f (J x) = '_' : f x
        f (B x) = '╲' : f x
        f (C x) = '╱' : f x
        f (S x) = '^' : f x

instance Show Tm where
    showsPrec p (Ap_ s a b) = showParen (p > 9) $ showsPrec 9 a . ((" " ++ show s ++ " ") ++) . showsPrec 10 b
    showsPrec _ (Int i) = (show i   ++)
    showsPrec _ I       = ("I"      ++)
    showsPrec _ Jit     = ("Jit"    ++)
    showsPrec _ Inline  = ("Inline" ++)
    showsPrec _ Y       = ("Y"      ++)
    showsPrec _ Add     = ("Add"    ++)
    showsPrec _ Sub     = ("Sub"    ++)
    showsPrec _ Mul     = ("Mul"    ++)
    showsPrec _ Abs     = ("Abs"    ++)
    showsPrec _ Sgn     = ("Sgn"    ++)
    showsPrec _ Eq      = ("Eq"     ++)
    showsPrec _ Lt      = ("Lt"     ++)
    showsPrec _ Le      = ("Le"     ++)
    showsPrec _ Mod     = ("Mod"    ++)
    showsPrec _ Sqrt    = ("Sqrt"   ++)

--------------------- lambda terms (used as the surface language)

type Name = Int

data LamT
    = Lam !Name LamT  -- innermost name is always 0; name is the maximum lambda-nesting in the body
    | Var Name
    | App LamT LamT
    | Embed !Tm       -- needed for constants and other extensions
        deriving (Eq, Show)

--------------------- construction of lambda terms (for convenience)

class LamTSource a where toLamT :: a -> LamT

instance LamTSource LamT    where toLamT = id
instance LamTSource Tm      where toLamT = Embed
instance LamTSource Nf      where toLamT = toLamT . source Ap_  -- see later
instance LamTSource Integer where toLamT = toLamT . Int . fromInteger
instance (LamTSource b, a ~ LamT) => LamTSource (a -> b) where
    toLamT f = Lam i t
      where
        i = g t
        t = toLamT $ f $ Var i

        g (App a b) = g a `max` g b
        g (Lam i _) = i + 1
        g _ = 0

infixl 7 ⎵, .@
infixr 0 ⎵$

e ⎵ f = toLamT e `App` toLamT f
(⎵$) = (⎵)
(.@) = (⎵)

instance Num LamT where
    fromInteger = toLamT
    a + b = Add ⎵ a ⎵ b
    a - b = Sub ⎵ a ⎵ b
    a * b = Mul ⎵ a ⎵ b
    abs a = Abs ⎵ a
    signum a = Sgn ⎵ a

let_ a f = f ⎵ case toLamT a of
    a@(Embed Y `App` _) -> Inline ⎵ a
    a@App{}             -> a
    a                   -> Inline ⎵ a

--------------------- compilation to terms

-- paramterized by smart Ap constructors
-- returns the list of free variables reverse-sorted
compileOpen :: (DString -> Tm -> Tm -> Tm) -> LamT -> ([Name], Tm)
compileOpen _ (Var i) = ([i], I)
compileOpen _ (Embed x) = ([], x)
-- collect the free variable lists of the subexpressions and merge them
compileOpen ap (App (compileOpen ap -> (va, ta)) (compileOpen ap -> (vb, tb))) = g id id va vb
  where
    g x y (a:as) (b:bs) | a == b            = g (x . (a:)) (y . S) as bs
    g x y (a:as) bs | a > head (bs ++ [-1]) = g (x . (a:)) (y . C) as bs
    g x y as (b:bs)                         = g (x . (b:)) (y . B) as bs
    g x y [] []                             = (x [], ap (y A) ta tb)
compileOpen ap (Lam i (compileOpen ap -> (v, e)))
    -- the name of the abstracted variable should be the last in the list
    -- if the variable is used in the body, we just delete the name from the free-vars
    | i == last (-1 : v) = (init v, e)
    -- if the variable is unused, we construct
    --     \v1 v2 ... vN v -> id (e v1 v2 ... vN)
    | otherwise          = (v, ap (foldr (const B) (J A) v) I e)

-- compilation of closed lambda terms
compile t = case compileOpen compileAp (toLamT t) of ([], tm) -> tm

{- notes on compilation

- each variable expression is translated to an I term
- each application is translated to an Ap term
  - applications can be optimized away by smart Ap constructors,
    for example,  (\x -> e x)  will be translated to  e
- lambdas with a used variables vanish during the translation 
- each lambda with an unused variable is translated to an Ap + an I term
  - this is usually optimized away by smart Ap constructors

How to recognise hidden lambdas and applications in Tm terms?

Ap s (Ap s' _ _) _

- if (arity s' > arityLeft s) then there are (arity s' - arityLeft s) lambdas around the inner application
- if (arity s' < arritLeft s) then there are (arityLeft s - arity s') applications around the inner application

-}

--------------------- semantics of terms (used only for testing)

data HLamT
    = HLam (HLamT -> HLamT)
    | HApp HLamT HLamT
    | HEmbed LamT

-- semantics (compile t) ~ t   where (~) is eta-convertibility

semantics :: Tm -> LamT
semantics = evalHLamT . evalTerm  where

    evalHLamT (HEmbed t) = t
    evalHLamT (HApp a b) = evalHLamT a ⎵ evalHLamT b
    evalHLamT (HLam f)   = toLamT (evalHLamT . f . HEmbed)

    evalTerm I = HLam id
    evalTerm (Ap_ s a b) = evalDString s (evalTerm a) (evalTerm b)
    evalTerm t = HEmbed (Embed t)

    evalDString    A  a b = HLam a b
    evalDString (J e) a b = HLam \c -> evalDString e  a        b
    evalDString (B e) a b = HLam \c -> evalDString e  a       (b .@ c)
    evalDString (C e) a b = HLam \c -> evalDString e (a .@ c)  b
    evalDString (S e) a b = HLam \c -> evalDString e (a .@ c) (b .@ c)

    HLam f .@ c@(HEmbed Var{}) = f c
    f .@ c = HApp f c

--------------------- normalization and conversion check

{-
           LamT

           | ^
compile ap | | semantics
           v |

           Tm

ap :: DString -> Tm -> Tm -> Tm

normalizeAp :: DString -> Tm -> Tm -> Tm


e : Tm  is in normal form, if (semantics e) is in normal form.
We use eta normal forms.
-}


normalize :: Tm -> Tm
normalize = source normalizeAp . nf

infix 4 ===, ~~

-- conversion check on terms
(===) :: Tm -> Tm -> Bool
a === b = normalize a == normalize b

-- conversion check on lambda terms
a ~~ b = compile a === compile b



------------------------------------------ normalization of terms
{-
Normalization of terms corresponds to the eta normal form of their semantic values.

For example, the normal form of (Ap (B A) I I) is (I), because

   ⟦Ap (B A) I I⟧
= ⟦B A⟧(⟦I⟧, ⟦I⟧)
= \c -> ⟦A⟧(⟦I⟧, ⟦I⟧ c)
= \c -> ⟦I⟧ (⟦I⟧ c)
= \c -> c
= ⟦I⟧

-}

-- options for evaluation
newtype EvalOptions = EvalOptions { reduceAll :: Bool }

normalizeAp = mkAp (EvalOptions { reduceAll = True })
compileAp   = mkAp (EvalOptions { reduceAll = False })

mkAp :: EvalOptions -> DString -> Tm -> Tm -> Tm
mkAp m u (x@(dStringLens m -> (dx, vx))) (dStringLens m -> (dy, vy)) = elim u dx dy mempty
  where
    i = id
    Ap_ _ xa xb = x

    elim    A  (J v)    l !q = mkAp m (tJ q v) xa xb
    elim    A  (B v)    l  q = mkAp m (tB q v) xa (mkAp m (tb q A) xb (vy $ ty q l))   -- TODO: not always
    elim    A  (C v)    l  q = mkAp m (tC q v) (mkAp m (ta q A) xa (vy $ ty q l)) xb   -- TODO: not always
    elim    A  (S v)    l  q | xc <- vy (ty q l), dupable m u xc
                             = mkAp m (tS q v) (mkAp m (ta q A) xa xc) (mkAp m (tb q A) xb xc)
    elim    A     v     l  q = reduceConsts m (tm q A) (vx $ tx q v) (vy $ ty q l)
    elim (J u)    v     l  q = elim u v l (q <> T  i i J J J J J i i)
    elim (B u)    v  (J l) q = elim u v l (q <> T  B B J J B C S i i)
    elim (B u)    v  (B l) q = elim u v l (q <> T  B B B J B C S i B)
    elim (B u)    v  (C l) q = elim u v l (q <> T  B B B J B C S i C)
    elim (B u)    v  (S l) q = elim u v l (q <> T  B B B J B C S i S)
    elim (B u)    v     A  q = elim u v A (q <> T  B B B J B C S i i)
    elim (C u) (J v)    l  q = elim u v l (q <> T  i i J J J J J i i)
    elim (C u) (B v)    l  q = elim u v l (q <> T  i C C B B B B B i)
    elim (C u) (C v)    l  q = elim u v l (q <> T  C i C C C C C C i)
    elim (C u) (S v)    l  q = elim u v l (q <> T  C C C S S S S S i)
    elim (C u)    A     l  q = elim u A l (q <> T  S S C S S S S i i)
    elim (S u) (J v) (J l) q = elim u v l (q <> T  B B J J B C S i i)
    elim (S u) (J v) (B l) q = elim u v l (q <> T  B B B J B C S i B)
    elim (S u) (J v) (C l) q = elim u v l (q <> T  B B B J B C S i C)
    elim (S u) (J v) (S l) q = elim u v l (q <> T  B B B J B C S i S)
    elim (S u) (J v)    A  q = elim u v A (q <> T  B B B J B C S i i)
    elim (S u) (B v) (J l) q = elim u v l (q <> T  B S C B B S S B i)
    elim (S u) (B v) (B l) q = elim u v l (q <> T  B S S B B S S B B)
    elim (S u) (B v) (C l) q = elim u v l (q <> T  B S S B B S S B C)
    elim (S u) (B v) (S l) q = elim u v l (q <> T  B S S B B S S B S)
    elim (S u) (B v)    A  q = elim u v A (q <> T  B S S B B S S B i)
    elim (S u) (C v) (J l) q = elim u v l (q <> T  S B C C S C S C i)
    elim (S u) (C v) (B l) q = elim u v l (q <> T  S B S C S C S C B)
    elim (S u) (C v) (C l) q = elim u v l (q <> T  S B S C S C S C C)
    elim (S u) (C v) (S l) q = elim u v l (q <> T  S B S C S C S C S)
    elim (S u) (C v)    A  q = elim u v A (q <> T  S B S C S C S C i)
    elim (S u) (S v) (J l) q = elim u v l (q <> T  S S C S S S S S i)
    elim (S u) (S v) (B l) q = elim u v l (q <> T  S S S S S S S S B)
    elim (S u) (S v) (C l) q = elim u v l (q <> T  S S S S S S S S C)
    elim (S u) (S v) (S l) q = elim u v l (q <> T  S S S S S S S S S)
    elim (S u) (S v)    A  q = elim u v A (q <> T  S S S S S S S S i)
    elim (S u)    A  (J l) q = elim u A l (q <> T  S S C S S S S i i)
    elim (S u)    A  (B l) q = elim u A l (q <> T  S S S S S S S i B)
    elim (S u)    A  (C l) q = elim u A l (q <> T  S S S S S S S i C)
    elim (S u)    A  (S l) q = elim u A l (q <> T  S S S S S S S i S)
    elim (S u)    A     A  q = elim u A A (q <> T  S S S S S S S i i)

-- accumulator for mkAp
data ReduceAcc = T { ta, tb, tm, tJ, tB, tC, tS, tx, ty :: !(DString -> DString) }

instance Semigroup ReduceAcc where
    T a b c d e f g h i <> T a' b' c' d' e' f' g' h' i'
        = T (a . a') (b . b') (c . c') (d . d') (e . e') (f . f') (g . g') (h . h') (i . i')

instance Monoid ReduceAcc where
    mempty = T id id id id id id id id id



reduceConsts :: EvalOptions -> DString -> Tm -> Tm -> Tm
reduceConsts m u a I | isCCB u = a  -- eta reduction
  where
    isCCB (C x) = isCCB x
    isCCB (B A) = True
    isCCB _     = False
reduceConsts m A I b = b
reduceConsts m A z x | p z = source Ap_ . nf $ Ap_ A z x
  where
    p Y = reduceAll m
    p Jit = reduceAll m
    p Sqrt = True
    p _ = False
reduceConsts m A a b | isNf a && isNf b = source Ap_ . nf $ Ap_ A a b
  where
    isNf (Ap_ A x y) = p x && isNf y
      where
        p Add = True
        p Sub = True
        p Mod = True
        p Eq = True
        p Lt = True
        p Le = True
        p _ = False
    isNf Ap_{} = False
    isNf _ = True

reduceConsts m A (Ap_ (S (B A)) I (Ap_ (B (C (B A))) I I)) (Ap_ (S A) I I) = Y
reduceConsts m A Add (Int 0) = I
reduceConsts m (C A) Add (Int 0) = I
reduceConsts m (C A) Sub (Int 0) = I
reduceConsts m (S A) I (Ap_ (B A) (Ap_ (S A) I I) (Ap_ (C (B A)) I (Ap_ (S A) I I))) = Y
reduceConsts m (B A) (Ap_ (S A) I I) (Ap_ (C (B A)) I (Ap_ (S A) I I)) = Y

reduceConsts m s (Ap_ s' Inline x) y = mkAp m s (mkAp m s' I x) y
reduceConsts m s Inline x | arityLeft s > 0 = reduceConsts m s I x
reduceConsts m s Inline I = reduceConsts m s I I
reduceConsts m s Inline x@(Ap_ _ Inline _) = reduceConsts m s I x
reduceConsts m u I e@(Ap_ v_ z1 z2) = skipBs id id id u v_    -- reduce I under lambda
  where
    skipBs y t q (J x)    v  = skipBs  y       (t .  J) (q . J) x v
    skipBs y t q (B x) (J v) = skipBs (y . kb) (t . kb) (q . J) x v
    skipBs y t q (B x) (B v) = skipBs (y . kb) (t . kb) (q . B) x v
    skipBs y t q (B x) (C v) = skipBs (y . kb) (t . kb) (q . C) x v
    skipBs y t q (B x) (S v) = skipBs (y . kb) (t . kb) (q . S) x v
    skipBs y t q (B x)    A  = skipBs (y . kb) (t . kb)  q      x A
    skipBs y t q    x     v  = case y x of A -> Ap_ (q v) z1 z2; _ -> Ap_ (t x) I e
reduceConsts m u I a = skipBs id u    -- reduce I under lambda
  where
    skipBs y (J x) = skipBs (y . J) x
    skipBs y (B x) = skipBs (y . kb) x
    skipBs y t = case y t of A -> a; u -> Ap_ u I a
reduceConsts m u a b = Ap_ u a b

kb A = A
kb v = B v


dStringLens :: EvalOptions -> Tm -> (DString, DString -> Tm)
dStringLens m (Ap_ u a b) = (u, \u -> reduceConsts m u a b)
dStringLens m t = (A, \A -> t)

dupable (reduceAll -> True) u _ = True
dupable m u (Ap_ A Inline _) = True
dupable m u Ap_{} = False
dupable m u x | arity x > arityRight u = True
  where
    arity :: Tm -> Int
    arity (Ap_ s a b) = 0 -- TODO
    arity Int{} = maxBound
    arity I = maxBound   -- the arity of I is 1, but the computation of I is dupable
    arity Y = 1
    arity Jit = 1
    arity Inline = 2 -- ?
    arity Sqrt = 1
    arity _ = 2
dupable m u x = False

----------------------- evaluation of saturated terms

{-
        LamT

        | ^
compile | | semantics
        v |

        Tm

        | ^ source Ap_
     nf | | source compileAp_
        v | source normalizeAp_

        Nf

-}


source ap = \case
    NInt i -> Int i
    N0 a _ -> a
    N1 a _ b -> ap A a (source ap b)
    N2 (Ap_ (C (B s)) I I) _ b c -> ap s (source ap b) (source ap c)
    N2 a _ b c -> ap A (ap A a (source ap b)) (source ap c)

-- fake Eq instance: don't compare cached Nf values
instance Eq Nf where
    a == b = True

instance Show Nf where
    show = show . source Ap_

ap1 :: Nf -> Nf -> Nf
ap1 (N0 _ f) x = f x
ap1 (N1 _ f a) x = f a x
ap1 (N2 _ f a b) x = f a b x

traceThis a = trace (take 500 $ show a) a

nf :: Tm -> Nf
nf = \case
    Ap t s a b -> t
    Int i -> NInt i
    I     -> iid
    Y     -> N0 Y \x -> let y = ap1 x y in y
    Jit   -> N0 Jit \x -> traceThis . nf . source compileAp . traceThis $ x
    Inline-> N0 Inline \x -> x
    Sqrt  -> N0 Sqrt \(NInt i) -> NInt (iSqrt i)
    Add   -> N0 Add (N1 Add \(NInt x) (NInt y) -> NInt (x + y))
    Sub   -> N0 Sub (N1 Sub \(NInt x) (NInt y) -> NInt (x - y))
    Mod   -> N0 Mod (N1 Mod \(NInt x) (NInt y) -> NInt (x `mod` y))
    Eq    -> N0 Eq (N1 Eq \(NInt x) (NInt y) -> mkBool (x == y))
    Lt    -> N0 Lt (N1 Lt \(NInt x) (NInt y) -> mkBool (x < y))
    Le    -> N0 Le (N1 Le \(NInt x) (NInt y) -> mkBool (x <= y))
    x -> error $ show x

mkBool True  = true
mkBool False = false

true  = N0 sBJii (N1 sBJii \x _ -> x)
false = N0 (Ap false (J A) I I) \_ -> iid

sBJii = Ap true (B (J A)) I I
iid = N0 I \x -> x

pattern Ap_ s a b <- Ap _ s a b
  where Ap_ s a b = Ap (mkN s a b) s a b

pattern M s a b <- (ff -> Just (s, a, b))

ff (Ap_ s (Ap_ s' Inline a) b) | arityLeft s' == 0 = Just (s, a, b)
ff (Ap_ s a b) = Just (s, a, b)
ff _ = Nothing


mkN s Inline x = nf $ compileAp s I x

mkN (J A) I I = false
mkN (J A) I x = N1 sBJii (\x _ -> x) (nf x)
mkN (J (C A)) (M (C A) I a@(nf -> x)) b@(nf -> y) = c where
    c = N0 s (N1 s \_ d -> ap1 (ap1 d x) y)
    s = Ap c (J (C A)) (Ap_ (C A) I a) b
mkN (C A) I I = c where c = N0 (Ap c (C A) I I) \x -> ap1 x iid
mkN (C A) I x = N1 (source Ap_ $ mkN (B (C A)) I I) (\x y -> ap1 y x) (nf x)
mkN (C (B (C A))) (M (B (C A)) I I) I = c where
    c = N0 s (N1 s (N2 s \a b c -> ap1 (ap1 c a) b))
    s = Ap c (C (B (C A))) (Ap_ (B (C A)) I I) I
mkN (B (J A)) I I = true
mkN (B (C A)) I I = c where c = N0 s (N1 s \x y -> ap1 y x); s = Ap c (B (C A)) I I
--mkN (B A) I b@(nf -> x) = error $ show x
--mkN (S A) I b@(nf -> x) = c where c = N1 (Ap ? (B (S A)) I I) (\x y -> ap1 y (ap1 x y)) x

mkN A x y = ap1 (nf x) (nf y)
mkN s@(q1 -> m) x y = N2 (rr s) m (nf x) (nf y)

rr s = Ap_ (C (B s)) I I

q1 = \case
    (J (C A)) -> gJC
    (J A) -> gJ
    (J s@(q1 -> !m)) | !x <- rr s -> \a b c -> N2 x m a b
    (B (J (C A))) -> gBJC
    (B (B A)) -> gBB
    (B (B (C A))) -> gBBC
    (B (C A)) -> gBC
    (B (C (B (J (C A))))) -> gBCBJC
    (B (S A)) -> gBS
    (B (S (J (C A)))) -> gBSJC
    (B (S (B A))) -> gBSB
    (B (S (C (B A)))) -> gBSCB
    (B A) -> gB
    (B s@(q1 -> !m)) | !x <- rr s -> \a b c -> N2 x m a (ap1 b c)
    (C (B A)) -> gCB
    (C (B (J (C A)))) -> gCBJC
    (C (B (C A))) -> gCBC
    (C (B (C (B A)))) -> gCBCB
    (C (B (S A))) -> gCBS
    (C (B (S (B A)))) -> gCBSB
    (C (C A)) -> gCC
    (C (C (C (C A)))) -> gCCCC
    (C (S A)) -> gCS
    (C (S (C (S A)))) -> gCSCS
    (C A) -> gC
    (C s@(q1 -> !m)) | !x <- rr s -> \a b c -> N2 x m (ap1 a c) b
    (S (J (C A))) -> gSJC
    (S (B A)) -> gSB
    (S (B (S (B A)))) -> gSBSB
    (S (C (S A))) -> gSCS
    (S (S A)) -> gSS
    (S A) -> gS
    (S s@(q1 -> !m)) | !x <- rr s -> \a b c -> N2 x m (ap1 a c) (ap1 b c)
    x -> error $ show x
  where
    gJ a b c = ap1 a b
    gB a b c = ap1 a (ap1 b c)
    gC a b c = ap1 (ap1 a c) b
    gS a b c = ap1 (ap1 a c) (ap1 b c)
    gJC a b c = N2 sC gC a b
    gBB a b c = N2 sB gB a (ap1 b c)
    gBC a b c = N2 sC gC a (ap1 b c)
    gBS a b c = N2 sS gS a (ap1 b c)
    gCB a b c = N2 sB gB (ap1 a c) b
    gCC a b c = N2 sC gC (ap1 a c) b
    gCS a b c = N2 sS gS (ap1 a c) b
    gSB a b c = N2 sB gB (ap1 a c) (ap1 b c)
    gSS a b c = N2 sS gS (ap1 a c) (ap1 b c)
    gBJC a b c = N2 sJC gJC a (ap1 b c)
    gBBC a b c = N2 sBC gBC a (ap1 b c)
    gBCB a b c = N2 sCB gCB a (ap1 b c)
    gBSB a b c = N2 sSB gSB a (ap1 b c)
    gCBC a b c = N2 sBC gBC (ap1 a c) b
    gCBS a b c = N2 sBS gBS (ap1 a c) b
    gCCC a b c = N2 sCC gCC (ap1 a c) b
    gSJC a b c = N2 sJC gJC (ap1 a c) (ap1 b c)
    gSCB a b c = N2 sCB gCB (ap1 a c) (ap1 b c)
    gSCS a b c = N2 sCS gCS (ap1 a c) (ap1 b c)
    gBSJC a b c = N2 sSJC gSJC a (ap1 b c)
    gBSCB a b c = N2 sSCB gSCB a (ap1 b c)
    gCBJC a b c = N2 sBJC gBJC (ap1 a c) b
    gCBCB a b c = N2 sBCB gBCB (ap1 a c) b
    gCBSB a b c = N2 sBSB gBSB (ap1 a c) b
    gCCCC a b c = N2 sCCC gCCC (ap1 a c) b
    gCSCS a b c = N2 sSCS gSCS (ap1 a c) b
    gSBSB a b c = N2 sBSB gBSB (ap1 a c) (ap1 b c)
    gBCBJC a b c = N2 sCBJC gCBJC a (ap1 b c)

    sB = Ap_ (C (B (B A))) I I
    sC = Ap_ (C (B (C A))) I I
    sS = Ap_ (C (B (S A))) I I
    sJC = Ap_ (C (B (J (C A)))) I I
    sBC = Ap_ (C (B (B (C A)))) I I
    sBS = Ap_ (C (B (B (S A)))) I I
    sCB = Ap_ (C (B (C (B A)))) I I
    sCC = Ap_ (C (B (C (C A)))) I I
    sCS = Ap_ (C (B (C (S A)))) I I
    sSB = Ap_ (C (B (S (B A)))) I I
    sBJC = Ap_ (C (B (B (J (C A))))) I I
    sBCB = Ap_ (C (B (B (C (B A))))) I I
    sBSB = Ap_ (C (B (B (S (B A))))) I I
    sCCC = Ap_ (C (B (C (C (C A))))) I I
    sSCB = Ap_ (C (B (S (C (B A))))) I I
    sSCS = Ap_ (C (B (S (C (S A))))) I I
    sSJC = Ap_ (C (B (S (J (C A))))) I I
    sCBJC = Ap_ (C (B (C (B (J (C A)))))) I I

------------------------------------ tests

churchNum i = \f x -> iterate (f ⎵) x !! i

cNum = Y ⎵ \f i g n -> Eq ⎵ 0 ⎵ i ⎵ n ⎵$ g ⎵$ f ⎵ (i - 1) ⎵ g ⎵ n

fusion = do
    stream <- \next s c -> c ⎵ next ⎵ s
    done  <-      \d y s -> d
    yield <- \a st d y s -> y ⎵ a ⎵ st
    skip  <-   \st d y s -> s ⎵ st
    mapS <- \f str -> str ⎵ \next0 s0 -> do
                next <- \s -> next0 ⎵ s
                    ⎵ done
                    ⎵ skip
                    ⎵ (\x -> yield ⎵ (f ⎵ x))
                stream ⎵ next ⎵ s0
    mapS ⎵ id ⎵ (mapS ⎵ id ⎵ (stream ⎵ (\s -> done) ⎵ 2)) -- ⎵ 3 --(\n s -> n ⎵ s)

ifThenElse True  t _ = t
ifThenElse False _ f = f

jitTest j n = go ⎵ n ⎵ ms ⎵ 0
  where
    go = \n -> Y ⎵ \go ops acc -> ops ⎵ acc ⎵$ \op ops -> go ⎵ ops ⎵$ go2 ⎵ (op ⎵ acc) ⎵ acc ⎵ n
    go2 = \op -> Y ⎵$ (if j then (Jit ⎵) else (id ⎵)) \go acc n -> Eq ⎵ 0 ⎵ n ⎵ acc ⎵$ go ⎵ (op ⎵ acc ⎵ n) ⎵ (n - 1)
    ms = cons ⎵ (\m x y -> (y + x) - m) ⎵$
         cons ⎵ (\a x y -> a + (x + y)) ⎵$
         cons ⎵ (\_ a _ -> a) ⎵ nil

    cons x xs = \n c -> c ⎵ x ⎵ xs
    nil       = \n c -> n

timed = print . normalize . compile


test = and tests
tests =
    [ id ~~ id ⎵ id
    , id ~~ (\x -> id ⎵ x)
    , churchNum 2 ⎵ churchNum 2 ~~ churchNum 4
    , churchNum 2 ⎵ churchNum 2 ⎵ churchNum 2 ~~ churchNum 16
    , churchNum 2 ⎵ churchNum 2 ⎵ churchNum 2 ⎵ churchNum 2 ⎵ (1 +) ⎵ 0 ~~ 65536
    , (\a b -> (\x -> \c -> c ⎵ (x ⎵ \a b -> a) ⎵ (x ⎵ \a b -> b)) ⎵ \c -> c ⎵ a ⎵ b) ~~ (\a b c -> c ⎵ a ⎵ b)

    , (\f -> (\x -> f ⎵ (x ⎵ x)) ⎵ (\x -> f ⎵ (x ⎵ x))) ~~ (\x -> x ⎵ (Y ⎵ x)) 
    , (\f -> (\x -> x ⎵ x) ⎵ (\x -> f ⎵ (x ⎵ x))) ~~ Y
    , (\m f -> m ⎵ (\x -> f ⎵ (m ⎵ x))) ⎵ (\x -> x ⎵ x) ~~ Y

    , nthPrimeReference 10 == 31
    , nthPrime 10 == 31
    , nthPrimeFused 10 == 31
    , nthPrimeDirector ⎵ 10 ~~ 31
    , nthPrimeFusedDirector ⎵ 10 ~~ 31
    , compile (1 + 1) == compile 2
    , compile (\x -> (x + x) ⎵ ((\y -> 3) ⎵ x)) == Ap_ (C A) (Ap_ (S A) Add I) (Int 3)
    , show (compile $ nthPrimeDirector 10) == pOpt
    , show (compile $ nthPrimeFusedDirector ⎵ 10) == pOptFused
    ]

pOpt = "Y ⎵ (I ╱ Y ╲╲╱ (Eq ⎵ 0 ╲^╱╲ (I ╱╲ (Sub ╱ 1)))) ⎵ 10 ⎵ (I ╱ 2 _╱ (Inline ⎵ (Y ⎵ (I ╱ 3 ╲_╱ (Y ╲ (I ╱ (Inline ⎵ (I ╲_ I)) ╲╲╱ (Y ╲ (I ╱ (Inline ⎵ (I ╲_ I)) ╲╲╱ (Eq ⎵ 0 ╲╲ Mod ╱╱ (Inline ⎵ (I _ I)) ╱╱ (Inline ⎵ (I ╲_ I)) ╱╲╱╲ I ╱╱╱╱ (Inline ⎵ (I _ I)))) ╲^ (Y ╲ (I ╱ (Inline ⎵ (I ╲_ I)) ╲╲╱ (Lt ╲╱ I ╱╱ (Inline ⎵ (I ╲_ I)) ^^ (Eq ╲╱ I) ╲ Sqrt ╱╲^╲ (I ╲╱ I ╲╱╲_╱ I) ╱╱╱╱ (Inline ⎵ (I ╲_ I)))) ╲╱ I) ╱╲^╲ (I ╲╱ I ╲╱╲_╱ I) ╱^╱^ I)) ╱ (Y ⎵ (I ╲╱ I ╲^_╱ (I ╱╲ (Add ⎵ 2))) ⎵ 5))))))"

pOptFused = "Y ⎵ (I ╲╲╱ (Eq ⎵ 0 ╲^╱╲ (I ╱╲ (Sub ╱ 1)))) ⎵ 10 ⎵ (I ╱ 2 ╱ (Y ⎵ (I ╱ 3 ╲╱ (Y ╲ (Y ╲ (I ╲╲╱ (Le ╲╱ Sqrt ^╲^╲ (Eq ⎵ 0 ╲╲ Mod ╱╱ (I _ I) ╱╲╱╲ I) ╱╱╱╱ (I ╲_ I))) ╲╱ I ╱^ (I ╲╱ I ╱╲╱ I) ╱╱ I ╱╲^ (I ╱╲ (Add ⎵ 2))) ╱ 5))))"

------------------------------------ main function

main = getArgs Prelude.>>= \case
    ["test"]  -> print test

    ["r", n]  -> print $ nthPrimeReference (read n)
    [n]       -> print $ nthPrime (read n)
    ["f", n]  -> print $ nthPrimeFused (read n)
    ["d", n]  -> timed $ nthPrimeDirector ⎵ fromInteger (read n)
    ["fd", n] -> timed $ nthPrimeFusedDirector ⎵ fromInteger (read n)

    ["j1", n] -> timed $ jitTest False ⎵ fromInteger (read n)
    ["j2", n] -> timed $ jitTest True ⎵ fromInteger (read n)
    ["c", read -> n] -> timed $ toLamT $ mkBool $ churchNum (2 * n) ⎵ churchNum 2 ~~ churchNum n ⎵ churchNum 4

    args -> error $ "Unknown arguments: " ++ show args

