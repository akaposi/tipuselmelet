{-# OPTIONS --prop #-}

module seminar20200623_red where

---------------------------------------------------------------------
-- library
---------------------------------------------------------------------

open import Agda.Primitive

infixr 5 _,∃_
infixr 5 _,Σ_
infixr 4 _×_ _×p_ _×sp_
infix 5 _≡_

record Σ {ℓ ℓ'} (A : Set ℓ) (B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,Σ_
  field
    proj₁ : A
    proj₂ : B proj₁
open Σ public
_×_ : ∀{ℓ ℓ'} → Set ℓ → Set ℓ' → Set (ℓ ⊔ ℓ')
A × B = Σ A λ _ → B

record Σp {ℓ ℓ'} (A : Prop ℓ) (B : A → Prop ℓ') : Prop (ℓ ⊔ ℓ') where
  constructor _,Σ_
  field
    proj₁ : A
    proj₂ : B proj₁
open Σp public
_×p_ : ∀{ℓ ℓ'} → Prop ℓ → Prop ℓ' → Prop (ℓ ⊔ ℓ')
A ×p B = Σp A λ _ → B

data ∃ {i}{j}(A : Set i)(B : A → Prop j) : Prop (i ⊔ j) where
  _,∃_ : (a : A) → B a → ∃ A B

with∃ : ∀{i j k}{A : Set i}{B : A → Prop j}{C : Prop k} → ∃ A B → (A → C) → C
with∃ (a ,∃ _) f = f a

record Σsp {ℓ ℓ'} (A : Set ℓ) (B : A → Prop ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,Σ_
  field
    proj₁ : A
    proj₂ : B proj₁
open Σsp public
_×sp_ : ∀{ℓ ℓ'} → Set ℓ → Prop ℓ' → Set (ℓ ⊔ ℓ')
A ×sp B = Σsp A λ _ → B

data 𝟘p : Prop where

record 𝟙p : Prop where
  constructor *

data _≡_ {ℓ}{A : Set ℓ} (x : A) : A → Prop ℓ where
  refl : x ≡ x

postulate
  ℕ : Set

ap : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
ap f refl = refl

,Σsp= : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'}{a a' : A} → a ≡ a' → {b : B a}{b' : B a'} → _≡_ {A = Σsp A B} (a ,Σ b) (a' ,Σ b')
,Σsp= refl = refl

postulate
  transp    : ∀{i j}{A : Set i}(B : A → Set j){a a' : A} → a ≡ a' → B a → B a'
  transp⁻¹  : ∀{i j}{A : Set i}(B : A → Set j){a a' : A} → a ≡ a' → B a' → B a
  transpp   : ∀{i j}{A : Set i}(B : A → Prop j){a a' : A} → a ≡ a' → B a → B a'
  transpp⁻¹ : ∀{i j}{A : Set i}(B : A → Prop j){a a' : A} → a ≡ a' → B a' → B a

module generalInfinitary where

  -- reduction of an infinitary IIT to indexed inductive types, Andras' way

  data Con₀ : Set
  data Ty₀ : Set
  data Con₀ where
    ∙₀ : Con₀
    _▷₀_ : Con₀ → Ty₀ → Con₀
  data Ty₀ where
    Π₀ : Con₀ → (ℕ → Ty₀) → Ty₀

  Con₁ : Con₀ → Prop
  Ty₁  : Con₀ → Ty₀ → Prop
  Con₁ ∙₀ = 𝟙p
  Con₁ (Γ₀ ▷₀ A₀) = Con₁ Γ₀ ×p Ty₁ Γ₀ A₀
  Ty₁ Γ₀ (Π₀ Γ₀' F₀) = Γ₀ ≡ Γ₀' ×p Con₁ Γ₀ ×p ((n : ℕ) → Ty₁ Γ₀ (F₀ n))

  postulate
    Con : Set
    Ty : Con → Set
    ∙ : Con
    _▷_ : (Γ : Con) → Ty Γ → Con
    Π : (Γ : Con) → (ℕ → Ty Γ) → Ty Γ

  Con₂ : Con₀ → Con → Prop
  Ty₂  : Ty₀  → (Γ : Con) → Ty Γ → Prop
  Con₂ ∙₀ Γ = Γ ≡ ∙
  Con₂ (Γ₀ ▷₀ A₀) Δ =
    ∃ Con λ Γ → ∃ (Ty Γ) λ A → Δ ≡ (Γ ▷ A) ×p Con₂ Γ₀ Γ ×p Ty₂ A₀ Γ A
  Ty₂ (Π₀ Γ₀ F₀) Γ A =
    Con₂ Γ₀ Γ ×p ∃ (ℕ → Ty Γ) λ F → A ≡ Π Γ F ×p ((n : ℕ) → Ty₂ (F₀ n) Γ (F n))

  isContr : Set → Set
  isContr A = Σsp A λ a → (a' : A) → a ≡ a'

  Con₃ : (Γ₀ : Con₀) → Con₁ Γ₀ → isContr (Σsp Con λ Γ → Con₂ Γ₀ Γ)
  Ty₃  : ∀{Γ₀}(A₀ : Ty₀) → Con₁ Γ₀ → Ty₁ Γ₀ A₀ → isContr (Σ Con λ Γ → Σsp (Ty Γ) λ A → Con₂ Γ₀ Γ ×p Ty₂ A₀ Γ A)
  Con₃ ∙₀ Γ₁ = (∙ ,Σ refl) ,Σ λ { (_ ,Σ refl) → refl }
  Con₃ (Γ₀ ▷₀ A₀) (Γ₁ ,Σ A₁) with Ty₃ A₀ Γ₁ A₁
  ... | ((Γ ,Σ A ,Σ Γ₂ ,Σ A₂) ,Σ uniΓA) =
    ((Γ ▷ A) ,Σ Γ ,∃ A ,∃ refl ,Σ Γ₂ ,Σ A₂) ,Σ
    λ { (Δ' ,Σ Γ' ,∃ A' ,∃ refl ,Σ Γ₂' ,Σ A₂') → ,Σsp= (ap (λ z → proj₁ z ▷ proj₁ (proj₂ z)) (uniΓA (Γ' ,Σ A' ,Σ Γ₂' ,Σ A₂'))) }
  Ty₃ {Γ₀} (Π₀ Γ₀' F₀) Γ₁ (e ,Σ Γ₁ ,Σ F₁) with Con₃ Γ₀ Γ₁
  ... | ((Γ ,Σ Γ₂) ,Σ uniΓ) = (
    Γ ,Σ
    Π Γ (λ n → transp⁻¹ Ty (ap proj₁ (uniΓ (_ ,Σ proj₁ (proj₂ (proj₂ (proj₁ (Ty₃ (F₀ n) Γ₁ (F₁ n)))))))) (proj₁ (proj₂ (proj₁ (Ty₃ (F₀ n) Γ₁ (F₁ n)))))) ,Σ
    Γ₂ ,Σ
    transpp (λ z → Con₂ z Γ) e Γ₂ ,Σ
    (λ n → transp⁻¹ Ty (ap proj₁ (uniΓ (_ ,Σ proj₁ (proj₂ (proj₂ (proj₁ (Ty₃ (F₀ n) Γ₁ (F₁ n)))))))) (proj₁ (proj₂ (proj₁ (Ty₃ (F₀ n) Γ₁ (F₁ n)))))) ,∃
    refl ,Σ
    λ n → {! (proj₂ (proj₂ (proj₂ (proj₁ (Ty₃ (F₀ n) Γ₁ (F₁ n))))))!}) ,Σ
    {!!}

  {-
  Con₃ : (Γ₀ : Con₀) → Con₁ Γ₀ → Σsp Con λ Γ → Con₂ Γ₀ Γ
  Ty₃  : ∀{Γ₀}(A₀ : Ty₀) → Con₁ Γ₀ → Ty₁ Γ₀ A₀ →
    Σ Con λ Γ → Σsp (Ty Γ) λ A → Con₂ Γ₀ Γ ×p Ty₂ A₀ Γ A
  Con₃ ∙₀ Γ₁ = ∙ ,Σ refl
  Con₃ (Γ₀ ▷₀ A₀) (Γ₁ ,Σ A₁) with Ty₃ A₀ Γ₁ A₁
  ... | (Γ ,Σ A ,Σ Γ₂ ,Σ A₂) = (Γ ▷ A) ,Σ Γ ,∃ A ,∃ refl ,Σ Γ₂ ,Σ A₂
  Ty₃ {Γ₀} (Π₀ Γ₀' F₀) Γ₁ (e ,Σ Γ₁ ,Σ F₁) with Con₃ Γ₀ Γ₁
  ... | (Γ ,Σ Γ₂) = Γ ,Σ Π Γ (λ n → {!proj(Ty₃ (F₀ n) ? ?)!}) ,Σ {!!}
  -}
  {-
  Con₃ : (Γ₀ : Con₀) → Con₁ Γ₀ → Σsp Con λ Γ → Con₂ Γ₀ Γ
  Ty₃  : ∀{Γ₀}(A₀ : Ty₀) → Con₁ Γ₀ → Ty₁ Γ₀ A₀ → (ΓΓ₂ : Σsp Con (λ Γ → Con₂ Γ₀ Γ)) →
    Σsp (Ty (proj₁ ΓΓ₂)) λ A → Ty₂ A₀ (proj₁ ΓΓ₂) A
  Con₃ ∙₀ Γ₁ = ∙ ,Σ refl
  Con₃ (Γ₀ ▷₀ A₀) (Γ₁ ,Σ A₁) with Con₃ Γ₀ Γ₁ | Ty₃ A₀ Γ₁ A₁ (Con₃ Γ₀ Γ₁)
  ... | (Γ ,Σ Γ₂) | (A ,Σ A₂)= (Γ ▷ A) ,Σ Γ ,∃ A ,∃ refl ,Σ Γ₂ ,Σ A₂
  Ty₃ (Π₀ Γ₀ F₀) Γ₁ (e ,Σ Γ₁' ,Σ F₁) (Γ ,Σ Γ₂) = {!!}
  -}

module simpleFinitary where

  -- only the simple elimination principle: then everything is easier,
  -- but it still doesn't seem to work with the relation in Prop. this
  -- is the method of Fredrik Forsberg

  data Con₀ : Set
  data Ty₀ : Set
  data Con₀ where
    ∙₀ : Con₀
    _▷₀_ : Con₀ → Ty₀ → Con₀
  data Ty₀ where
    Π₀ : Con₀ → Ty₀ → Ty₀ → Ty₀

  module _
    {i}{j}
    (Con₀ᴾ : Con₀ → Set i)
    (Ty₀ᴾ : Ty₀ → Set j)
    (∙₀ᴾ : Con₀ᴾ ∙₀)
    (_▷₀ᴾ_ : {Γ₀ : Con₀} → Con₀ᴾ Γ₀ → {A₀ : Ty₀} → Ty₀ᴾ A₀ → Con₀ᴾ (Γ₀ ▷₀ A₀))
    (Π₀ᴾ : {Γ₀ : Con₀} → Con₀ᴾ Γ₀ → {A₀ : Ty₀} → Ty₀ᴾ A₀ → {A₀' : Ty₀} → Ty₀ᴾ A₀' → Ty₀ᴾ (Π₀ Γ₀ A₀ A₀'))
    where
    
    indCon₀ : (Γ₀ : Con₀) → Con₀ᴾ Γ₀
    indTy₀  : (A₀ : Ty₀)  → Ty₀ᴾ  A₀
    indCon₀ ∙₀ = ∙₀ᴾ
    indCon₀ (Γ₀ ▷₀ A₀) = indCon₀ Γ₀ ▷₀ᴾ indTy₀ A₀
    indTy₀ (Π₀ Γ₀ A₀ A₀') = Π₀ᴾ (indCon₀ Γ₀) (indTy₀ A₀) (indTy₀ A₀')

  -- Szumi's idea to define equality by recursion and then we can
  -- define transport for Set-valued predicates

  _≡C_ : Con₀ → Con₀ → Prop
  _≡T_ : Ty₀  → Ty₀  → Prop
  ∙₀ ≡C ∙₀ = 𝟙p
  (Γ₀ ▷₀ A₀) ≡C (Δ₀ ▷₀ B₀) = Γ₀ ≡C Δ₀ ×p A₀ ≡T B₀
  _ ≡C _ = 𝟘p
  Π₀ Γ₀ A₀ A₀' ≡T Π₀ Δ₀ B₀ B₀' = Γ₀ ≡C Δ₀ ×p A₀ ≡T B₀  ×p A₀' ≡T B₀'

  reflC : {Γ₀ : Con₀} → Γ₀ ≡C Γ₀
  reflT : {A₀ : Ty₀} → A₀ ≡T A₀
  reflC {∙₀} = *
  reflC {Γ₀ ▷₀ A₀} = reflC {Γ₀} ,Σ reflT {A₀}
  reflT {Π₀ Γ₀ A₀ B₀} = reflC {Γ₀} ,Σ reflT {A₀} ,Σ reflT {B₀}

  transpC : ∀{i}(P : Con₀ → Set i){Γ₀ Δ₀ : Con₀} → Γ₀ ≡C Δ₀ → P Γ₀ → P Δ₀
  transpT : ∀{i}(P : Ty₀  → Set i){A₀ B₀ : Ty₀}  → A₀ ≡T B₀ → P A₀ → P B₀
  transpC P {∙₀} {∙₀} e u = u
  transpC P {Γ₀ ▷₀ A₀} {Δ₀ ▷₀ B₀} (eC ,Σ eT) u = transpC (λ Γ → P (Γ ▷₀ B₀)) eC (transpT (λ A → P (Γ₀ ▷₀ A)) eT u)
  transpT P {Π₀ Γ₀ A₀ A₀'} {Π₀ Δ₀ B₀ B₀'} (eC ,Σ eT ,Σ eT') u = transpC (λ Γ → P (Π₀ Γ B₀ B₀')) eC (transpT (λ A → P (Π₀ Γ₀ A B₀')) eT (transpT (λ A → P (Π₀ Γ₀ A₀ A)) eT' u))

  transpCP : ∀{i}(P : Con₀ → Prop i){Γ₀ Δ₀ : Con₀} → Γ₀ ≡C Δ₀ → P Γ₀ → P Δ₀
  transpTP : ∀{i}(P : Ty₀  → Prop i){A₀ B₀ : Ty₀}  → A₀ ≡T B₀ → P A₀ → P B₀
  transpCP P {∙₀} {∙₀} e u = u
  transpCP P {Γ₀ ▷₀ A₀} {Δ₀ ▷₀ B₀} (eC ,Σ eT) u = transpCP (λ Γ → P (Γ ▷₀ B₀)) eC (transpTP (λ A → P (Γ₀ ▷₀ A)) eT u)
  transpTP P {Π₀ Γ₀ A₀ A₀'} {Π₀ Δ₀ B₀ B₀'} (eC ,Σ eT ,Σ eT') u = transpCP (λ Γ → P (Π₀ Γ B₀ B₀')) eC (transpTP (λ A → P (Π₀ Γ₀ A B₀')) eT (transpTP (λ A → P (Π₀ Γ₀ A₀ A)) eT' u))

  JC : ∀{i}{Γ₀ : Con₀}(P : (Δ₀ : Con₀) → Γ₀ ≡C Δ₀ → Set i) → P Γ₀ reflC → {Δ₀ : Con₀}(e : Γ₀ ≡C Δ₀) → P Δ₀ e
  JC {_}{Γ₀} P w e = transpC (λ Δ₀ → (e : Γ₀ ≡C Δ₀) → P Δ₀ e) e (λ _ → w) e

  symC : {Γ₀ Δ₀ : Con₀} → Γ₀ ≡C Δ₀ → Δ₀ ≡C Γ₀
  symC {Γ₀}{Δ₀} e = transpCP (λ Δ₀ → Δ₀ ≡C Γ₀) e reflC

  Con₁ : Con₀ → Prop
  Ty₁  : Con₀ → Ty₀ → Prop
  Con₁ ∙₀ = 𝟙p
  Con₁ (Γ₀ ▷₀ A₀) = Con₁ Γ₀ ×p Ty₁ Γ₀ A₀
  Ty₁ Γ₀ (Π₀ Γ₀' A₀ B₀) = (Γ₀ ≡C Γ₀') ×p Con₁ Γ₀ ×p Ty₁ Γ₀ A₀ ×p Ty₁ (Γ₀' ▷₀ A₀) B₀

  Con : Set
  Con = Σsp Con₀ Con₁
  Ty : Con → Set
  Ty (Γ₀ ,Σ Γ₁)  = Σsp Ty₀ (Ty₁ Γ₀)
  ∙ : Con

  ∙ = ∙₀ ,Σ *
  _▷_ : (Γ : Con) → Ty Γ → Con
  (Γ₀ ,Σ Γ₁) ▷ (A₀ ,Σ A₁) = (Γ₀ ▷₀ A₀) ,Σ (Γ₁ ,Σ A₁)
  Π : (Γ : Con)(A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ
  Π (Γ₀ ,Σ Γ₁) (A₀ ,Σ A₁) (A₀' ,Σ A₁') = (Π₀ Γ₀ A₀ A₀') ,Σ reflC ,Σ Γ₁ ,Σ (A₁ ,Σ A₁')

  module elimToSet
    {i}{j}
    (Conᴰ : Con → Set i)
    (Tyᴰ : (Γ : Con) → Ty Γ → Set j)
    (∙ᴰ : Conᴰ ∙)
    (_▷ᴰ_ : {Γ : Con}(Γᴰ : Conᴰ Γ){A : Ty Γ}(Aᴰ : Tyᴰ Γ A) → Conᴰ (Γ ▷ A))
    (Πᴰ : {Γ : Con}(Γᴰ : Conᴰ Γ){A : Ty Γ}(Aᴰ : Tyᴰ Γ A){A' : Ty (Γ ▷ A)}(A'ᴰ : Tyᴰ (Γ ▷ A) A') → Tyᴰ Γ (Π Γ A A'))
    where

    Con₀ᴾ : Con₀ → Set i
    Con₀ᴾ Γ₀ = (Γ₁ : Con₁ Γ₀) → Conᴰ (Γ₀ ,Σ Γ₁)
    Ty₀ᴾ : Ty₀ → Set j
    Ty₀ᴾ A₀ = {Γ₀ : Con₀}{Γ₁ : Con₁ Γ₀}(A₁ : Ty₁ Γ₀ A₀) → Tyᴰ (Γ₀ ,Σ Γ₁) (A₀ ,Σ A₁)
    ∙₀ᴾ : Con₀ᴾ ∙₀
    ∙₀ᴾ Γ₁ = ∙ᴰ
    _▷₀ᴾ_ : {Γ₀ : Con₀} → Con₀ᴾ Γ₀ → {A₀ : Ty₀} → Ty₀ᴾ A₀ → Con₀ᴾ (Γ₀ ▷₀ A₀)
    _▷₀ᴾ_ {Γ₀} Γ₀ᴾ {A₀} A₀ᴾ (Γ₁ ,Σ A₁) = Γ₀ᴾ Γ₁ ▷ᴰ A₀ᴾ A₁
    Π₀ᴾ : {Γ₀ : Con₀} → Con₀ᴾ Γ₀ → {A₀ : Ty₀} → Ty₀ᴾ A₀ → {B₀ : Ty₀} → Ty₀ᴾ B₀ → Ty₀ᴾ (Π₀ Γ₀ A₀ B₀)
    Π₀ᴾ {Γ₀} Γ₀ᴾ {A₀} A₀ᴾ {B₀} B₀ᴾ {Γ₀'}{Γ₁'} (e ,Σ Γ₁' ,Σ A₁ ,Σ B₁) = JC
      (λ Δ₀ f → Tyᴰ (Δ₀ ,Σ transpCP Con₁ f (transpCP Con₁ e Γ₁')) (Π₀ Γ₀ A₀ B₀ ,Σ symC f ,Σ transpCP Con₁ f Γ₁ ,Σ transpCP (λ Δ₀ → Ty₁ Δ₀ A₀) f A₁' ,Σ B₁))
      (Πᴰ (Γ₀ᴾ Γ₁) (A₀ᴾ A₁') (B₀ᴾ B₁))
      (symC e)
      where
        Γ₁ : Con₁ Γ₀
        Γ₁ = transpCP Con₁ e Γ₁'
        A₁' : Ty₁ Γ₀ A₀
        A₁' = transpCP (λ Γ₀ → Ty₁ Γ₀ A₀) e A₁

    indCon : (Γ : Con) → Conᴰ Γ
    indCon (Γ₀ ,Σ Γ₁) = indCon₀ Con₀ᴾ Ty₀ᴾ ∙₀ᴾ _▷₀ᴾ_ Π₀ᴾ Γ₀ Γ₁
    indTy  : {Γ : Con}(A : Ty Γ) → Tyᴰ Γ A
    indTy {Γ₀ ,Σ Γ₁}(A₀ ,Σ A₁) = indTy₀ Con₀ᴾ Ty₀ᴾ ∙₀ᴾ _▷₀ᴾ_ Π₀ᴾ A₀ {Γ₀}{Γ₁} A₁

module simpleInfinitary where

  data Con₀ : Set
  data Ty₀ : Set
  data Con₀ where
    ∙₀ : Con₀
    _▷₀_ : Con₀ → Ty₀ → Con₀
  data Ty₀ where
    Π₀ : Con₀ → (ℕ → Ty₀) → Ty₀

  _≡C_ : Con₀ → Con₀ → Prop
  _≡T_ : Ty₀  → Ty₀  → Prop
  ∙₀ ≡C ∙₀ = 𝟙p
  (Γ₀ ▷₀ A₀) ≡C (Δ₀ ▷₀ B₀) = Γ₀ ≡C Δ₀ ×p A₀ ≡T B₀
  _ ≡C _ = 𝟘p
  Π₀ Γ₀ A₀ ≡T Π₀ Δ₀ B₀ = Γ₀ ≡C Δ₀ ×p ((n : ℕ) → A₀ n ≡T B₀ n)

  reflC : {Γ₀ : Con₀} → Γ₀ ≡C Γ₀
  reflT : {A₀ : Ty₀} → A₀ ≡T A₀
  reflC {∙₀} = *
  reflC {Γ₀ ▷₀ A₀} = reflC {Γ₀} ,Σ reflT {A₀}
  reflT {Π₀ Γ₀ A₀} = reflC {Γ₀} ,Σ λ n → reflT {A₀ n}

  transpC : (P : Con₀ → Set){Γ₀ Δ₀ : Con₀} → Γ₀ ≡C Δ₀ → P Γ₀ → P Δ₀
  transpT : (P : Ty₀  → Set){A₀ B₀ : Ty₀}  → A₀ ≡T B₀ → P A₀ → P B₀
  -- transpℕ : (P : (ℕ → Ty₀) → Set){A₀ B₀ : ℕ → Ty₀}  → ((n : ℕ) → A₀ n ≡T B₀ n) → P A₀ → P B₀
  transpC P {∙₀} {∙₀} e u = u
  transpC P {Γ₀ ▷₀ A₀} {Δ₀ ▷₀ B₀} (eC ,Σ eT) u = transpC (λ Γ → P (Γ ▷₀ B₀)) eC (transpT (λ A → P (Γ₀ ▷₀ A)) eT u)
  transpT P {Π₀ Γ₀ A₀} {Π₀ Δ₀ B₀} (eC ,Σ eT) u = transpC (λ Γ → P (Π₀ Γ B₀)) eC {!!}
  -- transpT P {Π₀ Γ₀ A₀} {Π₀ Δ₀ B₀} (eC ,Σ eT) u = transpC (λ Γ → P (Π₀ Γ B₀)) eC (transpℕ (λ A → P (Π₀ Γ₀ A)) eT u)
  -- transpℕ P {A₀}{B₀} eℕ u = {!!}

  Con₁ : Con₀ → Prop
  Ty₁  : Con₀ → Ty₀ → Prop
  Con₁ ∙₀ = 𝟙p
  Con₁ (Γ₀ ▷₀ A₀) = Con₁ Γ₀ ×p Ty₁ Γ₀ A₀
  Ty₁ Γ₀ (Π₀ Γ₀' F₀) = (Γ₀ ≡ Γ₀') ×p Con₁ Γ₀ ×p ((n : ℕ) → Ty₁ Γ₀ (F₀ n))

  Con : Set
  Con = Σsp Con₀ Con₁
  Ty : Con → Set
  Ty (Γ₀ ,Σ Γ₁)  = Σsp Ty₀ (Ty₁ Γ₀)
  ∙ : Con
  ∙ = ∙₀ ,Σ *
  _▷_ : (Γ : Con) → Ty Γ → Con
  (Γ₀ ,Σ Γ₁) ▷ (A₀ ,Σ A₁) = (Γ₀ ▷₀ A₀) ,Σ (Γ₁ ,Σ A₁)
  Π : (Γ : Con) → (ℕ → Ty Γ) → Ty Γ
  Π (Γ₀ ,Σ Γ₁) A = (Π₀ Γ₀ (λ n → proj₁ (A n))) ,Σ (refl ,Σ Γ₁ ,Σ λ n → proj₂ (A n))

  -- simple elimination principle

  module elimToProp
    {i}{j}
    (Conᴰ : Con → Prop i)
    (Tyᴰ : (Γ : Con) → Ty Γ → Prop j)
    (∙ᴰ : Conᴰ ∙)
    (_▷ᴰ_ : {Γ : Con}(Γᴰ : Conᴰ Γ){A : Ty Γ}(Aᴰ : Tyᴰ Γ A) → Conᴰ (Γ ▷ A))
    (Πᴰ : {Γ : Con}(Γᴰ : Conᴰ Γ){A : ℕ → Ty Γ}(Aᴰ : (n : ℕ) → Tyᴰ Γ (A n)) → Tyᴰ Γ (Π Γ A))
    where

    indCon : (Γ₀ : Con₀)(Γ₁ : Con₁ Γ₀) → Conᴰ (Γ₀ ,Σ Γ₁)
    indTy  : {Γ₀ : Con₀}{Γ₁ : Con₁ Γ₀}(A₀ : Ty₀)(A₁ : Ty₁ Γ₀ A₀) → Tyᴰ (Γ₀ ,Σ Γ₁) (A₀ ,Σ A₁)
    indCon ∙₀ Γ₁ = ∙ᴰ
    indCon (Γ₀ ▷₀ A₀) Δ₁ = indCon Γ₀ (proj₁ Δ₁) ▷ᴰ indTy A₀ (proj₂ Δ₁)
    indTy {.Γ₀} {Γ₁} (Π₀ Γ₀ A₀) (refl ,Σ Γ₁' ,Σ A₁) = Πᴰ (indCon Γ₀ Γ₁) (λ n → indTy (A₀ n) (A₁ n))

  module elimToSet
    {i}{j}
    (Conᴰ : Con → Set i)
    (Tyᴰ : (Γ : Con) → Ty Γ → Set j)
    (∙ᴰ : Conᴰ ∙)
    (_▷ᴰ_ : {Γ : Con}(Γᴰ : Conᴰ Γ){A : Ty Γ}(Aᴰ : Tyᴰ Γ A) → Conᴰ (Γ ▷ A))
    (Πᴰ : {Γ : Con}(Γᴰ : Conᴰ Γ){A : ℕ → Ty Γ}(Aᴰ : (n : ℕ) → Tyᴰ Γ (A n)) → Tyᴰ Γ (Π Γ A))
    where

    indCon : (Γ₀ : Con₀)(Γ₁ : Con₁ Γ₀) → Conᴰ (Γ₀ ,Σ Γ₁)
    indTy  : {Γ₀ : Con₀}{Γ₁ : Con₁ Γ₀}(A₀ : Ty₀)(A₁ : Ty₁ Γ₀ A₀) → Tyᴰ (Γ₀ ,Σ Γ₁) (A₀ ,Σ A₁)
    indCon ∙₀ Γ₁ = ∙ᴰ
    indCon (Γ₀ ▷₀ A₀) Δ₁ = indCon Γ₀ (proj₁ Δ₁) ▷ᴰ indTy A₀ (proj₂ Δ₁)
    indTy {Γ₀}{Γ₁}(Π₀ Γ₀' A₀) (e ,Σ Γ₁' ,Σ A₁) = {!Πᴰ (indCon Γ₀ ?) !}
