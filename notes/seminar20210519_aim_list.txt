Topics for AIM
--------------

1. transp and hcomp in HITs
https://github.com/agda/agda/issues/5362

2. Interleaved constructors in IITs

  interleaved mutual
    data A : Type
    data B : A → Type

    constructor
      a : A
      c : B a → A
      b : B a
      d : B (c b) → A

3. destructors for coinductive types and possibly higher coinductive types

https://github.com/agda/agda/issues/4650

4. Flag: Allow to eliminate out of prop equality.

5. Fine-grained interactive reduction of terms

e.g. another version of abstract which only affects printing, but not
testing for definitional equality (doesn't unfold during printing)

6. sometimes fully qualified names are printed even if the module is
open (probably a bug)

7. disallow mutual definitions

    (useful for metatheory when you don't want to allow e.g.
inductive-inductive types)
    --no-forward-declarations
    --no-mutual

8. define CwF as a record with the "variable" feature (without writing
universal quantifications for all equations)

9. obtain "algebras", "displayed algebras", "sections", "morphisms"
etc from an inductive type

