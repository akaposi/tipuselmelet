-- last time: Definition of categories with families (model of type theory)

-- categories with families

-- category structure
Con : Set
Sub : Con → Con → Set
id  : Sub Γ Γ
_∘_ : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    + laws

-- types
Ty  : Con → Set
_[_] : Ty Γ → Sub Δ Γ → Ty Δ

-- terms
Tm  : (Γ : Con) → Ty Γ → Set
_[_] : Tm Γ A → (σ : Sub Δ Γ) → Tm Δ A[σ]

-- empty context / empty substitution
◆ : Con
ε : Sub Γ ◆

-- context extension / substitution extension
_▷_ : (Γ : Con) → (A : Ty Γ) → Con
_,_ : (σ : Sub Δ Γ) (a : Tm Δ A[σ]) → Sub Δ (Γ ▷ A)


-- Booleans

-- Two options:
-- 𝔹 is defined in the empty context
- 𝔹 : Ty ◆
- 𝔹[ε] : Ty Γ

 𝔹 : ∀ {Γ} → Ty Γ
 𝔹[] : 𝔹[σ] = 𝔹

 tt ff : Tm Γ 𝔹
 tt[] : tt[σ] = tt
 ff[] : ff[σ] = ff

 -- non-dependent eliminator
 if-then-else : (A : Ty Γ) (t : Tm Γ A) (f : Tm Γ A) (b : Tm Γ 𝔹)
              → Tm Γ A

 -- dependent eliminator for 𝔹
 if-then-else : (A : Ty (Γ ▷ 𝔹)) (t : Tm Γ A[id , tt]) (f : Tm Γ A[id , ff]) (b : Tm Γ 𝔹)
              → Tm Γ A[id , b]
 if-then-else A t f tt = t
 if-then-else A t f ff = f

 -- substitution rule for if-then-else
   A : Ty (Γ ▷ 𝔹)
   t : Tm Γ A[id , tt]
   f : Tm Γ A[id , ff]
   b : Tm Γ 𝔹

   σ  : Sub Δ Γ
   σ⁺ : Sub (Δ ▷ 𝔹) (Γ ▷ 𝔹)       -- [δ : Δ, b : 𝔹]            [γ : Γ, b : 𝔹]
                                   -- σ⁺.γ = σ δ
                                   -- σ⁺.b = b
   -- operations of a CwF
     -- for any type A
     p : Sub (Δ ▷ A) Δ
     q : Tm (Δ ▷ A) A[p]

     -- for 𝔹
     p : Sub (Δ ▷ 𝔹) Δ
     q : Tm (Δ ▷ 𝔹) 𝔹
   σ⁺ = (σ ∘ p) , q
 (if-then-else A t f b)[σ] = if-then-else A[σ⁺] t[σ] f[σ] b[σ]
 -- Types:
   if-then-else A t f b : Tm Γ A[id, b]
   (if-then-else A t f b)[σ] : Tm Δ A[id, b][σ]
   if-then-else A[σ⁺] t[σ] f[σ] b[σ] : Tm Δ A[σ⁺][id, b[σ]]

   ???   (id, b) ∘ σ = σ⁺ ∘ (id , b[σ])
   -- both sides reduce to (σ, b[σ])

-- not with de Brujin indices
not : Tm (◆ ▷ 𝔹) 𝔹
not = if-then-else 𝔹 ff tt q
not = if-then-else 𝔹 ff tt ⓪

and : Tm (◆ ▷ 𝔹 ▷ 𝔹) 𝔹
and = if-then-else 𝔹 ⓪ ff ⑴

-- not with variable names
not : Tm [b : 𝔹] 𝔹
not [b : 𝔹] = if-then-else 𝔹 ff tt b

and : Tm [a : 𝔹, b : 𝔹] 𝔹
and [a : 𝔹, b : 𝔹] = if-then-else 𝔹 b ff a

-- and [tt, tt] =  tt
-- and [tt, b]  =  b
-- and [b, tt]  /= b
                = if-then-else 𝔹 tt ff b

-- Currently known models:
--   Initial model (syntax)           I
--   Terminal / trivial model

-- Standard model    S
--   We want to translate internal constructions to actual proofs
--   Closed types : sets
--   Closed terms of type A : elements of A

S.Con := Set
  [a : 𝔹 , b : 𝔹]    ∼>    (Bool × Bool)   in S.Con
S.Sub Δ Γ := (Δ → Γ)   (functions from Δ to Γ)
S.◆ := {⋆} (singleton set)

S.Ty Γ := Γ → Set

-- Type substitution (A : Γ → Set) (σ : Δ → Γ) (A[σ] : Δ → Set)
A [ σ ] := λ γ ↦ A (σ γ)

S.Tm Γ A := (γ : Γ) → A γ

-- Term substitution (a : (γ : Γ) → A γ) (σ : Δ → Γ) (a[σ] : (δ : Δ) → A (σ δ))
a [ σ ] := λ γ ↦ a (σ γ)

in set theory: (γ : Γ) → A γ is some subset of the set relations between Γ and { (γ,a) | γ : Γ, a : A γ }

(Γ S.▷ A) := (γ : Γ) × (a : A γ)
S.p := λ (γ, a) ↦ γ
S.q := λ (γ, a) ↦ a

-- σ : Δ → Γ
-- a : (δ : Δ) → A (σ δ)
-- S.(σ , a) : (δ : Δ) → (γ : Γ) × (a : A γ)
S.(σ , a) := λ δ ↦ (σ δ , a δ)

S.𝔹 := λ _ ↦ {true, false}
[a : 𝔹 , b : 𝔹] = (S.◆ S.▷ S.𝔹 S.▷ S.𝔹) = (⋆ : {⋆}) × (a : Bool) × (b : Bool)

S.tt := λ _ ↦ true
S.ff := λ _ ↦ false
S.if-then-else A t f b : S.Tm Γ A[id, b]
S.if-then-else A t f b : (γ : Γ) → (A[id, b] γ)
S.if-then-else A t f b : (γ : Γ) → A (γ, b γ)
S.if-then-else A t f b := λ γ ↦ if b γ then t γ else f γ

-- Empty type: ⊥
⊥ : Ty Γ
⊥[] : ⊥[σ] = ⊥
exfalso : (A : Ty Γ) (a : Tm Γ ⊥) → Tm Γ A
exfalso[] : (exfalso A a)[σ] = (exfalso A[σ] a[σ])

-- in the standard model
S.⊥ := λ _ → {}
S.exfalso A a := λ γ → { here (a γ) is an element of {} }


-- Consistency: there is no closed inhabitant of ⊥ in the initial model I
-- We have F : Morphism I S
-- Given a : I.Tm I.◆ I.⊥ , we have  (F a) : S.Tm S.◆ S.⊥
--  S.Tm S.◆ S.⊥ = (_ : {⋆}) → {}
-- We have proven that (I.Tm I.◆ I.⊥ → {}), i.e. I.Tm I.◆ I.⊥ is empty.


-- Π-types (dependent functions)
--   Γ : Con
--   A : Ty Γ
--   B : Ty (Γ ▷ A)

--     the set  Tm (Γ ▷ A) B  contains the dependent functions from A to B

(Π A B) : Ty Γ
isomorphism   Tm Γ (Π A B) ≃ Tm (Γ ▷ A) B
-- categorical application
app : Tm Γ (Π A B) → Tm (Γ ▷ A) B
lam : Tm (Γ ▷ A) B → Tm Γ (Π A B)
Πβ : (f : Tm (Γ ▷ A) B) → app (lam f) = f
Πη : (f : Tm Γ (Π A B)) → lam (app f) = f

-- + substitutions laws Π[], app[], lam[]
-- this needs the operation (-)⁺ on substitutions.

-- The usual application is equivalent to app.
app' : Tm Γ (Π A B) → (a : Tm Γ A) → Tm Γ (B[id, a])
-- HW: prove that app' and app are equivalent.
Πβ' : app (lam f) a = f[a]

-- Example terms:
-- not : Tm (◆ ▷ 𝔹) 𝔹
-- not = if-then-else 𝔹 ff tt q
-- not = if-then-else 𝔹 ff tt ⓪

not' : Tm ◆ (Π 𝔹 𝔹)
not' = lam not
not' = lam (if-then-else 𝔹 ff tt q)
not' = lam (b ↦ if-then-else 𝔹 ff tt b)

applyTrue : Tm ◆ (Π (Π 𝔹 𝔹) 𝔹)
applyTrue = lam (f ↦ app' f tt)
applyTrue = lam (app' ⓪ tt)

app' applyTrue not'
  = app' not' tt             (by Πβ')
  = if-then-else 𝔹 ff tt tt  (by Πβ')
  = ff                       (by the computation rule of if-then-else)


-- Π in the standard model S
-- (A : Γ → Set) (B : (γ : Γ) × (a : A γ) → Set)
S.Π A B := λ γ → (a : A γ) → B (γ , a)
-- HW: define lam and app in the standard model


-- Σ-types
--   Γ : Con
--   A : Ty Γ
--   B : Ty (Γ ▷ A)
--      (a : Tm Γ A) × (b : Tm Γ B[id , a])

(Σ A B) : Ty Γ
-- isomorphism: Tm Γ (Σ A B) ≃ (a : Tm Γ A) × (b : Tm Γ B[id , a])
π₁ : Tm Γ (Σ A B) → Tm Γ A
π₂ : (p : Tm Γ (Σ A B)) → Tm Γ B[id , π₁ p]
pair : (a : Tm Γ A) → (b : Tm Γ B[id , a]) → Tm Γ (Σ A B)
-- β laws:
π₁ (pair a b) = a
π₂ (pair a b) = b
-- η law:
pair (π₁ p) (π₂ p) = p

-- + substitution laws Σ[] π₁[] π₂[] pair[]

f : Tm ◆ (Π 𝔹 (Σ 𝔹 𝔹))
f = lam (b ↦ pair (app' not b) b)

-- Σ in the standard model S
-- (A : Γ → Set) (B : (γ : Γ) × (a : A γ) → Set)
S.Σ A B := λ γ → (a : A γ) × B (γ , a)
