
{-
-- review (dependent types)

-- universe

-- identity type
Γ ⊢ A   Γ ⊢ t : A  Γ ⊢ u : A
────────────────────────────
      Γ ⊢ Id A t u

-- dependent function types:

Γ ⊢ A   Γ, x : A ⊢ B
─────────────────────
  Γ ⊢ Π (x : A). B

-- universal quantification
-- (Π(n : ℕ). Id ℕ n n)  -- ∀ n ∈ ℕ. n = n

-- universe: type of types (Russell-style)

─────
Γ ⊢ U

-- we want to put some type inside U
-- change the rule for Π

Γ ⊢ A : U    Γ, x : A ⊢ B : U
──────────────────────────────
  Γ ⊢ Π(x : A). B : U

-- one example:

-- Π(A : U). A → A

-- modus ponens
-- Π(A : U).Π(B : U). (A → B) → A → B

-- what is the type of U?

Γ ⊢ U i : U (i + 1)

cumulativity: (supported by Coq, but not Agda)

  i < j
─────────────
Γ ⊢ U i : U j

-}

-- Set is shorthand for Set₀

Ty1 : Set₁   -- Set is U in Agda
Ty1 = Set₀

-- base types in Set₀

data Bool : Set where
  true  : Bool
  false : Bool

Ty2 : Set₀
Ty2 = Bool

-- getting induction and computation on Bool
-- Agda:
--  free-form recursive (case splitting) definitions

-- in the notation of TT seminar:
-- Π(A : U). Bool → A → A → A

-- syntax for lambda in Agda: λ x → t
--                previously: λ x. t
choose : (A : Set) → Bool → A → A → A
choose A true  t f = t
choose A false t f = f
  -- choose A false t f (looping)
  -- choose A true f t (not accepted)

Choose : Bool → Set → Set → Set
Choose true  A B = A
Choose false A B = B

depfun : (b : Bool) → Choose b Bool (Bool → Bool)
depfun true  = true
depfun false = λ b → b

-- natural numbers
data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

infix 6 _+_
   -- just setting precedence/associativity
   -- of notation
_+_ : ℕ → ℕ → ℕ
zero  + b = b
suc a + b = suc (a + b)

open import Relation.Binary.PropositionalEquality


0+ : (n : ℕ) → zero + n ≡ n
0+ n = refl

+0 : (n : ℕ) → n + zero ≡ n
+0 zero    = refl
+0 (suc n) = cong suc (+0 n)


-- _≡_ : {A : Set} → A → A → Set

id : {A : Set} → A → A
id a = a

ex1 : Bool
ex1 = id true

ex2 : Bool
ex2 = id {Bool} true

-- abbreviation for iterated Π
+-assoc : (n m k : ℕ) → (n + m) + k ≡ n + (m + k)
+-assoc zero    m k = refl
+-assoc (suc n) m k = cong suc (+-assoc n m k)

+-comm : (n m : ℕ) → n + m ≡ m + n
+-comm zero    zero    = refl
+-comm zero    (suc m) = cong suc (sym (+0 m))
+-comm (suc n) zero    = cong suc (+0 n)
+-comm (suc n) (suc m) =
   let p1 : n + suc m ≡ suc (m + n)
       p1 = +-comm n (suc m)

       p2 : suc (m + n) ≡ m + suc n
       p2 =
         trans
           (sym (cong suc (+-comm n m)))
           (+-comm (suc n) m)

   in cong suc (trans p1 p2)

-- induction for Bool, ℕ

indBool :
    (P : Bool → Set)
  → P true
  → P false
  → (b : Bool)
  → P b
indBool P pt pf true  = pt
indBool P pt pf false = pf

-- for any inductive type: recursion
-- is induction with constant predicates
recBool : (A : Set) → A → A → Bool → A
recBool A t f b = indBool (λ _ → A) t f b

indℕ :
    (P : ℕ → Set)
  → P zero
  → ((n : ℕ) → P n → P (suc n))
  → (n : ℕ)
  → P n
indℕ P pz ps zero    = pz
indℕ P pz ps (suc n) = ps n (indℕ P pz ps n)

-- same rec as in STLC
recℕ : (A : Set) → A → (A → A) → ℕ → A
recℕ A z s n = indℕ (λ _ → A) z (λ _ → s) n


----------------------------------------

data Ty : Set where
  ι   : Ty
  _⇒_ : Ty → Ty → Ty

-- type contexts
data Con : Set where
  ∙   : Con
  _▶_ : Con → Ty → Con

infixl 3 _▶_
infixr 7 _⇒_

-- if (p : Var Γ A) then A ∈ Γ
data Var : Con → Ty → Set where
  here  : ∀ {Γ A} → Var (Γ ▶ A) A
  there : ∀ {Γ A B} → Var Γ A → Var (Γ ▶ B) A

-- ───────────── here
--  A ∈ (Γ ▶ A)

--     A ∈ Γ
-- ──────────── there
--  A ∈ (Γ ▶ B)

data Tm : Con → Ty → Set where
  var : ∀ {Γ A} → Var Γ A → Tm Γ A
  lam : ∀ {Γ A B} → Tm (Γ ▶ A) B → Tm Γ (A ⇒ B)
  app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B

id' : Tm ∙ (ι ⇒ ι)
id' = lam (var here)
-- (var here)

apply' : Tm ∙ ((ι ⇒ ι) ⇒ ι ⇒ ι)
apply' = lam (lam (app (var (there here)) (var here)))
   -- λ f. λ x. f x
   -- var (there (there (there ... here)))

-- things to prove:

-- ⊥: empty type
data ⊥ : Set where

-- fairly simple to prove
-- (standard Set-model)
consistent : Tm ∙ ι → ⊥
consistent = {!!}

-- normalization
-- normal forms
-- conversion relation on terms
