

TT + Ind-Ind types     (syntax for constructors + elimination principle)
                       (syntax for constructors + recursion principle)
		       -- (practical takeways: top-level signature for inductive types is much simpler)

In Agda/Coq:
 - additional context containing inductive definitions
 - our goal here: embedded syntax of inductive signatures ("generic" representation)
    - on the fly construct an inductive type from an inductive signature

-- syntax for constructors (Ind-Ind)
--------------------------------------------------------------------------------

Theory of II signatures:
   - CwF + U + El + Π (strictly positive)

- idea: every ToS context is an inductive signature

     U  : Ty Γ
     El : Tm Γ U → Ty Γ
     Π  : (a : Tm Γ U) → Ty (Γ, El a) → Ty Γ     -- strict positivity restriction

  NatSig : Con
  NatSig := ∙, Nat : U, zero : El Nat, suc : Π Nat (El Nat)

  IIExample : Con
  IIExample : ∙, A : U, B : Π A U, a : El A, b : El (B a)

    in pseudo-Agda:
       A : Set
       B : A → Set
       a : A
       b : B a

- Type theory, s.t. all ind-ind constructors are in the syntax


-- What's a model for TT+IIC:
--------------------------------------------------------------------------------

  1. A model of ToS
  2. A model of CwF + (U closed under Π)
  3. A strict CwF-morphism from 1 to 2, which maps U to U and Π to Π : _ᴬ    ("algebra" interpretation of signatures)
  4. A functor from category of 2. contexts to the category of ToS-models    (ToC                                   )

     4 is basically the "theory of constructors"

       - it's pretty much the same as ToS (1.), but everything is indexed over 2. contexts
         (everything is indexed over contexts of the ambient type theory)
       - ToS-expression in a normal TT context

       we have: Con Γ     ToC context over a normal context
                Ty Γ Ω    ToC context over (Γ normal context) (Ω is ToC context over Γ)
		Tm Γ Ω A  ....

       (we also have (σ : 2.Sub Γ Δ) → ToC.Con Δ → ToC.Con Γ, etc.)

         unambiguous if we write Con Γ, Ty Γ Ω, Tm Γ Ω A

  5. _^ : natural transformation from (Const 1) to 4

       - I want to embed ToS constructions as ToC constructions
       - Ω : ToS.Con    →    Ω^ : ToC.Con Γ
       - A : ToS.Ty Ω   →    A^ : ToC.Ty Γ Ω^

  6.
     type constructors and term constructors

          -- term in ToC with type U (specifies an inductive sort)

       ind : {Ω : ToS.Con} → Tm Γ Ω^ U → 2.Tm Γ U               ("lifting" operation)

       -- (natural) iso:
       (↑, ↓) : Tm Γ Ω^ (El A) ~ Tm Γ (El (ind {Ω} A))

  -- examples:

   Ω := ∙, N : U, z : El N, s : N → El N   -- N is in ToS

   Nat : Tm Γ U
   Nat := ind Ω N  -- N is a var in ToC

   zero : Tm Γ (El Nat)
   zero := ↑ z        -- z : Tm Γ Ω^ (El N)

   suc : Tm Γ (El Nat) → Tm Γ (El Nat)
   suc n := ↑(s (↓n))

     s : Tm Γ Ω^ (N → El N)
     ? : Tm Γ Ω^ (El N)
     n : Tm Γ (El Nat)

   -- example:
      B : A → Set
      (t : A)  -- arbitrary term with type A
      B t      -- not valid in ToS, only valid in ToC

   Ω := ∙, A : U, B : A → U, a : El A, b : (a : A) → El (B a)

   A : Tm Γ U
   A := ind Ω A

   B : Tm Γ (El A) → Tm Γ U
   B a := ind Ω (B ↓a)

      ?  : Tm Γ Ω^ U
      B  : Tm Γ Ω^ ((a : A) → El (B a))
      ↓a : Tm Γ Ω^ (El A)
      B ↓a : Tm Γ Ω^ (El (B ↓a))

   a : Tm Γ (El A)
   a := ↑a

   b : (a : Tm Γ (El A)) → Tm Γ (El (B a))
   b a := ↑(b ↓a)

      Tm Γ (El (B ↑↓a)) = Tm Γ (El (B a))

  -- how to add params to signatures?

    - ToS : everything is indexed over (Γ : 2.Con)
    -       Π̂    (external function space)  (abstracts from outer theory)
    -       _^   (keeping extra Γ param around)


     Π̂ : (a : 2.Tm Γ U) → 1.Ty (Γ, El a) Ω → 1.Ty Γ Ω


    (a : Tm Γ U)
    ListSig mentioning a : 1.Con (Γ, a)


-- + recursion principle (ugly)
------------------------------------------------------------

   recursor arguments: (σ : Sub Γ Ωᴬ)

     given (σ : Sub Γ Ωᴬ)                   (substitution operation, which goes from ToC to 2)

      recTy : Ty Γ Ω → Ty Γ
      recTm : Tm Γ Ω A → Tm Γ (recTy A)

      recCon : Con Γ → Con
      recTy  : Ty Γ (Ω++Δ) → Ty (Γ++recCon Δ)
      recTm  : Tm Γ (Ω++Δ) A → Tm (Γ++recCon Δ) (recTy A)


-- Actual implementation
--------------------------------------------------------------------------------

-- top-level context of inductive things

...

inductive
  Nat : Set
  zero : Nat
  suc : Nat → Nat

....

t : Nat   -- (elimination is still substitution, where we only substitute (Nat, zero, suc)
