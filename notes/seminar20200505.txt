Multimodal type theory


Modal type theory
  - models of type theories : presheaf models, ...


Crisp type theory
    C a category with a terminal object

    colim : Psh_C → Set
                 ⊥
    const : Set → Psh_C
                 ⊥
    lim   : Psh_C → Set   lim = evaluation at the terminal object

    ♭ = const ∘ lim : Psh_C → Psh_C     replaces a presheaf by a constant presheaf

    A presheaf        ♭ A ≃ A       then A is constant/discrete

    How do we extend ♭ to dependent presheaves (types).

    We need notions of global and local types.

    Γ | Δ ⊢ A type   Γ global context
                     Δ local context

    if Δ is empty, then A is a global type

     Γ | ⋄ ⊢ A type
    ────────────────
     Γ | Δ ⊢ (♭ A) type

Crisp type theory ~~~ Multimodal type theory with a single mode and an idempotent comonad


# Dependent right adjoints

Adjunction between categories C and D
 functors L : D → C, R : C → D
 C(L A, B) ≃ D(A, R B)


Functor F : A → B
    F_! : Â → B̂
            ⊥
    F^⋆ : B̂ → Â
            ⊥
    F_⋆ : Â → B̂

Dependent adjunction between CwFs C and D
 functor L : D → C
 (dependent) natural transformation
         R : {Γ : D} → Ty_C (L Γ) → Ty_D Γ
 Tm_C (L Γ) A ≃ Tm_D Γ (R A)


-- mode : Set
-- Hom : mode → mode → Set

-- Con : mode → Set

Context  Γ @ m
  extended context
    Γ, x : A @ m

  "lock"    μ : m → n
    Γ, L_μ @ n
    R_μ : {Γ : Con_m} → Ty_n (L_μ Γ) → Ty_m Γ

  -- extended context μ : m → n
  --   Γ, L_μ ⊢ A @ n
  --   ──────────────
  --   Γ, x : R_μ A @ m

  L_id = id
  L_(μ ∘ ν) = L_μ ∙ L_ν

μ : m → n
ν : m → n
α : μ ⇒ ν
  (Γ, L_ν) → (Γ, L_μ)

Parameter : mode theory
  M   2-category



M = 2-category Cat of categories

m : M       C_m  is  Psh_m
μ : m → n   L_μ  is  μ_! : Psh_m → Psh_n
            R_μ  is  μ^⋆
α : μ ⇒ ν   L_μ ⇒ L_ν


M = dual of 2-category Cat of categories

m : M       C_m  is  Psh_m
μ : m → n   L_μ  is  μ^* : Psh_n → Psh_m
            R_μ  is  μ_⋆
α : μ ⇒ ν   L_μ ⇒ L_ν


     const
       →
  Set     Set^ω  ↔ shift
       ←
     lim = ev₀


Type theory 𝕋 → how do we define models and morphisms of models
  in the same style as the QIIT paper ?

  for models over a category C : we work in a single presheaf category Psh_C
  for morphisms over a functor F : C → D  : we need to use the adjunction (F_! ⊣ F^*)

            C∙
            ↓↑
   Ren(C) → C
