{-# OPTIONS --omega-in-omega --no-pattern-matching #-}

module IST.Talk1 where

open import Agda.Primitive
open import IST.Base
open import IST.Naturals
open import IST.PredicatedTopologies

{-

- Internal Set Theory: 
  An axiomatic set theory for doing Non-standard Analysis.

- Non-standard Analysis: 
  a logically rigorous way of working with infinitesimal quantities.

- Infinitesimal quantities: 
  Take an ordered field (𝔽;≤,+,×,0,1). 
  𝔽 contains a copy of the naturals, ℕ = {0, 1, 1+1, ...}.
  A quantity x ∈ 𝔽 is infinitesimal if ∀n ∈ ℕ. |x| < n⁻¹,
  ...                 infinite      if ∀n ∈ ℕ. n < x.
  Fact: ℝ is Archimedean (no infinitesimals but 0).

- IST is fairly non-constructive (globally).
  IST is fairly constructive (locally).
  We'll see.

- IST is a conservative extension of ordinary set theory ZFC.

  ∃ algorithm that converts IST proofs to ZFC proofs.
  ∃ algorithms that extract *constructions* from certain fragments
    e.g. nonstandard Dialectica, implemented in Agda by Chuangjie Xu























----------------------------------------------------------------------


Why should we care?

1. Because Non-standard Analysis can benefit from Type Theory.
2. Because Type Theory can benefit from Non-standard Analysis.

- NSA, like Quine's NF, forces you to deal with logic all the time. [5]
  Requires certain "syntactic" checks that even experts get wrong sometimes. [6]
  Easy to make mistakes, huge benefits from formalization.


Start with 2, but start at some distance.
We'll work in classical mathematics at first.


[5] See Asaf Karagila at https://math.stackexchange.com/a/193203/645935

[6] Takuma Imamura: A nonstandard construction of direct limit group actions,2018, https://arxiv.org/pdf/1812.00575.pdf



















----------------------------------------------------------------------


Question. What is a monotone function?
Answer.
A function that preserves order. [1]

  f: (A,≤) → (B,≤) monotone when
  x ≤ y in A implies f(x) ≤ f(y) in B. 

So there is an *order predicate* (order relation) that is preserved.



Question. What is a continuous function?
Answer.
A continuous function is one that preserves “nearness” of points. [2]

Naively, you'd think our co/domains come equipped with some 
nearness predicate ≈ obeying some axioms, and

  f: (A,≈) → (B,≈) continuous when
  x ≈ y in A implies f(x) ≈ f(y) in B.

But of course that does not work!


Theorem. 
  There is no predicate ≈ : ℝ × ℝ → Set such that a function 
  f: ℝ → ℝ is continuous precisely if f preserves ≈.
Proof. Exercise.
Qed.


In fact,

Theorem (ZFC+LC).
  One cannot construct an equivalence of categories
  between Top and a full subcategory of the category of
  reflexive graphs.
Proof. Much harder exercise.
Qed.

[1] https://en.wikipedia.org/wiki/Monotonic_function
[2] S. Lovett: Differential Geometry of Manifolds, ... 2020






















----------------------------------------------------------------------

Sometimes we get lucky, chiefly in "computer science" topology.

Def. We call a topological space T *Alexandroff* if arbitrary (as
     opposed to just finite) intersections of open subsets
     are open in T.
Stated.

The category Alex that has
  objects: Alexandroff spaces (A, ΩA)
  morphisms (A, ΩA) → (B, ΩB): continuous functions f: (A, ΩA) → (B, ΩB)
is equivalent to the category of preordered sets w/ monotone maps [3].

In fact, Alex is maximal with this property.

x ≤ y   pif.  ∀ N ∈ ΩT. x ∈ N → y ∈ N
specialization preorder

pif. means iff.

[3] https://ncatlab.org/nlab/show/specialization+topology#alexandroff_topological_spaces






















----------------------------------------------------------------------

So ≈ relations generally do not exist, but it would be massively
convenient if we had them!

Doubt? IST allows us to have them: we can define topological spaces,the
Hausdorff condition, compactness, and prove that all metric spaces are Hausdorff in < 300 lines of Agda on top of IST.

Kripke semantics becomes formally the same as topological semantics.

  w ⊩ P → Q   pif.  ∀u. w ≤ u → u ⊩ P → u ⊩ Q

  Replace ≤ with ≈.

  We have that      ∀ u. w ≈ u → u ∈ ⟦P⟧ → u ∈ ⟦Q⟧
               pif. ∀ u. w ≈ u → u ∉ ⟦P⟧ ∨ u ∈ ⟦Q⟧
               pif. ∀ u. w ≈ u → u ∈ ⟦P⟧ᶜ ∪ ⟦Q⟧
               pif. w is an interior point of ⟦P⟧ᶜ ∪ ⟦Q⟧
               pif. w ∈ ⟦P → Q⟧
  hence ⟦P → Q⟧ = Int(⟦P⟧ᶜ ∪ ⟦Q⟧) as desired.

Sections of sheaves F on topological spaces turn into
"continuous dependent functions" Π(x : T). F_x.

[4] https://github.com/zaklogician/agda-ist-algebra/blob/master/src/PredicatedTopologies.agda






















----------------------------------------------------------------------

We want ≈, we don't have ≈. Cheeky idea:
let's extend set theory with a predicate ≈ that does what we want.
Instead we will adjoin a single unary predicate st(-) to ordinary set theory,
and we'll define ≈_T in terms of st(-).

If ℝ contained infinitely large numbers, we could construct
a genuine nearness predicate ≈ : ℝ × ℝ → ESet (more about ESet later):

x ≈ y precisely if
  1. x is not infinite,
  2. x is infinitesimally close to y, i.e. |x-y| infinitesimal
  3. y is not infinite (for symmetry, follows from 1,2).

Problem:
  if we "add more elements" to ℝ, we also get
  new functions ℝ → ℝ. (Exercise: why is that a problem?)

Solution:
  a predicate st(f) saying that the function (number, set, object, etc.)
  f is not defined in terms of the new elements that we added.

-}


{- Useful refs.


Sam Sanders: The unreasoanble effectiveness of nonstandard analysis
Journal of Logic and Computation, Volume 30, Issue 1, January 2020,
Pages 459–524,
https://doi.org/10.1093/logcom/exaa019

Edward Nelson: The syntax of nonstandard analysis,
Annals of Pure and Applied Logic, Volume 38, Issue 2, 1988,
Pages 123-134,
https://doi.org/10.1016/0168-0072(88)90050-4.

Z. A. K.: Development of Group Theory in the Language of IST
PhD Thesis, University of Manchester, UK, 2019,
https://github.com/zaklogician/agda-ist-algebra

Benno van den Berg, Eyvind Briseid, Pavol Safarik: A functional interpretation for nonstandard arithmetic,
Annals of pure and applied logic, Volume 163, Issue 12, 2012.
arXiv:1109.3103


-}
