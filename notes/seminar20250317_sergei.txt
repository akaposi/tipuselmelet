Sergei Soloviev
IRIT, Toulouse

Asymmetry in Semantic Games

Our aim is to study how the asymmetrical constraints imposed on
Verifier and Falsifier may influence Game Semantics. These constraints
may concern computational power (complexity of strategies) of the
players, their access to information such as history (previous moves),
the right to backtrack, etc. To some extent we have already studied
certain cases where semantics has been modified [2,3]. For example, if
Verifier is able to compute a<a universal function for the set of the
strategies of Falsifier, under certain conditions Verifier may win in
semantic games based on formulas that are false.
