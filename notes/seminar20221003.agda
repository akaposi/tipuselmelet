open import Agda.Primitive

infixr 50 _,_
infixr 30 _×_

data ⊥ : Set where
exfalso : ∀ {i} {A : Set i} → ⊥ → A
exfalso ()

data _≡_ {i} {A : Set i} (x : A) : A → Set i where
  refl : x ≡ x

-- Unit type
record ⊤ {j} : Set j where
  constructor tt
open ⊤ public

-- Σ-types
record Σ {i j} (A : Set i) (B : A → Set j) : Set (i ⊔ j) where
  constructor _,_
  field fst : A
        snd : B fst
open Σ public

_×_ : ∀ {i j} (A : Set i) (B : Set j) → Set (i ⊔ j)
A × B = Σ A λ _ → B

-- W-types

-- also: Polynomial functor

record Container : Set₁ where
  field
    Shape    : Set          -- kind of node in a tree
    Position : Shape → Set  -- arity of a kind of node in a tree

  F : Set → Set
  F X = Σ Shape (λ s → Position s → X)

open Container

module _ (C : Container) where

  data W : Set where
    sup : (s : C .Shape) → (C .Position s → W) → W
    -- sup : C .F W → W

-- W + universes, ⊤ , ⊥ , Bool , Id -> all inductive types

-- Natural numbers
--  Trees: two shapes :
--    - zero  of arity 0    ⊥
--    - suc   of arity 1    ⊤

module Nat where
  data Nat-Shape : Set where
    shape-zero shape-suc : Nat-Shape

  Nat-Position : Nat-Shape → Set
  Nat-Position shape-zero = ⊥
  Nat-Position shape-suc  = ⊤

  C : Container
  Shape C    = Nat-Shape
  Position C = Nat-Position

  ℕ = W C

  zero : ℕ
  zero = sup shape-zero exfalso

  suc : ℕ → ℕ
  suc n = sup shape-suc (λ _ → n)

  module _ {i} (P : Set i) (z : P) (s : P → P) where
    rec-ℕ : ℕ → P
    rec-ℕ (sup shape-zero x) = z
    rec-ℕ (sup shape-suc x)  = s (rec-ℕ (x tt))

  module _ (P : ℕ → Set) (z : P zero) (s : ∀ {n} → P n → P (suc n)) where
    ind-ℕ : ∀ n → P n
    ind-ℕ (sup shape-zero x) = {!z!} -- needs funext to prove   x ≡ exfalso
    ind-ℕ (sup shape-suc x)  = s (ind-ℕ (x tt))

module BinTree where

  data Shape-BinTree : Set where
    shape-leaf shape-node : Shape-BinTree

  data Position-node : Set where
    left right : Position-node

  Position-BinTree : Shape-BinTree → Set
  Position-BinTree shape-leaf = ⊥
  Position-BinTree shape-node = Position-node

  C : Container
  Shape C    = Shape-BinTree
  Position C = Position-BinTree

  BinTree = W C

  leaf : BinTree
  leaf = sup shape-leaf exfalso

  node : BinTree → BinTree → BinTree
  node l r = sup shape-node f
    where f : Position-node → BinTree
          f left = l
          f right = r

module List (A : Set) where

  -- data List : Set where
  --   nil  : List
  --   cons : A → List → List
  --  for every a : A, cons_a : List → List

  data Shape-List : Set where
    shape-nil  : Shape-List
    shape-cons : A → Shape-List

  Position-List : Shape-List → Set
  Position-List shape-nil      = ⊥
  Position-List (shape-cons _) = ⊤

  C : Container
  Shape C    = Shape-List
  Position C = Position-List

  List = W C

  nil : List
  nil = sup shape-nil exfalso

  cons : A → List → List
  cons x xs = sup (shape-cons x) (λ _ → xs)

-- "Why not W?" Jasper Hugunin
module Nat' where
  open Nat

  well-formed-ℕ : ℕ → Set
  well-formed-ℕ (sup shape-zero f) = f ≡ exfalso
  well-formed-ℕ (sup shape-suc f)  = ∀ p → well-formed-ℕ (f p)

  ℕ' = Σ ℕ (λ n → well-formed-ℕ n)

  zero' : ℕ'
  zero' = zero , refl

  -- suc n = sup shape-suc (λ _ → n)

  suc' : ℕ' → ℕ'
  suc' (n , wf-n) = suc n , λ _ → wf-n
    -- wf-n : well-formed-ℕ n
    -- ? : well-formed-ℕ (suc n)
    -- ? : ∀ p → well-formed-ℕ ((λ _ → n) p)

  module _ (P : ℕ' → Set) (z : P zero') (s : ∀ {n} → P n → P (suc' n)) where
    ind-ℕ' : ∀ n → P n
    ind-ℕ' (sup shape-zero _ , refl) = z
    ind-ℕ' (sup shape-suc x , wf-n)  = s (ind-ℕ' (x tt , wf-n tt))


record IxContainer : Set₁ where
  field
    Indices  : Set
    Shape    : Indices → Set
    Position : (i : Indices) → Shape i → Indices → Set
open IxContainer

record IxContainer' : Set₁ where
  field
    Indices  : Set
    Shape    : Indices → Set
    Position : (i : Indices) → Shape i → Set
    PositionIndex : ∀ {i s} → Position i s → Indices
open IxContainer'

module _ (C : IxContainer') where
  data IW : C .Indices → Set where
    sup : ∀ {i : C .Indices} (s : C .Shape i)
          → (∀ (p : C .Position i s) → IW (C .PositionIndex p)) → IW i

module Fin where
  -- data Fin : ℕ → Set where
  --   fz : ∀ n → Fin (suc n)
  --   fs : ∀ n → Fin n → Fin (suc n)

  -- data Fin : ℕ → Set where
  --   fz : ∀ n m → suc n ≡ m → Fin m
  --   fs : ∀ n m → suc n ≡ m → Fin n → Fin m

  data ℕ : Set where
    zero : ℕ
    suc  : ℕ → ℕ

  Fin-Indices  : Set
  Fin-Indices = ℕ

  data Fin-Shape-suc : Set where
    shape-fz shape-fs : Fin-Shape-suc

  Fin-Shape : Fin-Indices → Set
  Fin-Shape zero    = ⊥
  Fin-Shape (suc x) = Fin-Shape-suc

  Fin-Position : (i : Fin-Indices) → Fin-Shape i → Set
  Fin-Position (suc i) shape-fz = ⊥
  Fin-Position (suc i) shape-fs = ⊤

  Fin-PositionIndex : ∀ {i s} → Fin-Position i s → Fin-Indices
  Fin-PositionIndex {suc n} {shape-fs} x = n

  IC : IxContainer'
  Indices IC = Fin-Indices
  Shape IC = Fin-Shape
  Position IC = Fin-Position
  PositionIndex IC = Fin-PositionIndex

  Fin : ℕ → Set
  Fin = IW IC

  fz : ∀ n → Fin (suc n)
  fz n = sup shape-fz (λ {()})

  fs : ∀ n → Fin n → Fin (suc n)
  fs n x = sup shape-fs (λ _ → x)
