joint work with Rafael Bocquet, Christian Sattler

what is type theory? how to avoid talking about contexts and substitutions? -- type theory with only terms (types)
 - work in Agda -- 2LTT
 - combinatory type theory SK combinators
 - add equations: Con = Ty ∙, Sub Γ Δ = Tm Γ (Δ[ε]), Ty Γ = Tm Γ U   Tm : (Γ:Tm ∙ U) → Tm Γ U → Set
 - HOAS: Π : (A : Ty) → (Tm A → Ty) → Ty      Martin Hofmann, presheaves

paper: "Relative induction principles for type theories" https://arxiv.org/pdf/2102.11649.pdf

Relative displayed models of type theory
----------------------------------------

Usual canonicity proof for type theory
I will reconstruct it using two steps (2nd step will be a special case of a general construction)

Language of type theory (category with families, CwF):

Con:Set
Sub:Con→Con→Set
_∘_:Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
id :Sub Γ Γ
ass,idl,idr
Ty :Con → Set
Tm :(Γ:Con)→Ty Γ → Set                  (λx.y) : (Bool → Nat) -->  lam v¹ : Tm (∙▹Nat) (Bool⇒Nat)   v¹ : Tm(∙▹Nat▹Bool) Nat
_[_]:Ty Γ → Sub Δ Γ → Ty Δ
[id],[∘]
_[_]:Tm Γ A → (γ:Sub Δ Γ) → Tm Δ (A[γ])
[id],[∘]
◆ : Con
ε : Sub Γ ◆
◆η : (σ:Sub Γ ◆) → σ=ε
_▹_ : (Γ:Con)→Ty Γ→Con
proj : Sub Δ (Γ▹A) ≅ (γ:Sub Δ Γ) × Tm Δ (A[γ]) : pair     proj= (p∘_, q[_]),  pair = _,_   (natural in Δ)
------------------
Π : (A:Ty Γ)→Ty (Γ▹A) → Ty Γ
lam : Tm (Γ▹A) B ≅ Tm Γ (Π A B) : app
Π[] : (Π A B)[γ] = Π (A[γ]) (B[γ∘p,q])       γ:Sub Δ Γ,  (γ∘p,v⁰):Sub (Δ▹A[γ]) (Γ▹A)
lam[] : (lam t)[γ] = lam (t[γ∘p,q])
------------------
Bool : {Γ:Con} → Ty Γ
true, false : Tm Γ Bool
Bool[] : (Bool {Γ})[γ] = Bool {Δ}
true[], false[]
if-then-else : ...
----------------------------------------------

canonicity: (t : Tm ◆ Bool) → (t=true) + (t=false)

{-
NAT := (Nat:Set, zero:Nat, suc:Nat→Nat) Peano
syntax = (ℕ,0,+1) = I = initial NAT-model, it is a NAT-model.
displayed NAT-model := (Nat∙ : ℕ → Set, zero∙ : Nat∙ 0, suc∙ : ∀ n . Nat∙ n → Nat∙ (1+n))
                        motive          methods
induction : any displayed NAT-model has a section (dependent homomorphism from the syntax to the displayed model)
ind : (n : ℕ) → Nat∙ n
ind zero = zero∙
ind (suc n) = suc∙ {n} (ind n)
-}

syntax (it is a model of type theory = CwF + Π... + Bool...)
displayed model
  Con∙:Con → Set
  Sub∙:{Δ} → Con∙ Δ → Con∙ Γ → Sub Δ Γ → Set
  id∙ :Sub∙ Γ∙ Γ∙ id
  Ty∙:Con∙ Γ → Ty Γ → Set
  Tm∙ :(Γ∙:Con∙ Γ)→Ty∙ Γ∙ A → Tm Γ A → Set
  Bool∙ : Ty∙ Γ∙ Bool
  true∙ : Tm∙ Γ∙ Bool∙ true
  Π∙ : (A∙:Ty∙ Γ∙ A) → (B∙ : Ty∙ (Γ∙▹∙A∙) B) → Ty∙ Γ∙ (Π A B)
  ...
induction:
  ⟦_⟧_Con : (Γ : Con) → Con∙ Γ
  ⟦_⟧_Ty  : (A : Ty Γ) → Ty∙ ⟦Γ⟧ A
  ⟦_⟧_Tm  : (a : Tm Γ A) → Tm∙ ⟦Γ⟧ ⟦A⟧ a

-- canonicity: (t : Tm ◆ Bool) → (t=true) + (t=false)

canonicity (logical predicates, Tait's method, gluing over global section):
  Con∙ Γ := Sub ◆ Γ → Set
  Sub∙ Δ∙ Γ∙ γ := {δ:Sub ◆ Δ} → Δ∙ δ → Γ∙ (γ∘δ)
  Ty∙ Γ∙ A := {γ:Sub ◆ Γ} → Γ∙ γ → Tm ◆ (A[γ]) → Set
  Tm∙ Γ∙ A∙ a := {γ:Sub ◆ Γ}(γ* : Γ∙ γ) → A∙ γ* (t[γ])
  Bool∙ : {γ:Sub ◆ Γ} → Γ∙ γ → Tm ◆ Bool → Set
  Bool∙ _ t := (t=true) + (t=false)
  true∙ : {γ:Sub ◆ Γ}(γ* : Γ∙ γ) → (true=true)+(true=false)
  true∙ _ := inl refl
  HW: define the other components
  _[_]∙ : Ty∙ Γ∙ A → Sub∙ Δ∙ Γ∙ γ → Ty∙ Δ∙ (A[γ])
  true[]∙ : true∙[γ∙]∙ = true∙
  etc.

canon t := ⟦t⟧ tt : (a:Tm ◆ Bool) → (t=true)+(t=false)

HOAS:

  displayed model for things in the empty context
  displayed model relative to ◆:

  Ty● : Ty ◆ → Set
  Tm● : {A:Ty ◆} → Ty● A → Tm ◆ A → Set
  Bool● : Ty● Bool
  true● : Tm● Bool● true
  Π● : {A : Ty ◆}(A● : Ty● A){B : Ty (◆▹A)}(B● : (a:Tm ◆ A)→Tm● A● a → Ty● (B[a])) → Ty● (Π A B)

  Ty● A := Tm ◆ A → Set
  Tm● {A} A● a := A● a
  Bool● t := (t=true)+(t=false)
  true● : (true=true)+(true=false) := inl refl
  Π● A● B● t := (a:Tm ◆ A)(a● : A● a) → B● a a● (t$a)
    A● : Tm ◆ A → Set
    B● : (a:Tm ◆ A)→A● a → Tm ◆ (B[a]) → Set
    t : Tm ◆ (Π A B)
  ...

  Con∙ Γ := (γ : Sub ◆ Γ) → Set
  Ty∙ Γ∙ A := {γ:Sub ◆ Γ} → Γ∙ γ → Ty● (A[γ])
  ...
  HW

  from the relative displayed model (SMALL, EASY, w/o boilerplate), I obtain a displayed model (with all the boilerplate you want)

GENERALISATION:
  normalisation:   rel.disp.model,
  parametricity
  conservativity proofs

relative displayed model = displayed model - (_▹_,p,q,_,_), I change the _▹_s into HOAS

  Con● Γ := (Γ=◆)
  Sub● (Δ=◆) (Γ=◆) (γ:Sub ◆ ◆) := ⊤
  id● := tt
  ...

  Ty● : Ty ◆ → Set
  Tm● : {A:Ty ◆} → Ty● A → Tm ◆ A → Set
  Bool● : Ty● Bool
  true● : Tm● Bool● true
  Π● : {A : Ty ◆}(A● : Ty● A){B : Ty (◆▹A)}(B● : (a:Tm ◆ A)→Tm● A● a → Ty● (B[a])) → Ty● (Π A B)

model of t.t.:
  CwF+Π+... =
  category C with ◆
  Ty : PSh(C)              Con_C^                         Ty : Set   
  Tm : DepPSh(C) Ty        Ty_C^ Ty                       Tm : Ty → Set
  Bool : PShNat Unit Ty    Sub_C^ ◆ Ty                    Bool : Ty
  Π  : Tm_C^ (A:Ty ▹ B : Tm[A] ⇒ Ty) Ty                   Π : (A:Ty)→(Tm A → Ty) → Ty
  lam : Tm_C^ (A:_▹ B : _ ▹ Π (a:Tm[A]).Tm[B$a]) (Tm[Π])  lam : ∀ A B . ((a:Tm A) → Tm (B a)) → Tm (Π A B)
  _▹_ <- I don't say anything about this

  relative displayed model is:
  ...
  C category with ◆, F : C → I functor preservering ◆ 
                     F* : I^ -> C^ strict CwF morphism (preserves _▹_ strictly)
  Ty : Con_I^                                            internally to I^ : Ty : Set
  Ty● : Ty_C^ (F* Ty)                                    Ty● : F* Ty → Set  in C^
  Tm● : Ty_C^ (A:F* Ty ▹ A● : Ty●[A] ▹ a : (F* Tm)[A])                 multi-modal t.t. 
  ...
