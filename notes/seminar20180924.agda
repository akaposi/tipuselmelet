{-# OPTIONS --type-in-type #-}

module seminar20180924 where

open import Agda.Primitive

-- Agda intro for Haskellers

data Bool : Set where  -- GADT syntax in Haskell
  true  : Bool
  false : Bool

f1 : Bool → Bool -- Boolean negation
f1 true  = false
f1 false = true

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

n0 : ℕ
n0 = zero

n3 : ℕ
n3 = suc (suc (suc zero)) -- also known as 3

-- we two different base types

if_then_else_ : {A : Set} → Bool → A → A → A
if true  then t else f = t
if false then t else f = f

-- dependent function (Idris tutorial, Epigram tutorial)
fun : (b : Bool) → if b then Bool else ℕ
fun true  = true
fun false = n3

infixl 6 _+_
_+_ : ℕ → ℕ → ℕ
zero  + b = b 
suc a + b = suc (a + b)  -- structural recursion (terminating funtions)

-- =: definitional equality (computes)
--    left-to-right computation
--    always reduces to β-normal forms

-- Bool is sometimes called 2

data ⊤ : Set where    -- also called: Unit, Top, One, 1
  tt : ⊤

data ⊥ : Set where

exfalso : ⊥ → (A : Set) → A
exfalso ()  -- absurd pattern: matching on something with no constructors


comp : {A B C : Set} → (B → C) → (A → B) → A → C
comp {A}{B}{C} f g a = f (g a)

infixr 5 _∘_
_∘_ :
  {A : Set}
  {B : A → Set}
  {C : (a : A) → B a → Set}
  (f : {a : A} → (b : B a) → C a b)
  (g : (a : A) → B a)
  (a : A)
  → C a (g a)
(f ∘ g) a = f (g a)

foo : ℕ → ℕ
foo = suc ∘ suc

not = if_then false else true

bar : (b : Bool) → if (not b) then Bool else ℕ
bar = fun ∘ not

-- type of equality proofs
infixr 4 _≡_
data _≡_ {A : Set} : A → A → Set where
  refl : {x : A} → x ≡ x

n1 : ℕ
n1 = suc zero

p1 : n3 ≡ n1 + n1 + n1
p1 = refl                    

p2 : refl ≡ refl {_}{n3}     -- equality of equality proofs
p2 = refl {_}{refl {_}{n3}}

data So : Bool → Set where
  oh : So true

eq : ℕ → ℕ → Bool
eq zero zero = true
eq zero (suc b) = false
eq (suc a) zero = false
eq (suc a) (suc b) = eq a b

so1 : So (eq n3 (n1 + n1 + n1))
so1 = oh

-- every function respect ≡ 
ap : {A B : Set}(f : A → B){x y : A} → x ≡ y → f x ≡ f y
ap {A} {B} f {x} {.x} refl = refl -- dependent pattern matching
                                  -- refines type indices when matching
                                  -- in simple TT, you only transport

+-assoc : (a b c : ℕ) → a + (b + c) ≡ (a + b) + c
+-assoc zero    b c = refl
+-assoc (suc a) b c = ap suc (+-assoc a b c)
-- you can rewrite this to ℕ-induction on "a" argument

-- type of proofs that some ℕ is even
data Even : ℕ → Set where
  doubleOf : (n : ℕ) → Even (n + n)

p3 : Even (suc n3)
p3 = doubleOf (suc (suc zero))

p4 : Even zero
p4 = doubleOf zero


fun2 : (n : ℕ) → Even n → ⊤
fun2 .(m + m) (doubleOf m) = tt

¬_ : Set → Set
¬ A = A → ⊥

p5 : ∀ n → n ≡ suc zero → ¬ (Even n)    
p5 .zero () (doubleOf zero)
p5 .(suc (suc zero)) () (doubleOf (suc zero))
p5 .(suc (suc (n + suc (suc n)))) () (doubleOf (suc (suc n)))

p6 : ¬ (Even (suc zero))
p6 = p5 (suc zero) refl


data Even' : ℕ → Set where
  zeroEven' : Even' zero
  ssEven'   : {n : ℕ} → Even' n → Even' (suc (suc n))

p7 : ¬ (Even' (suc zero))
p7 ()

p8 : ∀ {n} → Even' n → ¬ (Even' (suc n))
p8 zeroEven' ()
p8 (ssEven' p) (ssEven' q) = p8 p q


-- simply typed lambda calculus, consistency proof

infixr 4 _⇒_
data Ty : Set where
  Bot : Ty           -- empty base type
  _⇒_ : Ty → Ty → Ty -- function type

infixl 3 _▶_
data Con : Set where
  ∙   : Con
  _▶_ : Con → Ty → Con

con1 : Con
con1 = ∙ ▶ Bot ▶ Bot ▶ (Bot ⇒ Bot)  -- list of types in context/scope

-- well-typed de Bruijn indices
data Var : Con → Ty → Set where
  vz : ∀ {Γ A} → Var (Γ ▶ A) A
  vs : ∀ {Γ A B} → Var Γ A → Var (Γ ▶ B) A

var1 : Var con1 Bot
var1 = vs (vs vz)

-- well-typed lambda terms
data Tm (Γ : Con) : Ty → Set where
  var : ∀ {A} → Var Γ A → Tm Γ A
  lam : ∀ {A B} → Tm (Γ ▶ A) B → Tm Γ (A ⇒ B)
  app : ∀ {A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B

tm1 : Tm ∙ (Bot ⇒ Bot)
tm1 = lam (var vz)     -- λ x . x

tm2 : Tm ∙ (Bot ⇒ Bot ⇒ Bot)
tm2 = lam (lam (var vz))  -- λ x y. y
                          -- other solution: λ x y. x

infixr 4 _,_
infixr 2 _×_

record Σ {a b} (A : Set a) (B : A → Set b) : Set (a ⊔ b) where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open Σ public

_×_ : ∀ {a b} (A : Set a) (B : Set b) → Set (a ⊔ b)
A × B = Σ A λ _ → B

-- consistency proof ~ well-typed interpreter ~ "standard model"
-- ~ "set model"


-- values
⟦_⟧Ty : Ty → Set
⟦ Bot   ⟧Ty = ⊥
⟦ A ⇒ B ⟧Ty = ⟦ A ⟧Ty → ⟦ B ⟧Ty

-- list of values
⟦_⟧Con : Con → Set
⟦ ∙     ⟧Con = ⊤
⟦ Γ ▶ A ⟧Con = ⟦ Γ ⟧Con × ⟦ A ⟧Ty

-- lookup function
⟦_⟧Var : ∀ {Γ A} → Var Γ A → ⟦ Γ ⟧Con → ⟦ A ⟧Ty
⟦ vz   ⟧Var (γ , v) = v
⟦ vs x ⟧Var (γ , v) = ⟦ x ⟧Var γ

-- evaluation function
⟦_⟧Tm : ∀ {Γ A} → Tm Γ A → ⟦ Γ ⟧Con → ⟦ A ⟧Ty
⟦ var x   ⟧Tm γ = ⟦ x ⟧Var γ
⟦ lam t   ⟧Tm γ = λ α → ⟦ t ⟧Tm (γ , α)
⟦ app t u ⟧Tm γ = ⟦ t ⟧Tm γ (⟦ u ⟧Tm γ)

consistency : Tm ∙ Bot → ⊥
consistency t = ⟦ t ⟧Tm tt


-- consistency : Tm ∙ Bot → ⊥  -- Haskell version
-- consistency t = consistency t



















-- zero  + b = b 
-- suc a + b = suc (a + b)



-- set theory: you need to prove explicitly that
--  n1 + n1 + n1 ≡ suc (suc (suc zero))

-- data Eq :: forall a. a -> a -> * where  -- Haskell
--   Refl :: forall x. Eq x x














 







-- type of proofs of equality
-- data _≡_ 

















