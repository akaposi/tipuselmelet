{-# OPTIONS #-}

module seminar20181001 where

-- https://photos.app.goo.gl/YBJma5eU3rdSNkFb6

open import Agda.Primitive

-- empty type

data ⊥ : Set where

-- unit type

data ⊤ : Set where
  tt : ⊤

-- natural numbers

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

recℕ : ∀{i}(A : Set i) → A → (A → A) → ℕ → A
recℕ A z s zero = z
recℕ A z s (suc n) = s (recℕ A z s n)

indℕ : ∀{i}(P : ℕ → Set i) → P zero → ((n : ℕ) → P n → P (suc n)) → (n : ℕ) → P n
indℕ P z s zero = z
indℕ P z s (suc n) = s n (indℕ P z s n)

_+_ : ℕ → ℕ → ℕ
a + b = recℕ ℕ b suc a

-- the equality type

data _≡_ {i}{A : Set i} : A → A → Set i where
  refl : (a : A) → a ≡ a

-- nondependent eliminator of the equality type

transport : ∀{i j}{A : Set i}(P : A → Set j){a a' : A} → a ≡ a' → P a → P a'
transport P (refl a) x = x

-- the interval

postulate
  -- type formation rule
  I : Set
  -- point constructors
  l r : I
  -- equality (path) constructor
  s : l ≡ r
  -- induction principle
  indI : ∀{i}(P : I → Set i)(x : P l)(y : P r)(xy : transport P s x ≡ y) → (i : I) → P i

postulate
  ℤ₃ : Set
  ∣_∣ : ℕ → ℤ₃
  eq : (n : ℕ) → ∣ n + 3 ∣ ≡ ∣ n ∣

-- disjointness of constructors of ℕ

disjℕ : {n : ℕ} → zero ≡ suc n → ⊥
disjℕ e = transport P e tt
  where
    P : ℕ → Set
    P n = indℕ (λ n → Set) ⊤ (λ _ _ → ⊥) n

-- the dependent eliminator of the equality type

J : ∀{i j}{A : Set i}{a : A}(P : (x : A) → a ≡ x → Set j) → P a (refl a) → {x : A}(e : a ≡ x) → P x e
J P p (refl _) = p

trconst : {i j : Level}{A : Set i}{a : A}{B : Set j}{b b' : B}(e : b ≡ b') → a ≡ transport (λ _ → A) e a
trconst {A = A}{a}{B}{b}{b'} e = J {A = B}{b}(λ x e → a ≡ transport (λ _ → A) e a) (refl a) {b'} e

-- transitivity of equality

_◾_ : ∀{i}{A : Set i}{a a' a'' : A}(e : a ≡ a')(e' : a' ≡ a'') → a ≡ a''
_◾_ {a = a} e e' = transport (λ x → a ≡ x) e' e

_⁻¹ : ∀{i}{A : Set i}{a a' : A}(e : a ≡ a') → a' ≡ a
_⁻¹ {a = a}{a'} e = transport (λ x → x ≡ a){a}{a'} e (refl _)

tryDisjI : l ≡ r → ⊥
tryDisjI = {!!}
  where
    P : I → Set
    P = indI (λ _ → Set) ⊤ ⊥ ((trconst s ⁻¹) ◾ {!!})

A→¬¬A : ∀{i}{A : Set i} → A → ((A → ⊥) → ⊥)
A→¬¬A a na = na a

InotDisjoint : (l ≡ r → ⊥) → ⊥
InotDisjoint = A→¬¬A s

-- if I was able to prove tryDisjI, Agda would be inconsistent:

AgdaInconsistent : ⊥
AgdaInconsistent = InotDisjoint tryDisjI

-- Booleans

data Bool : Set where
  true false : Bool

indBool : ∀{i}(P : Bool → Set i) → P true → P false → (b : Bool) → P b
indBool P ptrue pfalse true = ptrue
indBool P ptrue pfalse false = pfalse
