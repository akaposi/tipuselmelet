
Elaboration algorithm

presyntax: anything (finitary inductive type)
core syntax: intrinsically well-typed (CwF, CwA, etc.)

elab : presyntax -> core syntax

correctness of elab?

1. define a relation between core syntax and presyntax
  (erasure) (let's call this _~_)
  _~_ : CwF -> PreSyn -> Set

simply typed CwF + presyntax

(erasure function, but not actually a function, because we don't want
 to truncate)

((lam t) ~ (λ x. t')) iff (t ~ t')
app t u  ~ (t' u')    iff (t ~ t') and (u ~ u')
...

- soundness property of elab:

  elab : (t : PreSyntax) -> Maybe ((t' : CoreSyn) × t' ~ t)
  - if elab has this type, then it is sound

- completeness:
  (Dec = decidable)

  elab : (t : PreSyntax) -> Dec ((t' : CoreSynt) × t' ~ t)

  (usual "complete" (e.g. Hindley-Milner completeness))

  (classical formulation: whenever there is a way of elaborating
   presyntax, then elab finds a way of doing it)

- + uniqueness

  elab : (t : PreSyntax) -> Dec (isContr ((t' : CoreSynt) × t' ~ t))
  -- in practice : this only holds for simple theories: H-M
  --              some predicative restriction of System-F with
  --              some mandatory type annotations

  -- what does "completeness" depend on?
  --   depends on what presyntax is!
  --   depends on the explicitness of presyntax

  Agda cares more about uniqueness than Coq
   (trade-off between fragility & convenience)

Algorithms + solutions

  bidirectional checking:
     sometimes we already know the expected type
     sometimes we don't

  check : (Γ : Con)(t : PreTm)(A : Ty Γ) → Maybe ((t' : Tm Γ A) × (t' ~ t))
  infer : (Γ : Con)(t : PreTm) → Maybe ((A : Ty Γ) × (t' : Tm Γ A) × (t' ~ t))

  it's a bit weak:

    BoolRec: {A : Set} → Bool → A → A → A

    infer (BoolRec b t f)?
      we have a choice: which branch to check/infer

    naive solution: try both (exponential time)


More infrastructure:
- modal core theory (simple version of "crisp")
- modality : metavariables

Dual context:

  Ω|Γ ⊢ A type
  Ω|Γ ⊢ t : A

modal substitutions: Sub Ω₀ Ω₁, etc.

  Ω₀, α : A, Ω₁|Γ ⊢ α : A       -- metavariable

point: accummulate partial information about types and terms in Ωᵢ

-- H-M: order of traversal does not matter because we get the same info
-- in the end.

We still use bidirectional checking:

Modify type check/infer:

  action of metasubstituion on "normal" contexts:
    σ    : Sub Ω' Ω
    Γ    : Con Ω
    Γ[σ] : Con Ω'

  check :
    (Ω : MCon)(Γ : Con Ω)(t : PreTm)(A : Ty Γ)
    → Maybe ((Ω' : MCon) × (σ : Sub Ω' Ω) × (t' : Tm (Ω'|Γ[σ]) (A[σ])) × t' ~ t)

  infer :
    (Ω : MCon)(Γ : Con Ω)(t : PreTm)
    → Maybe ((Ω' : MCon) × (σ : Sub Ω' Ω) × (A : Ty (Ω'|Γ[σ]))
             × (t' : Tm (Ω'|Γ[σ]) A) × t' ~ t)

  create a new meta: weakening subst for modal context
    (p : Sub (Ω, α : A) Ω)

  solve a meta:
    context looks like : Ω, α : A
    Sub Ω (Ω, α : A)
    we construction this by giving a term  t : Tm (Ω|∙) A

_~_ for some toy Agda:

  (t' ~ t) iff t' can have _ filled and some extra implicit arguments/lambdas inserted

  starting/ending elab:

   Success:
    infer ∙ ∙ (t : PreTm)   -->  Just (∙, id, A, t', (p : t' ~ t))

What do we need to implement elab:
  - decidable conversion: in the core syntax
       (holds for ITT, doesn't hold for ETT)

  - "unification" : deciding existence of equalizers

    Unification in non-modal setting:
       t, u : Tm Γ A
       we want to find equalizer of t, u: (Γ' : Con) × (σ : Sub Γ' Γ)
         such that t[σ] ≡ u[σ]     + (Γ', σ) is also terminal

       classic in literature : "most general unifier" of t and u
                            (only weak equalizer classically)

      (holds for ETT, doesn't hold for ITT)
         In ITT, we have some approximations, like "Miller pattern unification"


-- ICFP paper
------------------------------------------------------------
observation: not enough to only have above modal TT to solve some problems

Agda can only insert an implicit λ, if we are in checking
mode, and we know that the checking is ({x : A} → B)

usually what should happen:
check Γ t ({x : A} → B)        t ≠ (λ {x}. t')
  let t' = check (Γ, x : A) t
  return (λ {x : A}. t')

Extension of the core theory in Agda:
  (x : A) → B
  {x : A} → B
In the core syntax, these behave exactly the same.
Elaboration makes choices based on explicit/implicit function types.

infer (t u) :
  if t has an inferred implicit function type,
  Agda applies t to fresh meta
  output: t {α₀}{α₁} u

what happens with insertion when types are not known?
  i.e. neutral types with meta head (α spine)

Agda assumes that unknown types are *not* implicit functions

In Haskell/ML setting this problem is called: "impredicative inference/instantiation"
(orthogonal to "impredicative" in type theory)

Solution:
 Extend core theory:
   Telescopes (generic notion of record types)
   ε   : empty telescopes
   _▶_ : extended telescope

Γ ⊢ Tel : U

  Γ ⊢ t : Tel
──────────────
 Γ ⊢ Rec t : U

 Γ ⊢ ε : Tel       (Rec ε) isomorphic to ⊤
 Γ ⊢ (x : A) ▶ B   (Rec ((x : A) ▶ B)) isomorphic to (Σ A (Rec B))

Another function type: telescope for domain, definitional currying

Γ ⊢ A : Tel   Γ, x : Rec A ⊢ B : U
───────────────────────────────────
      Γ ⊢ {x : A}*→ B : U

({x : ε}* → B) ≡ B
({x : (y : A) ▶ B}* → C) ≡ {y : A}{x : B}* → C

domain is a telescope
if the domain is known, the type computes to an iterated implicit function
type

+ equations for lambda + application
λ computes to iterated implicit λ
app computes to iterated implicit application

check Γ t {α}           -- α is neutral meta-headed type
   create a new meta A : Tel
   let t' = check (Γ, x : Rec A) t
   return (λ*{x : A}. t')

if A is refined, then implcit λ insertion can happen later

 ({x : A}* → B)[ A ↦ x : A' ▶ y : B' ▶ ε] ≡ ....

unify ({x : A}*→ B) with B'      s.t. B' is definitely *not* an implicit fun.
   unify A with ε
   unify B with B'

unify ({x : A}*→ B) with B'

Tel := Maybe U, Just A, Nothing
({x : A}* → B)[A ⊢> Just A]

------------------------------------------------------------
"Quick Look impredicativity"

  - looks at a neutral spine (t u₀ u₁ ... uₙ)
  - "pre-pass" on the spine which tries to finds some solutions
