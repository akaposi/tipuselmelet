module seminar20240502_szumi_nbe where

open import Agda.Builtin.Equality
open import Agda.Builtin.Sigma
open import Agda.Builtin.Unit
open import Agda.Builtin.Nat

infixr 2 _×_
_×_ : Set → Set → Set
A × B = Σ A λ a → B

module Typed where
  infixr 0 _⇒_
  infixl 2 _▹_
  infixl 9 _∘p _↑ _∘_ _[_]Var

  data Ty : Set where
    ⊥   : Ty
    _⇒_ : Ty → Ty → Ty

  data Con : Set where
    ◇   : Con
    _▹_ : Con → Ty → Con

  private variable
    Γ Δ Θ Ψ Ω : Con
    A B       : Ty

  data Var : Con → Ty → Set where
    zero : Var (Γ ▹ A) A
    suc  : Var Γ B → Var (Γ ▹ A) B

  data Tm : Con → Ty → Set where
    var : Var Γ A → Tm Γ A
    app : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    lam : Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)

  CNat : Ty
  CNat = (⊥ ⇒ ⊥) ⇒ ⊥ ⇒ ⊥

  church : Nat → Tm Γ CNat
  church n = lam (lam (go n))
    where
    go : Nat → Tm (Γ ▹ (⊥ ⇒ ⊥) ▹ ⊥) ⊥
    go zero    = var zero
    go (suc n) = app (var (suc zero)) (go n)

  thousand : Tm Γ CNat
  thousand =
    Let CNat ∋ church 5 In
    Let CNat ⇒ CNat ⇒ CNat ∋ ƛ ƛ ƛ ƛ # S S S Z · # S Z · (# S S Z · # S Z · # Z) In
    Let CNat ⇒ CNat ⇒ CNat ∋ ƛ ƛ ƛ ƛ # S S S Z · (# S S Z · # S Z) · # Z In
    Let CNat ∋ # S Z · # S S Z · # S S Z In
    Let CNat ∋ # S Z · # Z · # Z In
    Let CNat ∋ # S S Z · # S Z · # Z In
    # Z
    where
    infixr -1 Let_∋_In_
    infixr  0 ƛ_
    infixl 10 _·_
    infixr 11 S_ #_
    Z : Var (Γ ▹ A) A
    Z = zero
    S_ : Var Γ A → Var (Γ ▹ B) A
    S_ = suc
    #_ : Var Γ A → Tm Γ A
    #_ = var
    _·_ : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    _·_ = app
    ƛ_ : Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    ƛ_ = lam
    Let_∋_In_ : (A : Ty) → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
    Let A ∋ a In b = (ƛ b) · a

  data Wk : Con → Con → Set where
    id  : Wk Γ Γ
    _∘p : Wk Δ Γ → Wk (Δ ▹ A) Γ
    _↑  : Wk Δ Γ → Wk (Δ ▹ A) (Γ ▹ A)

  _∘_ : Wk Δ Γ → Wk Θ Δ → Wk Θ Γ
  γ      ∘ id     = γ
  γ      ∘ (δ ∘p) = γ ∘ δ ∘p
  id     ∘ (δ ↑)  = δ ↑
  (γ ∘p) ∘ (δ ↑)  = γ ∘ δ ∘p
  (γ ↑)  ∘ (δ ↑)  = γ ∘ δ ↑

  _[_]Var : Var Γ A → Wk Δ Γ → Var Δ A
  x     [ id   ]Var = x
  x     [ γ ∘p ]Var = suc (x [ γ ]Var)
  zero  [ γ ↑  ]Var = zero
  suc x [ γ ↑  ]Var = suc (x [ γ ]Var)

  module NbE where
    infixl 9 _[_]Ne _[_]Nf _[_]STy _[_]SCon

    data Nf : Con → Ty → Set
    data Ne : Con → Ty → Set

    data Nf where
      ne  : Ne Γ ⊥ → Nf Γ ⊥
      lam : Nf (Γ ▹ A) B → Nf Γ (A ⇒ B)

    data Ne where
      var : Var Γ A → Ne Γ A
      app : Ne Γ (A ⇒ B) → Nf Γ A → Ne Γ B

    Nf-emb : Nf Γ A → Tm Γ A
    Ne-emb : Ne Γ A → Tm Γ A

    Nf-emb (ne t)  = Ne-emb t
    Nf-emb (lam b) = lam (Nf-emb b)

    Ne-emb (var x)   = var x
    Ne-emb (app f a) = app (Ne-emb f) (Nf-emb a)

    _[_]Nf : Nf Γ A → Wk Δ Γ → Nf Δ A
    _[_]Ne : Ne Γ A → Wk Δ Γ → Ne Δ A

    ne t  [ γ ]Nf = ne (t [ γ ]Ne)
    lam b [ γ ]Nf = lam (b [ γ ↑ ]Nf)

    var x   [ γ ]Ne = var (x [ γ ]Var)
    app f a [ γ ]Ne = app (f [ γ ]Ne) (a [ γ ]Nf)

    ⟦_⟧Ty : Ty → Con → Set
    ⟦ ⊥     ⟧Ty Ω = Nf Ω ⊥
    ⟦ A ⇒ B ⟧Ty Ω = ∀ {Ψ} → Wk Ψ Ω → ⟦ A ⟧Ty Ψ → ⟦ B ⟧Ty Ψ

    _[_]STy : ⟦ A ⟧Ty Ω → Wk Ψ Ω → ⟦ A ⟧Ty Ψ
    _[_]STy {A = ⊥}     t ω     = t [ ω ]Nf
    _[_]STy {A = A ⇒ B} f ω ψ a = f (ω ∘ ψ) a

    ⟦_⟧Con : Con → Con → Set
    ⟦ ◇     ⟧Con Ω = ⊤
    ⟦ Γ ▹ A ⟧Con Ω = ⟦ Γ ⟧Con Ω × ⟦ A ⟧Ty Ω

    _[_]SCon : ⟦ Γ ⟧Con Ω → Wk Ψ Ω → ⟦ Γ ⟧Con Ψ
    _[_]SCon {Γ = ◇}     tt      ω = tt
    _[_]SCon {Γ = Γ ▹ A} (γ , a) ω = γ [ ω ]SCon , a [ ω ]STy

    ⟦_⟧Var : Var Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ zero  ⟧Var (γ , a) = a
    ⟦ suc x ⟧Var (γ , a) = ⟦ x ⟧Var γ

    ⟦_⟧Tm : Tm Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ var x   ⟧Tm γ     = ⟦ x ⟧Var γ
    ⟦ app f a ⟧Tm γ     = ⟦ f ⟧Tm γ id (⟦ a ⟧Tm γ)
    ⟦ lam b   ⟧Tm γ ω a = ⟦ b ⟧Tm (γ [ ω ]SCon , a)

    reify   : ⟦ A ⟧Ty Ω → Nf Ω A -- quote
    reflect : Ne Ω A → ⟦ A ⟧Ty Ω -- unquote

    reify {A = ⊥}     t = t
    reify {A = A ⇒ B} f = lam (reify (f (id ∘p) (reflect (var zero))))

    reflect {A = ⊥}     t     = ne t
    reflect {A = A ⇒ B} f ω a = reflect (app (f [ ω ]Ne) (reify a))

    reflect-id : ⟦ Γ ⟧Con Γ
    reflect-id {Γ = ◇}     = tt
    reflect-id {Γ = Γ ▹ A} = reflect-id [ id ∘p ]SCon , reflect (var zero)

    norm : Tm Γ A → Nf Γ A
    norm a = reify (⟦ a ⟧Tm reflect-id)

    test : Nf-emb (norm (thousand {◇})) ≡ church 1000
    test = refl

  module IndSem where
    infixl 9 _[_]Ne _[_]Nf _[_]STy _[_]SCon

    data Nf : Con → Ty → Set
    data Ne : Con → Ty → Set

    data Nf where
      ne  : Ne Γ ⊥ → Nf Γ ⊥
      lam : Nf (Γ ▹ A) B → Nf Γ (A ⇒ B)

    data Ne where
      var : Var Γ A → Ne Γ A
      app : Ne Γ (A ⇒ B) → Nf Γ A → Ne Γ B

    Nf-emb : Nf Γ A → Tm Γ A
    Ne-emb : Ne Γ A → Tm Γ A

    Nf-emb (ne t)  = Ne-emb t
    Nf-emb (lam b) = lam (Nf-emb b)

    Ne-emb (var x)   = var x
    Ne-emb (app f a) = app (Ne-emb f) (Nf-emb a)

    _[_]Nf : Nf Γ A → Wk Δ Γ → Nf Δ A
    _[_]Ne : Ne Γ A → Wk Δ Γ → Ne Δ A

    ne t  [ γ ]Nf = ne (t [ γ ]Ne)
    lam b [ γ ]Nf = lam (b [ γ ↑ ]Nf)

    var x   [ γ ]Ne = var (x [ γ ]Var)
    app f a [ γ ]Ne = app (f [ γ ]Ne) (a [ γ ]Nf)

    {-# NO_POSITIVITY_CHECK #-}
    data ⟦_⟧Ty : Ty → Con → Set where
      ne  : Ne Ω ⊥ → ⟦ ⊥ ⟧Ty Ω
      lam : (∀ {Ψ} → Wk Ψ Ω → ⟦ A ⟧Ty Ψ → ⟦ B ⟧Ty Ψ) → ⟦ A ⇒ B ⟧Ty Ω

    _[_]STy : ⟦ A ⟧Ty Ω → Wk Ψ Ω → ⟦ A ⟧Ty Ψ
    ne t  [ ω ]STy = ne (t [ ω ]Ne)
    lam f [ ω ]STy = lam λ ψ a → f (ω ∘ ψ) a

    data ⟦_⟧Con : Con → Con → Set where
      tt  : ⟦ ◇ ⟧Con Ω
      _,_ : ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω → ⟦ Γ ▹ A ⟧Con Ω

    _[_]SCon : ⟦ Γ ⟧Con Ω → Wk Ψ Ω → ⟦ Γ ⟧Con Ψ
    tt      [ ω ]SCon = tt
    (γ , a) [ ω ]SCon = γ [ ω ]SCon , a [ ω ]STy

    ⟦_⟧Var : Var Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ zero  ⟧Var (γ , a) = a
    ⟦ suc x ⟧Var (γ , a) = ⟦ x ⟧Var γ

    appS : ⟦ A ⇒ B ⟧Ty Ω → ⟦ A ⟧Ty Ω → ⟦ B ⟧Ty Ω
    appS (lam f) a = f id a

    ⟦_⟧Tm : Tm Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ var x   ⟧Tm γ = ⟦ x ⟧Var γ
    ⟦ app f a ⟧Tm γ = appS (⟦ f ⟧Tm γ) (⟦ a ⟧Tm γ)
    ⟦ lam b   ⟧Tm γ = lam λ ω a → ⟦ b ⟧Tm (γ [ ω ]SCon , a)

    reify   : ⟦ A ⟧Ty Ω → Nf Ω A -- quote
    reflect : Ne Ω A → ⟦ A ⟧Ty Ω -- unquote

    reify (ne t)  = ne t
    reify (lam f) = lam (reify (f (id ∘p) (reflect (var zero))))

    reflect {A = ⊥}     t = ne t
    reflect {A = A ⇒ B} f = lam λ ω a → reflect (app (f [ ω ]Ne) (reify a))

    reflect-id : ⟦ Γ ⟧Con Γ
    reflect-id {Γ = ◇}     = tt
    reflect-id {Γ = Γ ▹ A} = reflect-id [ id ∘p ]SCon , reflect (var zero)

    norm : Tm Γ A → Nf Γ A
    norm a = reify (⟦ a ⟧Tm reflect-id)

    test : Nf-emb (norm (thousand {◇})) ≡ church 1000
    test = refl

  module NoReflect where
    infixl 9 _[_]Ne _[_]Nf _[_]STy _[_]SCon

    data Nf : Con → Ty → Set
    data Ne : Con → Ty → Set

    data Nf where
      ne  : Ne Γ ⊥ → Nf Γ ⊥
      lam : Nf (Γ ▹ A) B → Nf Γ (A ⇒ B)

    data Ne where
      var : Var Γ A → Ne Γ A
      app : Ne Γ (A ⇒ B) → Nf Γ A → Ne Γ B

    Nf-emb : Nf Γ A → Tm Γ A
    Ne-emb : Ne Γ A → Tm Γ A

    Nf-emb (ne t)  = Ne-emb t
    Nf-emb (lam b) = lam (Nf-emb b)

    Ne-emb (var x)   = var x
    Ne-emb (app f a) = app (Ne-emb f) (Nf-emb a)

    _[_]Nf : Nf Γ A → Wk Δ Γ → Nf Δ A
    _[_]Ne : Ne Γ A → Wk Δ Γ → Ne Δ A

    ne t  [ γ ]Nf = ne (t [ γ ]Ne)
    lam b [ γ ]Nf = lam (b [ γ ↑ ]Nf)

    var x   [ γ ]Ne = var (x [ γ ]Var)
    app f a [ γ ]Ne = app (f [ γ ]Ne) (a [ γ ]Nf)

    {-# NO_POSITIVITY_CHECK #-}
    data ⟦_⟧Ty : Ty → Con → Set where
      ne  : Ne Ω A → ⟦ A ⟧Ty Ω
      lam : (∀ {Ψ} → Wk Ψ Ω → ⟦ A ⟧Ty Ψ → ⟦ B ⟧Ty Ψ) → ⟦ A ⇒ B ⟧Ty Ω

    _[_]STy : ⟦ A ⟧Ty Ω → Wk Ψ Ω → ⟦ A ⟧Ty Ψ
    ne a  [ ω ]STy = ne (a [ ω ]Ne)
    lam f [ ω ]STy = lam λ ψ a → f (ω ∘ ψ) a

    data ⟦_⟧Con : Con → Con → Set where
      tt  : ⟦ ◇ ⟧Con Ω
      _,_ : ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω → ⟦ Γ ▹ A ⟧Con Ω

    _[_]SCon : ⟦ Γ ⟧Con Ω → Wk Ψ Ω → ⟦ Γ ⟧Con Ψ
    tt      [ ω ]SCon = tt
    (γ , a) [ ω ]SCon = γ [ ω ]SCon , a [ ω ]STy

    ⟦_⟧Var : Var Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ zero  ⟧Var (γ , a) = a
    ⟦ suc x ⟧Var (γ , a) = ⟦ x ⟧Var γ

    reify : ⟦ A ⟧Ty Ω → Nf Ω A -- quote
    appS : ⟦ A ⇒ B ⟧Ty Ω → ⟦ A ⟧Ty Ω → ⟦ B ⟧Ty Ω

    reify {A = ⊥}     (ne t) = ne t
    reify {A = A ⇒ B} f      = lam (reify (appS (f [ id ∘p ]STy) (ne (var zero))))

    appS (lam f) a = f id a
    appS (ne f)  a = ne (app f (reify a))

    ⟦_⟧Tm : Tm Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ var x   ⟧Tm γ = ⟦ x ⟧Var γ
    ⟦ app f a ⟧Tm γ = appS (⟦ f ⟧Tm γ) (⟦ a ⟧Tm γ)
    ⟦ lam b   ⟧Tm γ = lam λ ω a → ⟦ b ⟧Tm (γ [ ω ]SCon , a)

    reflect-id : ⟦ Γ ⟧Con Γ
    reflect-id {Γ = ◇}     = tt
    reflect-id {Γ = Γ ▹ A} = reflect-id [ id ∘p ]SCon , ne (var zero)

    norm : Tm Γ A → Nf Γ A
    norm a = reify (⟦ a ⟧Tm reflect-id)

    test : Nf-emb (norm (thousand {◇})) ≡ church 1000
    test = refl

  module SemNe where
    infixl 9 _[_]Ne _[_]Nf _[_]STy _[_]SCon

    data Nf : Con → Ty → Set
    data Ne : Con → Ty → Set

    data Nf where
      ne  : Ne Γ ⊥ → Nf Γ ⊥
      lam : Nf (Γ ▹ A) B → Nf Γ (A ⇒ B)

    data Ne where
      var : Var Γ A → Ne Γ A
      app : Ne Γ (A ⇒ B) → Nf Γ A → Ne Γ B

    Nf-emb : Nf Γ A → Tm Γ A
    Ne-emb : Ne Γ A → Tm Γ A

    Nf-emb (ne t)  = Ne-emb t
    Nf-emb (lam b) = lam (Nf-emb b)

    Ne-emb (var x)   = var x
    Ne-emb (app f a) = app (Ne-emb f) (Nf-emb a)

    _[_]Nf : Nf Γ A → Wk Δ Γ → Nf Δ A
    _[_]Ne : Ne Γ A → Wk Δ Γ → Ne Δ A

    ne t  [ γ ]Nf = ne (t [ γ ]Ne)
    lam b [ γ ]Nf = lam (b [ γ ↑ ]Nf)

    var x   [ γ ]Ne = var (x [ γ ]Var)
    app f a [ γ ]Ne = app (f [ γ ]Ne) (a [ γ ]Nf)

    {-# NO_POSITIVITY_CHECK #-}
    data ⟦_⟧Ty : Ty → Con → Set
    data SNe : Con → Ty → Set

    data ⟦_⟧Ty where
      ne  : SNe Ω A → ⟦ A ⟧Ty Ω
      lam : (∀ {Ψ} → Wk Ψ Ω → ⟦ A ⟧Ty Ψ → ⟦ B ⟧Ty Ψ) → ⟦ A ⇒ B ⟧Ty Ω

    data SNe where
      var : Var Ω A → SNe Ω A
      app : SNe Ω (A ⇒ B) → ⟦ A ⟧Ty Ω → SNe Ω B

    _[_]STy : ⟦ A ⟧Ty Ω → Wk Ψ Ω → ⟦ A ⟧Ty Ψ
    _[_]SNe : SNe Ω A → Wk Ψ Ω → SNe Ψ A

    ne a  [ ω ]STy = ne (a [ ω ]SNe)
    lam f [ ω ]STy = lam λ ψ a → f (ω ∘ ψ) a

    var x   [ ω ]SNe = var (x [ ω ]Var)
    app f a [ ω ]SNe = app (f [ ω ]SNe) (a [ ω ]STy)

    data ⟦_⟧Con : Con → Con → Set where
      tt  : ⟦ ◇ ⟧Con Ω
      _,_ : ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω → ⟦ Γ ▹ A ⟧Con Ω

    _[_]SCon : ⟦ Γ ⟧Con Ω → Wk Ψ Ω → ⟦ Γ ⟧Con Ψ
    tt      [ ω ]SCon = tt
    (γ , a) [ ω ]SCon = γ [ ω ]SCon , a [ ω ]STy

    ⟦_⟧Var : Var Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ zero  ⟧Var (γ , a) = a
    ⟦ suc x ⟧Var (γ , a) = ⟦ x ⟧Var γ

    appS : ⟦ A ⇒ B ⟧Ty Ω → ⟦ A ⟧Ty Ω → ⟦ B ⟧Ty Ω
    appS (lam f) a = f id a
    appS (ne f)  a = ne (app f a)

    ⟦_⟧Tm : Tm Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ var x   ⟧Tm γ = ⟦ x ⟧Var γ
    ⟦ app f a ⟧Tm γ = appS (⟦ f ⟧Tm γ) (⟦ a ⟧Tm γ)
    ⟦ lam b   ⟧Tm γ = lam λ ω a → ⟦ b ⟧Tm (γ [ ω ]SCon , a)

    {-# TERMINATING #-}
    reify : ⟦ A ⟧Ty Ω → Nf Ω A -- quote
    reifyN : SNe Ω A → Ne Ω A

    reify {A = ⊥}     (ne t) = ne (reifyN t)
    reify {A = A ⇒ B} f      = lam (reify (appS (f [ id ∘p ]STy) (ne (var zero))))

    reifyN (var x)   = var x
    reifyN (app f a) = app (reifyN f) (reify a)

    reflect-id : ⟦ Γ ⟧Con Γ
    reflect-id {Γ = ◇}     = tt
    reflect-id {Γ = Γ ▹ A} = reflect-id [ id ∘p ]SCon , ne (var zero)

    norm : Tm Γ A → Nf Γ A
    norm a = reify (⟦ a ⟧Tm reflect-id)

    test : Nf-emb (norm (thousand {◇})) ≡ church 1000
    test = refl

  module Defunc where
    infixl 9 _[_]Ne _[_]Nf _[_]STy _[_]SCon

    data Nf : Con → Ty → Set
    data Ne : Con → Ty → Set

    data Nf where
      ne  : Ne Γ ⊥ → Nf Γ ⊥
      lam : Nf (Γ ▹ A) B → Nf Γ (A ⇒ B)

    data Ne where
      var : Var Γ A → Ne Γ A
      app : Ne Γ (A ⇒ B) → Nf Γ A → Ne Γ B

    Nf-emb : Nf Γ A → Tm Γ A
    Ne-emb : Ne Γ A → Tm Γ A

    Nf-emb (ne t)  = Ne-emb t
    Nf-emb (lam b) = lam (Nf-emb b)

    Ne-emb (var x)   = var x
    Ne-emb (app f a) = app (Ne-emb f) (Nf-emb a)

    _[_]Nf : Nf Γ A → Wk Δ Γ → Nf Δ A
    _[_]Ne : Ne Γ A → Wk Δ Γ → Ne Δ A

    ne t  [ γ ]Nf = ne (t [ γ ]Ne)
    lam b [ γ ]Nf = lam (b [ γ ↑ ]Nf)

    var x   [ γ ]Ne = var (x [ γ ]Var)
    app f a [ γ ]Ne = app (f [ γ ]Ne) (a [ γ ]Nf)

    data ⟦_⟧Ty : Ty → Con → Set
    data ⟦_⟧Con : Con → Con → Set
    data SNe : Con → Ty → Set

    data ⟦_⟧Ty where
      ne  : SNe Ω A → ⟦ A ⟧Ty Ω
      lam : Tm (Γ ▹ A) B → ⟦ Γ ⟧Con Ω → ⟦ A ⇒ B ⟧Ty Ω

    data ⟦_⟧Con where
      tt  : ⟦ ◇ ⟧Con Ω
      _,_ : ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω → ⟦ Γ ▹ A ⟧Con Ω

    data SNe where
      var : Var Ω A → SNe Ω A
      app : SNe Ω (A ⇒ B) → ⟦ A ⟧Ty Ω → SNe Ω B

    _[_]STy : ⟦ A ⟧Ty Ω → Wk Ψ Ω → ⟦ A ⟧Ty Ψ
    _[_]SCon : ⟦ Γ ⟧Con Ω → Wk Ψ Ω → ⟦ Γ ⟧Con Ψ
    _[_]SNe : SNe Ω A → Wk Ψ Ω → SNe Ψ A

    ne a    [ ω ]STy = ne (a [ ω ]SNe)
    lam b γ [ ω ]STy = lam b (γ [ ω ]SCon)

    tt      [ ω ]SCon = tt
    (γ , a) [ ω ]SCon = γ [ ω ]SCon , a [ ω ]STy

    var x   [ ω ]SNe = var (x [ ω ]Var)
    app f a [ ω ]SNe = app (f [ ω ]SNe) (a [ ω ]STy)

    ⟦_⟧Var : Var Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ zero  ⟧Var (γ , a) = a
    ⟦ suc x ⟧Var (γ , a) = ⟦ x ⟧Var γ

    {-# TERMINATING #-}
    ⟦_⟧Tm : Tm Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    appS : ⟦ A ⇒ B ⟧Ty Ω → ⟦ A ⟧Ty Ω → ⟦ B ⟧Ty Ω

    ⟦ var x   ⟧Tm γ = ⟦ x ⟧Var γ
    ⟦ app f a ⟧Tm γ = appS (⟦ f ⟧Tm γ) (⟦ a ⟧Tm γ)
    ⟦ lam b   ⟧Tm γ = lam b γ

    appS (lam b γ) a = ⟦ b ⟧Tm (γ , a)
    appS (ne f)  a = ne (app f a)

    {-# TERMINATING #-}
    reify : ⟦ A ⟧Ty Ω → Nf Ω A -- quote
    reifyN : SNe Ω A → Ne Ω A

    reify {A = ⊥}     (ne t) = ne (reifyN t)
    reify {A = A ⇒ B} f      = lam (reify (appS (f [ id ∘p ]STy) (ne (var zero))))

    reifyN (var x)   = var x
    reifyN (app f a) = app (reifyN f) (reify a)

    reflect-id : ⟦ Γ ⟧Con Γ
    reflect-id {Γ = ◇}     = tt
    reflect-id {Γ = Γ ▹ A} = reflect-id [ id ∘p ]SCon , ne (var zero)

    norm : Tm Γ A → Nf Γ A
    norm a = reify (⟦ a ⟧Tm reflect-id)

    test : Nf-emb (norm (thousand {◇})) ≡ church 1000
    test = refl

  module NoEta where
    infixl 9 _[_]Ne _[_]Nf _[_]STy _[_]SCon

    data Nf : Con → Ty → Set
    data Ne : Con → Ty → Set

    data Nf where
      ne  : Ne Γ A → Nf Γ A
      lam : Nf (Γ ▹ A) B → Nf Γ (A ⇒ B)

    data Ne where
      var : Var Γ A → Ne Γ A
      app : Ne Γ (A ⇒ B) → Nf Γ A → Ne Γ B

    Nf-emb : Nf Γ A → Tm Γ A
    Ne-emb : Ne Γ A → Tm Γ A

    Nf-emb (ne a)  = Ne-emb a
    Nf-emb (lam b) = lam (Nf-emb b)

    Ne-emb (var x)   = var x
    Ne-emb (app f a) = app (Ne-emb f) (Nf-emb a)

    _[_]Nf : Nf Γ A → Wk Δ Γ → Nf Δ A
    _[_]Ne : Ne Γ A → Wk Δ Γ → Ne Δ A

    ne a  [ γ ]Nf = ne (a [ γ ]Ne)
    lam b [ γ ]Nf = lam (b [ γ ↑ ]Nf)

    var x   [ γ ]Ne = var (x [ γ ]Var)
    app f a [ γ ]Ne = app (f [ γ ]Ne) (a [ γ ]Nf)

    {-# NO_POSITIVITY_CHECK #-}
    data ⟦_⟧Ty : Ty → Con → Set
    data SNe : Con → Ty → Set

    data ⟦_⟧Ty where
      ne  : SNe Ω A → ⟦ A ⟧Ty Ω
      lam : (∀ {Ψ} → Wk Ψ Ω → ⟦ A ⟧Ty Ψ → ⟦ B ⟧Ty Ψ) → ⟦ A ⇒ B ⟧Ty Ω

    data SNe where
      var : Var Ω A → SNe Ω A
      app : SNe Ω (A ⇒ B) → ⟦ A ⟧Ty Ω → SNe Ω B

    _[_]STy : ⟦ A ⟧Ty Ω → Wk Ψ Ω → ⟦ A ⟧Ty Ψ
    _[_]SNe : SNe Ω A → Wk Ψ Ω → SNe Ψ A

    ne a  [ ω ]STy = ne (a [ ω ]SNe)
    lam f [ ω ]STy = lam λ ψ a → f (ω ∘ ψ) a

    var x   [ ω ]SNe = var (x [ ω ]Var)
    app f a [ ω ]SNe = app (f [ ω ]SNe) (a [ ω ]STy)

    data ⟦_⟧Con : Con → Con → Set where
      tt  : ⟦ ◇ ⟧Con Ω
      _,_ : ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω → ⟦ Γ ▹ A ⟧Con Ω

    _[_]SCon : ⟦ Γ ⟧Con Ω → Wk Ψ Ω → ⟦ Γ ⟧Con Ψ
    tt      [ ω ]SCon = tt
    (γ , a) [ ω ]SCon = γ [ ω ]SCon , a [ ω ]STy

    ⟦_⟧Var : Var Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ zero  ⟧Var (γ , a) = a
    ⟦ suc x ⟧Var (γ , a) = ⟦ x ⟧Var γ

    appS : ⟦ A ⇒ B ⟧Ty Ω → ⟦ A ⟧Ty Ω → ⟦ B ⟧Ty Ω
    appS (lam f) a = f id a
    appS (ne f)  a = ne (app f a)

    ⟦_⟧Tm : Tm Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ var x   ⟧Tm γ = ⟦ x ⟧Var γ
    ⟦ app f a ⟧Tm γ = appS (⟦ f ⟧Tm γ) (⟦ a ⟧Tm γ)
    ⟦ lam b   ⟧Tm γ = lam λ ω a → ⟦ b ⟧Tm (γ [ ω ]SCon , a)

    reify : ⟦ A ⟧Ty Ω → Nf Ω A -- quote
    reifyN : SNe Ω A → Ne Ω A

    reify (ne a)  = ne (reifyN a)
    reify (lam f) = lam (reify (f (id ∘p) (ne (var zero))))

    reifyN (var x)   = var x
    reifyN (app f a) = app (reifyN f) (reify a)

    reflect-id : ⟦ Γ ⟧Con Γ
    reflect-id {Γ = ◇}     = tt
    reflect-id {Γ = Γ ▹ A} = reflect-id [ id ∘p ]SCon , ne (var zero)

    norm : Tm Γ A → Nf Γ A
    norm a = reify (⟦ a ⟧Tm reflect-id)

    test : Nf-emb (norm (thousand {◇})) ≡ church 1000
    test = refl

  module NoNf where
    infixl 9 _[_]STy _[_]SCon

    {-# NO_POSITIVITY_CHECK #-}
    data ⟦_⟧Ty : Ty → Con → Set
    data SNe : Con → Ty → Set

    data ⟦_⟧Ty where
      ne  : SNe Ω A → ⟦ A ⟧Ty Ω
      lam : (∀ {Ψ} → Wk Ψ Ω → ⟦ A ⟧Ty Ψ → ⟦ B ⟧Ty Ψ) → ⟦ A ⇒ B ⟧Ty Ω

    data SNe where
      var : Var Ω A → SNe Ω A
      app : SNe Ω (A ⇒ B) → ⟦ A ⟧Ty Ω → SNe Ω B

    _[_]STy : ⟦ A ⟧Ty Ω → Wk Ψ Ω → ⟦ A ⟧Ty Ψ
    _[_]SNe : SNe Ω A → Wk Ψ Ω → SNe Ψ A

    ne a  [ ω ]STy = ne (a [ ω ]SNe)
    lam f [ ω ]STy = lam λ ψ a → f (ω ∘ ψ) a

    var x   [ ω ]SNe = var (x [ ω ]Var)
    app f a [ ω ]SNe = app (f [ ω ]SNe) (a [ ω ]STy)

    data ⟦_⟧Con : Con → Con → Set where
      tt  : ⟦ ◇ ⟧Con Ω
      _,_ : ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω → ⟦ Γ ▹ A ⟧Con Ω

    _[_]SCon : ⟦ Γ ⟧Con Ω → Wk Ψ Ω → ⟦ Γ ⟧Con Ψ
    tt      [ ω ]SCon = tt
    (γ , a) [ ω ]SCon = γ [ ω ]SCon , a [ ω ]STy

    ⟦_⟧Var : Var Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ zero  ⟧Var (γ , a) = a
    ⟦ suc x ⟧Var (γ , a) = ⟦ x ⟧Var γ

    appS : ⟦ A ⇒ B ⟧Ty Ω → ⟦ A ⟧Ty Ω → ⟦ B ⟧Ty Ω
    appS (lam f) a = f id a
    appS (ne f)  a = ne (app f a)

    ⟦_⟧Tm : Tm Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ var x   ⟧Tm γ = ⟦ x ⟧Var γ
    ⟦ app f a ⟧Tm γ = appS (⟦ f ⟧Tm γ) (⟦ a ⟧Tm γ)
    ⟦ lam b   ⟧Tm γ = lam λ ω a → ⟦ b ⟧Tm (γ [ ω ]SCon , a)

    reify : ⟦ A ⟧Ty Ω → Tm Ω A -- quote
    reifyN : SNe Ω A → Tm Ω A

    reify (ne a)  = reifyN a
    reify (lam f) = lam (reify (f (id ∘p) (ne (var zero))))

    reifyN (var x)   = var x
    reifyN (app f a) = app (reifyN f) (reify a)

    reflect-id : ⟦ Γ ⟧Con Γ
    reflect-id {Γ = ◇}     = tt
    reflect-id {Γ = Γ ▹ A} = reflect-id [ id ∘p ]SCon , ne (var zero)

    norm : Tm Γ A → Tm Γ A
    norm a = reify (⟦ a ⟧Tm reflect-id)

    test : norm (thousand {◇}) ≡ church 1000
    test = refl

  module MergeSNe where
    infixl 9 _[_]STy _[_]SCon

    {-# NO_POSITIVITY_CHECK #-}
    data ⟦_⟧Ty : Ty → Con → Set where
      var : Var Ω A → ⟦ A ⟧Ty Ω
      app : ⟦ A ⇒ B ⟧Ty Ω → ⟦ A ⟧Ty Ω → ⟦ B ⟧Ty Ω
      lam : (∀ {Ψ} → Wk Ψ Ω → ⟦ A ⟧Ty Ψ → ⟦ B ⟧Ty Ψ) → ⟦ A ⇒ B ⟧Ty Ω

    _[_]STy : ⟦ A ⟧Ty Ω → Wk Ψ Ω → ⟦ A ⟧Ty Ψ
    var x   [ ω ]STy = var (x [ ω ]Var)
    app f a [ ω ]STy = app (f [ ω ]STy) (a [ ω ]STy)
    lam f   [ ω ]STy = lam λ ψ a → f (ω ∘ ψ) a

    data ⟦_⟧Con : Con → Con → Set where
      tt  : ⟦ ◇ ⟧Con Ω
      _,_ : ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω → ⟦ Γ ▹ A ⟧Con Ω

    _[_]SCon : ⟦ Γ ⟧Con Ω → Wk Ψ Ω → ⟦ Γ ⟧Con Ψ
    tt      [ ω ]SCon = tt
    (γ , a) [ ω ]SCon = γ [ ω ]SCon , a [ ω ]STy

    ⟦_⟧Var : Var Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ zero  ⟧Var (γ , a) = a
    ⟦ suc x ⟧Var (γ , a) = ⟦ x ⟧Var γ

    appS : ⟦ A ⇒ B ⟧Ty Ω → ⟦ A ⟧Ty Ω → ⟦ B ⟧Ty Ω
    appS (lam f) a = f id a
    appS f       a = app f a

    ⟦_⟧Tm : Tm Γ A → ⟦ Γ ⟧Con Ω → ⟦ A ⟧Ty Ω
    ⟦ var x   ⟧Tm γ = ⟦ x ⟧Var γ
    ⟦ app f a ⟧Tm γ = appS (⟦ f ⟧Tm γ) (⟦ a ⟧Tm γ)
    ⟦ lam b   ⟧Tm γ = lam λ ω a → ⟦ b ⟧Tm (γ [ ω ]SCon , a)

    reify : ⟦ A ⟧Ty Ω → Tm Ω A -- quote
    reify (var x)   = var x
    reify (app f a) = app (reify f) (reify a)
    reify (lam f)   = lam (reify (f (id ∘p) (var zero)))

    reflect-id : ⟦ Γ ⟧Con Γ
    reflect-id {Γ = ◇}     = tt
    reflect-id {Γ = Γ ▹ A} = reflect-id [ id ∘p ]SCon , var zero

    norm : Tm Γ A → Tm Γ A
    norm a = reify (⟦ a ⟧Tm reflect-id)

    test : norm (thousand {◇}) ≡ church 1000
    test = refl

module Untyped where
  postulate err : {A : Set} → A

  Var : Set
  Var = Nat

  data Tm : Set where
    var : Var → Tm
    app : Tm → Tm → Tm
    lam : Tm → Tm

  church : Nat → Tm
  church n = lam (lam (go n))
    where
    go : Nat → Tm
    go zero    = var 0
    go (suc n) = app (var 1) (go n)

  thousand : Tm
  thousand =
    Let church 5 In
    Let ƛ ƛ ƛ ƛ # 3 · # 1 · (# 2 · # 1 · # 0) In
    Let ƛ ƛ ƛ ƛ # 3 · (# 2 · # 1) · # 0 In
    Let # 1 · # 2 · # 2 In
    Let # 1 · # 0 · # 0 In
    Let # 2 · # 1 · # 0 In
    # 0
    where
    infixl 10 _·_
    infixr 0 ƛ_
    infixr -1 Let_In_
    # = var
    _·_ = app
    ƛ_ = lam
    Let_In_ : Tm → Tm → Tm
    Let a In b = (ƛ b) · a

  module NbE where
    infixl 9 _∘p _↑ _∘_ _[_]Val _[_]Env

    data Wk : Set where
      id  : Wk
      _∘p : Wk → Wk
      _↑  : Wk → Wk

    _∘_ : Wk → Wk → Wk
    ω      ∘ id     = ω
    ω      ∘ (ψ ∘p) = ω ∘ ψ ∘p
    id     ∘ (ψ ↑)  = ψ ↑
    (ω ∘p) ∘ (ψ ↑)  = ω ∘ ψ ∘p
    (ω ↑)  ∘ (ψ ↑)  = ω ∘ ψ ↑

    infixl 9 _[_]Var
    _[_]Var : Var → Wk → Var
    a     [ id   ]Var = a
    a     [ ω ∘p ]Var = suc (a [ ω ]Var)
    zero  [ ω ↑  ]Var = zero
    suc a [ ω ↑  ]Var = suc (a [ ω ]Var)

    {-# NO_POSITIVITY_CHECK #-}
    data Val : Set where
      var : Var → Val
      app : Val → Val → Val
      lam : (Wk → Val → Val) → Val

    _[_]Val : Val → Wk → Val
    var x   [ ω ]Val = var (x [ ω ]Var)
    app f a [ ω ]Val = app (f [ ω ]Val) (a [ ω ]Val)
    lam f   [ ω ]Val = lam λ ψ a → f (ω ∘ ψ) a

    data Env : Set where
      tt  : Env
      _,_ : Env → Val → Env

    _[_]Env : Env → Wk → Env
    tt      [ ω ]Env = tt
    (γ , a) [ ω ]Env = γ [ ω ]Env , a [ ω ]Val

    lookup : Var → Env → Val
    lookup zero    (γ , a) = a
    lookup (suc x) (γ , a) = lookup x γ
    lookup _       _       = err

    appV : Val → Val → Val
    appV (lam f) a = f id a
    appV f       a = app f a

    eval : Tm → Env → Val
    eval (var x)   γ = lookup x γ
    eval (app f a) γ = appV (eval f γ) (eval a γ)
    eval (lam b)   γ = lam λ ω a → eval b (γ [ ω ]Env , a)

    reify : Val → Tm -- quote
    reify (var x)   = var x
    reify (app f a) = app (reify f) (reify a)
    reify (lam f)   = lam (reify (f (id ∘p) (var zero)))

    reflect-id : Nat → Env
    reflect-id zero    = tt
    reflect-id (suc Γ) = reflect-id Γ [ id ∘p ]Env , var zero

    norm : Nat → Tm → Tm
    norm Γ a = reify (eval a (reflect-id Γ))

    test : norm 0 thousand ≡ church 1000
    test = refl

  module Levels where
    Lvl : Set
    Lvl = Nat

    {-# NO_POSITIVITY_CHECK #-}
    data Val : Set where
      lvl : Lvl → Val
      app : Val → Val → Val
      lam : (Val → Val) → Val

    data Env : Set where
      tt  : Env
      _,_ : Env → Val → Env

    lookup : Var → Env → Val
    lookup zero    (γ , a) = a
    lookup (suc x) (γ , a) = lookup x γ
    lookup _       _       = err

    appV : Val → Val → Val
    appV (lam f) a = f a
    appV f       a = app f a

    eval : Tm → Env → Val
    eval (var x)   γ = lookup x γ
    eval (app f a) γ = appV (eval f γ) (eval a γ)
    eval (lam b)   γ = lam λ a → eval b (γ , a)

    reify : Lvl → Val → Tm -- quote
    reify Γ (lvl l)   = var (Γ - l - 1)
    reify Γ (app f a) = app (reify Γ f) (reify Γ a)
    reify Γ (lam f)   = lam (reify (suc Γ) (f (lvl Γ)))

    reflect-id : Nat → Env
    reflect-id zero    = tt
    reflect-id (suc Γ) = reflect-id Γ , lvl Γ

    norm : Nat → Tm → Tm
    norm Γ a = reify Γ (eval a (reflect-id Γ))

    test : norm 0 thousand ≡ church 1000
    test = refl

  module Defunc where
    Lvl : Set
    Lvl = Nat

    data Val  : Set
    data Env  : Set
    data Clos : Set

    data Val where
      lvl : Lvl → Val
      app : Val → Val → Val
      lam : Clos → Val

    data Env where
      tt  : Env
      _,_ : Env → Val → Env

    data Clos where
      clos : Tm → Env → Clos

    lookup : Var → Env → Val
    lookup zero    (γ , a) = a
    lookup (suc x) (γ , a) = lookup x γ
    lookup _       _       = err

    {-# TERMINATING #-}
    eval  : Tm → Env → Val
    appV  : Val → Val → Val
    appCl : Clos → Val → Val

    eval (var x)   γ = lookup x γ
    eval (app f a) γ = appV (eval f γ) (eval a γ)
    eval (lam b)   γ = lam (clos b γ)

    appV (lam cl) a = appCl cl a
    appV f        a = app f a

    appCl (clos b γ) a = eval b (γ , a)

    {-# TERMINATING #-}
    reify : Lvl → Val → Tm -- quote
    reify Γ (lvl l)   = var (Γ - l - 1)
    reify Γ (app f a) = app (reify Γ f) (reify Γ a)
    reify Γ (lam f)   = lam (reify (suc Γ) (appCl f (lvl Γ)))

    reflect-id : Nat → Env
    reflect-id zero    = tt
    reflect-id (suc Γ) = reflect-id Γ , lvl Γ

    norm : Nat → Tm → Tm
    norm Γ a = reify Γ (eval a (reflect-id Γ))

    test : norm 0 thousand ≡ church 1000
    test = refl
