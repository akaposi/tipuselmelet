Title: The Probability of Excluded Middle

Abstract:
In usual classical logic, "P or not P" holds for any proposition P, and "not not P" is always equivalent to P.
However, in Martin-Löf type theory we typically use intuitionistic logic, where the former need not hold, and
only one half of the latter equivalence obtains. So, in a model of intuitionistic logic we can ask what’s the
probability that a randomly chosen proposition P obeys "P or not P". And the surprising answer is "at most 2/3"
- or else your model is classical!  In the talk, I will explain this and related results (all joint work with
Ben Bumpus [1][2]). You'll be fine if you know basic Agda or Coq - no prior experience with models is required.

[1] Ben M. BUMPUS, Zoltan A. KOCSIS. DEGREE OF SATISFIABILITY IN HEYTING ALGEBRAS. The Journal of Symbolic
Logic. Published online 2024:1-19. doi:10.1017/jsl.2024.2 
[2] https://johncarlosbaez.wordpress.com/2024/03/13/the-probability-of-the-law-of-excluded-middle/
