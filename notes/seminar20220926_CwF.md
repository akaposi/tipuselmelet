# Category with Families

Sorts
=====
Con : Set
(Γ Δ ...)

Sub : Con → Con → Set
(γ δ ...)

Ty : Con → Set
(A B ...)
<!-- example: Vec n Bool : Ty (◇▹Nat) -->

Tm : (Γ : Con) → Ty Γ → Set
(a b ...)


Operators
=========
◇ : Con
_▹_ : (Γ : Con) → (A : Ty Γ) → Con
<!-- example: ◇▹A▹B... -->

Isomorphism (extended substitution, depended pair of substitution and term)
Sub Δ (Γ▹A) ≌ Σ (Sub Δ Γ) (λ γ → Tm Δ A[γ])
→ : (p∘_, q[_])
← : _,_

         γ
Δ      ----> Nat
Vec[γ] <---- Vec

_[_] : Ty Γ → Sub Δ Γ → Ty Δ
_[_] : {A : Ty Γ} → Tm Γ Α → (γ : Sub Δ Γ) → Tm Δ A[γ]

ε : Sub Γ ◇
_,_ : {A : Ty Γ} → (γ : Sub Δ Γ) → Tm Δ (A[γ]) → Sub Δ (Γ▹A)

εη : {σ : Sub Γ ◇} → σ = ε
▹η : {γa : Sub Δ (Γ▹A)} → γa = (p ∘ γa, q[γa])
<!-- ▹η : (p, q) = id -->
<!-- ,∘ : (γ, a)[δ] = (γ∘δ, a[δ]) -->

id : Sub Γ Γ

<!-- addOne : Tm (◇▹Nat) Nat -->
<!-- addOne = (n + 1) -->

<!-- addOne' : Tm (◇▹Bool) Nat -->
<!-- addOne' = addOne[natFromBool] -->

<!-- natFromBool : Sub (◇▹Bool) (◇▹Nat) -->
<!-- natFromBool = (ε , if b then 1 else 0) -->

<!--             ib   in      ob   on -->
<!-- ex : Sub (◇▹Bool▹Nat) (◇▹Bool▹Nat) -->
<!-- ex = [ob ↦ not ib || in < 5; on ↦ if ib then in + 3 else in * 2] -->

<!-- (n + 1)[n ↦ 2] = (2 + 1) -->

<!-- natFromBool : Bool → Nat -->
<!-- natFromBool b = if b then 1 else 0 -->

             v₁   v₀
ex : Sub (◇▹Bool▹Nat) (◇▹Bool▹Nat)
ex = (ε, not v₁ || v₀ < 5, if v₁ then v₀ + 3 else v₀ * 2)

p : {A : Ty Γ} → Sub (Γ▹A) Γ
q : {A : Ty Γ} → Tm (Γ▹A) A[p]

v₀ : Tm (Γ▹A▹B▹C) C[p]
v₀ = q

v₁ : Tm (Γ▹A▹B▹C) B[p][p]
v₁ : q[p]

v₂ : Tm (Γ▹A▹B▹C) ((A[p{A}])[p{B}])[p{C}]
v₂ : Tm (Γ▹A▹B▹C) A[p³]
v₂ = (q[p])[p]

_∘_ : Sub Δ Γ → Sub Ω Δ → Sub Ω Γ

idl : {γ : Sub Δ Γ} → id ∘ γ = γ
idr : {γ : Sub Δ Γ} → γ ∘ id = γ
[id] : A[id] = A
[id] : a[id] = a

[∘]ᵀ : A[γ][δ] = A[γ∘δ]
[∘]ᵗ : a[γ][δ] = a[γ∘δ]

p² = p∘p : Sub (Γ▹A▹B▹C) (Γ▹A)

ass : {γ : Sub Δ Γ} → {δ : Sub Ω Δ} → {ω : Sub Θ Ω}
      ((γ ∘ δ) ∘ ω) = (γ ∘ (δ ∘ ω))

▹β₁ : {γ : Sub Δ Γ} → {a : Tm Δ A[γ]} → p ∘ (γ , a) = γ
▹β₂ : {γ : Sub Δ Γ} → {a : Tm Δ A[γ]} → q[γ , a] = a

--

Bool : Ty ◇
true : Tm ◇ Bool
false : Tm ◇ Bool
if_then_else_ : Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A

Boolβ₁ : if true then t else f = t
Boolβ₂ : if false then t else f = f

ite[] : {γ : Sub Δ Γ} → (if b then t else f)[γ] = if b[γ] then t[γ] else f[γ]
