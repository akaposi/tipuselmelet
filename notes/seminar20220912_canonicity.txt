CwF:
  Con : Set                              category
  Sub : Con → Con → Set
  _∘_ : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  id  : Sub Γ Γ
  ass,idl,idr
  ◆   : Con                              term.obj.
  ε   : Sub Γ ◆
  η law
  Ty  : Con → Set                        family
  _[_] : Ty Γ → Sub Δ Γ → Ty Δ
  [∘],[id]
  Tm  : (Γ : Con) → Ty Γ → Set
  _[_] : Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A[γ])
  [∘],[id]
  _▹_ : (Γ:Con)→Ty Γ→Con
  _,_ : (γ:Sub Δ Γ)×Tm Δ (A[γ]) ≅ Sub Δ (Γ▹A) : p∘_, q[_]
  p   : Sub (Γ▹A) Γ
  q   : Tm  (Γ▹A) (A[p])         <- zero De Bruijn index 0:=q, 1:=q[p], 2:=q[p][p], ...
        q[γ,a] = a
        p∘(γ,a) = γ
        (p∘γa,q[γa]) = γa

  Sub Δ (◆▹A▹B▹C)  ≅  (a: Tm Δ (A[ε])) × (b:Tm Δ (B[ε,a])) × Tm Δ (C[ε,a,b])
    B     : Ty (◆▹A)
    B[?]  : Ty Δ
    (ε,a) :

  HOAS (logical framework, presheaf models, two-level type theory):
    Bool : Ty
    true, false : Tm Bool
    elimBool : (P:Tm Bool→Ty) → Tm (P true)→Tm (P false)→(b:Tm Bool)→Tm (P b)
    Boolβ₁ : elimBool P t f true = t
    Boolβ₂ : elimBool P t f false = f
  a CwF supports Bool: (this automatically comes from the HOAS description):
    Bool : ∀{Γ} → Ty Γ
    Bool[] : Bool[γ] = Bool
    true, false : Tm Γ Bool
    true[] : true[γ] = true, false[]
    elimBool : Ty (Γ▹Bool) → Tm Γ (P[id,true])→Tm Γ(P[id,false])→(b:Tm Γ Bool) →
               Tm Γ (P[id,b])
    elimBool[] ....

  a CwF has Π types HOAS:
    Π : (A : Ty) → (Tm A → Ty) → Ty
    lam : ((a:Tm A) → Tm (B a)) ≅ Tm (Π A B) : _$_
  Π : (A : Ty Γ) → Ty (Γ▹A) → Ty Γ
  Π[] : (Π A B)[γ] = Π (A[γ]) (B[γ^])
     _^ : (γ:Sub Δ Γ) → Sub (Δ▹A[γ]) (Γ▹A)
     γ^ := (γ∘p,q)
             A : Ty Γ
             -----------
             A[γ] : Ty Δ

  CwF paper: Peter Dybjer: Internal type theory
             Peter Dybjer: Categories with families, uni-typed, simply typed, dependently typed
             Altenkirch,Kaposi: T.t. in t.t. using QITs
  categorical alternatives to CwFs: contextual categories, display map category, CwA, natural models, clan, ... (comprehension categories, LCCC)


this was (algebraic) decription of t.t.

model = CwF with Bool and Π structures (above)
syntax := initial model (I)
interpretation, evaluation: morphism from the initial model to a particular model

t : I.Tm ◆ Bool
------------------------  canonicity (Bool)
t = true or t = false

t : Tm ◆ (Π A B)
----------------------- η law for function space
t = lam (t[p]$q)


in category theory, there is no notion of elementhood
  Γ is an object. what are its elements
  - generalised elements: (Δ:Con)×Sub Δ Γ
  - global elements of Γ (we have terminal object ◆): Sub ◆ Γ
    if there is no ◆, then: (γ∙ : (Δ:Con)→Sub Δ Γ) × (δ : Sub Θ Δ → γ∙ Θ = (γ∙ Δ)∘δ)
    - Bool : Ty ◆
    - Bool : ∀{Γ} → Ty Γ
      Bool[] : Bool[γ] = Bool

variants of presentation:
 - direct, interpretation function / displayed model / ordinary model (total space)
 - gluing (which uses the global section functor)
 - HOAS presentation (relative displayed model)
 - Artin gluing (Jon Sterling way)
 - low level (preterms)

(N : Set) × N × (N → N) = NAT-model

N := (n:ℕ)×(∀ m o . n+(m+o) = (n+m)+o)
z := (0, λ m o . refl_{m+o})
s := (λ(n,_). ...)

induction on ℕ = for every dependent NAT-model D (over I),
      there is a dependent morphism from I to D

(N∙ : ℕ→Set)× N∙(0) × (∀n . N∙ n → N∙(1+n)) = dependent NAT-model over I=(ℕ,0,_+1)

canonicity displayed model (over I) (logical predicate proof) :

Con∙ Γ := Sub ◆ Γ → Set            -- the logical predicate for contexts
Sub∙ {Δ} Δ∙ {Γ} Γ∙ γ := (δ : Sub ◆ Δ) → Δ∙ δ → Γ∙ (γ ∘ δ) -- fundamental theorem
                   ^ : Sub Δ Γ                            -- every Sub preserves the predicate
Ty∙ Γ∙ A := ∀γ → Γ∙ γ → Tm ◆ (A[γ]) → Set    -- the logical predicate for types
Tm∙ Γ∙ A∙ a := ∀ γ → (γ∙ : Γ∙ γ) → A∙ γ∙ (a[γ])

◆∙ ε := ⊤

Bool∙ {Γ} γ∙ t := (t = true or t = false)   -- the logical predicate at Bool
             ^ : Tm ◆ Bool

true∙ : Tm∙ Γ∙ Bool∙ true
      : ∀ γ → (γ∙ : Γ∙ γ) → Bool∙ γ∙ (true[γ])
      : ∀ γ → (γ∙ : Γ∙ γ) → (true[γ] = true or true[γ] = false)
true∙ γ γ∙ := inl true[]
      
HOMEWORK: elimBool∙ 

------------------

reaping the fruits after defining the dependent model:


t : I.Tm ◆ Bool
------------------------  canonicity (Bool)
⟦t⟧ : Tm∙ ⟦◆⟧ ⟦Bool⟧ t
    : (γ:Sub ◆ ◆) → (γ∙ : ⟦◆⟧ γ) → Bool∙ γ∙ (t[γ])
    : (γ:Sub ◆ ◆) → (γ∙ : ⊤) → (t[γ] = true or t[γ] = false)
    
⟦t⟧ id tt : (t[id] = true or t[id] = false)
⟦t⟧ id tt : (t = true or t = false)
