Brunerie: type-theoretic definition of ∞-groupoids.

1. Motivation
2. Equivalent definitions of sequences/streams
3. ∞-groupoids

1. In mathematics : we work with sets.
   Bool : Set.
   There are two isomorphisms Bool ≅ Bool : Set should not be a set, it should be a 1-groupoid:
         there are several different ways to identify two sets.

We want to talk about ∞-groupoids.
Option: work in Homotopy Type Theory, where the basic objects are ∞-groupoids.

But we also need to be able to define ∞-groupoids starting from just sets.

∞-groupoids are models for spaces.

Many models of ∞-groupoids:
- Topological spaces (with the Quillen model structure)

- Simplicial sets
- Cubical sets
- Brunerie's type-theoretic definition of ∞-groupoids : globular shape.
  Simon Henry: Brunerie's definition is a model of spaces.

  X₀ : Set
  X₁ : X₀ → X₀ → Set
  X₂ : {x₀ x₁ : X₀} (y₀ y₁ : X₁ x₀ x₁) → Set
  ...

  + operations (ways to compose shapes)
  e.g.    X₁ x y → X₁ y z → X₁ x z

  A globular set G is
    A set G₀ of points.
    for every x,y : G₀, a globular set (Id x y) of paths between x and y.

  Brunerie's definition : some type theory is used to define the shapes of all of the composition operations.
  The type theory: essentially type theory with one base type ⋆ + identity types.
      A globular set G is an ∞-groupoid if we have
      for every operation o of this type theory.
      Some operation Inputs(o, G) → Outputs(o, G).

      x : ⋆, (y : ⋆ , p : Id x y), (z : ⋆, q : Id y z) ⊢ (p · q) : Id x z
   ∼> x : G₀, (y : G₀ , p : (Id x y)₀), (z : G₀, q : (Id y z)₀) ⊢ (p · q) : (Id x z)₀

2. Take X : Set.

Sequences:
  x₀ : X
  x₁ : X
  x₂ : X
  ...

Direct definition:
       Seq := (ℕ → X)

Coinductive definition:
  A sequence S : Seq consists of
    a head   S.head : X
    a tail   S.tail : Seq

  A generalized sequence consists of:
    a set     T : Set
    a map     next : T → T
    a map     elem : T → X
    a point   t : T

 Generalized sequence (T, next, elem, t) ⇒ Sequence   seq (T,next,elem,t)
   elem t , elem (next t) , elem (next (next t)) , ...

   (T, next) : ℕ-coalgebra. (coalgebra for Y ↦ Y)
   (T, next, elem) : (coalgebra for Y ↦ Y × X)

 Sequence S ⇒ Generalized sequence (minimal way to construct a generalized sequence)
                T    := ℕ
                next := suc
                elem :  ℕ → X
                elem := S

                t    := zero

 Sequence S ⇒ Generalized sequence (universal way to construct a generalized sequence)
                T    := Seq
                next := (-).tail
                elem := (-).head

                t    := S

  If sequences are defined as the terminal coalgebra for (Y ↦ Y × X), this says
  that (Seq, (-).tail, (-).head) is a universal way to see sequences as
  generalized sequences.

  Instead we define:
          A sequence is a generalized sequence (T, next, elem, t) such that for every morphism of generalized sequence
            F : (T', next', elem', t') → (T, next, elem, t), such F is an isomorphism on elem,
            F is an isomorphism of generalized sequences.
         
          elem (F x) = elem x  for every x : T'

          (ℕ , suc, seq (T,next,elem,t), zero) → (T, next, elem, t)

3. Definition of ∞-groupoid

   Generalized ∞-groupoid:
     model C of type theory with identity types.
     a closed type ⋆ of C

   example of generalized ∞-groupoid.
   For every simplicial Kan complex K, (sSet, K) is a generalized ∞-groupoid.
   For every type A of HoTT, (HoTT, A) is a generalized ∞-groupoid.

   Reduced ∞-groupoid:
      A generalized ∞-groupoid (C, ⋆c) such that
      for every F : (D, ⋆d) → (C, ⋆c), such that F is an isomorphism on closed terms, F is an isomorphism.

      F iso on closed terms : for every closed type A of D, F : Tm_D ◆ A → Tm_C ◆ (F A) is bijective.
      (We specify the things that we want not to be inductively generated. Here: closed terms.)

   Underlying globular set G of (C, ⋆):
     G₀      := Tm_C ◆ ⋆
     G₁(x,y) := Tm_C ◆ (Id x y)
     ...

   Going from generalized ∞-groupoid to reduced ∞-groupoid.
   We use orthogonal factorization systems in the 1-category of generalized ∞-groupoids:
     class L of morphisms
     class R of morphisms
     such that for every   f : C → D
     there is a unique (up to iso) way to write f = l · r, l ∈ L, r ∈ R

   If we have a set L₀ of morphisms, the pair (L, R) where R are morphisms that are right orthogonal to L₀
                                                           L are morphisms that are left orthogonal to R
   is a OFS.

   If we pick L₀ carefully, then R = { F | F is an isomorphism on closed terms }.
   Given a generalized ∞-groupoid (C , ⋆),    0   →   (C , ⋆)
                                              l \     / r
                                                 Red(C,⋆)

  (C , ⋆)                                                 → (D , ⋆)
     ↓
  ({C extended freely with some new closed type}, ⋆)
  There is no morphism from ({C extended freely with some new closed type}, ⋆) to (D, ⋆)
