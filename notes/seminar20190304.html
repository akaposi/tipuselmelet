<!DOCTYPE html>
<html>
  <head>
    <title>Rust for Haskellers</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      .remark-slide-content { font-size: 36px }
      .remark-slide-content h1 { font-size: 64px }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }

      .column:first-of-type {float:left}
      .column:last-of-type {float:right}

      .split-50 .column:first-of-type {width: 50%}
      .split-50 .column:last-of-type {width: 50%}

      .codeslide {
        padding-top: 10px;
        padding-left: 10px;
        padding-right: 10px;
        padding-bottom: 10px;
      }

      .image-66 img {
        width: 66%;
      }
    </style>
  </head>
  <body>
    <textarea id="source">


# Rust for Haskellers

----

1. What is Rust

2. High-level comparison

3. Syntax comparison

4. Rust-specific things

---

# Basic facts

----

 * Originally by Graydon Hoare, ~2009

 * Backed by Mozilla

 * Long evolution, 1.0 in 2015

 * Time-based release every 6 weeks

 * Current version 1.33

---

# Motivation

----

 * Bring memory safety to systems programming

 * Co-designed with experimental browser engine: Servo

 * Spoken-unspoken: displace C++

 * Flipside: bring systems programming to mainstream

---

# One-sentence summary

----

<br>

.center[
<big><big>"C++, if it was designed by Haskellers"</big></big>
]

<br>

<small>(Actually, it was bootstrapped in OCaml)</small>

---

# Longer summary

----

 * Strict, imperative, curly-braces

 * Low-level control, high-level abstractions ("zero-cost abstractions")

 * Compiler-checked and -assisted manual memory management


---

# Both Haskell and Rust

----

 * Hindley-Milnerish type inference

 * Algebraic datatypes

 * Parametric polymorphism

 * Type classes, associated types

---

# Only Haskell

----

 * Global type inference

 * Higher-kinded types

 * Higher-rank types

 * GADTs, and so on

---

# Only Rust

----

 * Affine types ("linear", "move semantics")

 * Region types ("lifetimes")

---

# Guarantees

----

 * Both:
    * Type safety, typeclass canonicity ("coherence")

 * Only Haskell:
    * Purity, parametricity

 * Neither: Totality

---
class: split-50

# Glossary

----

.column[
**Haskell**

"Polymorphism"

"Type class"

`instance`

"Constraint"

Template Haskell
]
.column[
**Rust**

"Generics"

"Trait"

`impl`

"Bound"

Macros
]

---

class: center, middle

# <big><big>Syntax comparisons</big></big>

---

class: split-50, codeslide

.column[
**Haskell**
----
```haskell
fact :: Word64 -> Word64
fact 0 = 1
fact n = n * fact (n - 1)
```
]
.column[
**Rust**
----
```rust
fn fact(n: u64) -> u64 {
    // if-else is an expression,
    // like in Haskell!
    if n == 0 {
        1
    } else {
        n * fact(n - 1)
    }
}
```
]

---

class: split-50, codeslide

.column[
**Haskell**
----
```haskell
map :: (a -> b) -> Maybe a -> Maybe b
map f m = case m of
    Just a  -> Just (f a)
    Nothing -> Nothing

mapIsZero :: Maybe Int -> Maybe Bool
mapIsZero m = map (\n -> n == 0) m
```
]
.column[
**Rust**
----
```rust
// (ignore what "impl FnOnce" means for now)
fn map<A, B>(f: impl FnOnce(A) -> B,
             opt: Option<A>) -> Option<B> {
    match opt {
        Some(a) => Some(f(a)),
        None    => None
    }
}

fn map_is_zero(opt: Option<i64>)
                        -> Option<bool> {
    map(|n| n == 0, opt)
}
```
]

---

class: split-50, codeslide

.column[
**Haskell**
----
```haskell
data Size = Size {
    width :: Int,
    height :: Int
} deriving (Eq, Show)

area :: Size -> Int
area size = width size * height size

isEmpty :: Size -> Bool
isEmpty size = area size == 0

```
]
.column[
**Rust**
----
```rust
#[derive(PartialEq, Eq, Debug)]
struct Size {
    width: u64,
    height: u64
}

// `impl` blocks declare methods
// methods use method call syntax,
//   functions use function call syntax
// methods use type-directed name resolution,
//   functions use lexical name resolution
impl Size {
    // sugar for: `self: &Size`
    fn area(&self) -> u64 {
        // fields also use TDNR
        self.width * self.height
    }

    fn is_empty(&self) -> bool {
        self.area() == 0 // method call
    }
}
```
]

---

class: split-50, codeslide

.column[
**Haskell**
----
```haskell
printSize :: Size -> IO ()
printSize size = do
    putStrLn ("Width: " ++ show (width size))
    putStrLn ("Height: " ++ show (height size))
```
]
.column[
**Rust**
----
```rust
// Return type defaults to unit, `()`
// No effect tracking
fn print_size(size: &Size) {
    // This is a macro
    // Rust macros are nice and hygienic
    println!("Width: {}", size.width);
    println!("Height: {}", size.height);
}
```
]

---


class: split-50, codeslide

.column[
**Haskell**
----
```haskell
data List a
    = Nil
    | Cons a (List a)

instance Eq a => Eq (List a) where
    Nil == Nil = True
    Cons a l1 == Cons b l2
        = (a == b) && (l1 == l2)
    _ == _ = False
```
]
.column[
**Rust**
----
```rust
enum List<T> {
    Nil,
    Cons(T, Box<List<T>>)
    // Indirection must be inserted manually
    // Otherwise type would have infinite size
}

impl<T: PartialEq> PartialEq for List<T> {
    // implicit: `type Self = List<T>;`
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Nil, Nil) => true,
            (Cons(a, l1), Cons(b, l2))
                => (a == b) && (l1 == l2),
            (_, _) => false
        }
    }
    // `==` operator desugars to calling `eq()`
}
```
]

---

class: split-50, codeslide

.column[
**Haskell**
----
```haskell
class (Izé a, Bigyó a, Show (Tartozék a))
        => Dolog a where
    type Tartozék a
    getTartozék :: a -> Tartozék a

-- Haskell thinks of associated types
--   as type-level functions
```
]
.column[
**Rust**
----
```rust
// the first type parameter, `Self`, is implicit
// this is short for: `where Self: Izé + Bigyó`
trait Dolog: Izé + Bigyó {
    type Tartozék: Debug;
    fn get_tartozék(&self) -> &Self::Tartozék;
}

// Rust thinks of associated types
//   as type level fields/components
```
]

---

class: center, middle

# <big><big>More about Rust</big></big>

---

# Move semantics

------

 * Technically: Affine types

 * Loosely: "linear types"; umbrella term: "substructural types"

 * Values may not be freely copied

 * Affine types: they may still be freely dropped

 * Haskell: Indexed monads

---

# Move semantics

----

```rust
// the `Vec` is owned by the variable `vec`
let vec: Vec<bool> = ...;

f(vec); // ownership is given to `f`

g(vec); // error: use after move
```

---

# Don't Fear Mutation

----

```rust
fn append_len(mut vec: Vec<usize>) -> Vec<usize> {
    vec.push(vec.len());
    return vec;
}
```

This is "pure"!

The caller cannot observe that their `vec` has changed.

```rust
// `mut` is just a qualifier on a local variable name
// as a form of redundancy, to indicate that we intend
// to mutate that variable later

// `usize` means "pointer-sized unsigned integer"
```

---

# Destructors

----

 * At end of scope, if value was not moved, destructor runs

 * Frees memory **and other resources**

 * E.g.: closes file, unlocks mutex

 * More prompt than finalizers, more convenient and correct than `withResource`

---

# `Clone`

----

```rust
trait Clone {
    fn clone(&self) -> Self;
}

// Logically equivalent to: fn dup(self) -> (Self, Self);
```

 * `Vec<T>: Clone` whenever `T: Clone`

 * Makes a new allocation, clones all elements

 * Must be called manually

---

# `Copy`

----

 * `Copy: Clone` ("subtrait")

 * "Just a `memcpy()`", no user-code required

 * Happens automatically

 * Most primitive types: `bool`, `u32`, `Option<i64>`, `(f32, char)`, etc.

---

# References

----

 * `&T`: Shared reference

 * `&mut T`: Unique, mutable reference

 * May not outlive the `T`

 * Represented as plain pointer

---

# Shared references

----

  * `&T: Copy` for *all* `T`

  * "Shared" *because* there can be any number of copies

  * For normal types, means the `T` is immutable while the reference to it exists

```rust
// Does not need to mutate or consume the `Vec`!
fn len<T>(self: &Vec<T>) -> usize { ... }
```

---

# Mutable references

-----

 * `&mut T` is *never* `Clone`, for any `T`

 * Only one `&mut T` may exist to a value, XOR any number of `&T`, at a given time

```rust
// Mutates the `Vec`, but does not need to consume it
// Logically equivalent to `fn(Vec<T>) -> Vec<T>`!
fn push<T>(self: &mut Vec<T>) { ... }
```

---

# Borrowing

----

```rust
fn get_width(size: &Size) -> &u64 { &size.width }

fn get_width_mut(size: &mut Size) -> &mut u64 { &mut size.width }

fn lookup<T>(vec: &Vec<T>, index: usize) -> Option<&T> { ... }

fn lookup_mut<T>(vec: &mut Vec<T>, index: usize)
    -> Option<&mut T> { ... }

// Composable: `&mut Vec<Vec<Vec<Size>>>` -> `&mut u64`!
```

---

# Impure mutation

----

 * A.K.A. "shared mutability", "interior mutability"

 * Mutation through *shared* references

```rust
// `Cell<T>` is Rust's version of `IORef<T>`
struct Cell<T> { ... }
fn get<T: Copy>(cell: &Cell<T>) -> T { ... }
fn set<T>(cell: &Cell<T>, val: T) { ... }
fn replace<T>(cell: &Cell<T>, val: T) -> T { ... }

// Tradeoff: No borrowing!
// Not possible: `&Cell<T>` -> `&T`
// Interaction only through the above methods
```

---

class: middle, center

<small><small>Haskell only has Sharing+Borrowing (normal `data`),
and Sharing+Mutation (`IORef`, `STRef`, etc.)</small></small>

.image-66[
    ![ChooseTwo.svg]
]

[ChooseTwo.svg]: data:image/svg+xml;base64,PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7IiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgd2lkdGg9IjcyMnB4IiBoZWlnaHQ9IjY3MnB4IiB2ZXJzaW9uPSIxLjEiIGNvbnRlbnQ9IiZsdDtteGZpbGUgdXNlckFnZW50PSZxdW90O01vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NDsgcnY6NTcuMCkgR2Vja28vMjAxMDAxMDEgRmlyZWZveC81Ny4wJnF1b3Q7IHZlcnNpb249JnF1b3Q7OC41LjEmcXVvdDsgZWRpdG9yPSZxdW90O3d3dy5kcmF3LmlvJnF1b3Q7IHR5cGU9JnF1b3Q7Z29vZ2xlJnF1b3Q7Jmd0OyZsdDtkaWFncmFtIG5hbWU9JnF1b3Q7UGFnZS0xJnF1b3Q7IGlkPSZxdW90O2VmYjczOGE4LTdjODAtYzdmMi02ODJjLTU3OTAzMjdiODNjOCZxdW90OyZndDsxWm5MZHBzd0VJYWZobVY3aEdTd1dTWnVuQ3lhTHVyMnRGMnFNQVkxQXZuSXdwYytmU1VqYm9ZNGJodmJ3UXVIK1VmWCtXWkFKZzZacHR0N1NaZkpvNGlBT3hoRlc0ZDhjREFPeHE3K05zS3VFQ1lJRjBJc1dWUkliaTNNMlcrd0lySnF6aUpZdFJvcUliaGl5N1lZaWl5RFVMVTBLcVhZdEpzdEJHL1B1cVF4ZElSNVNIbFgvY1lpbFZqVlJhaDJQQUNMRXp2MXhMT09uelI4aXFYSU16dWZnOGxpL3luY0tTM0hzdTFYQ1kzRXBpR1JPNGRNcFJDcXVFcTNVK0FtdEdYWWluNnpaN3pWdWlWazZwUU9lT3lPb2dsMkYrSEVvMlFTdk1Pa0dHSk5lUTdsSHZZclZic3lPbm9JRFVJYnQ1dUVLWmd2YVdnOEc1MEtXa3RVeXJYbDZzdDlJQ0N5VnJWWHBJMVFwQ3kwMXlzbHhSTk1CUmR5UHdFaFBnbElWSGxLQkRvNnR3dkdlYU9sNzg5bXZtOTBrU21iUnhoYnU5RU83VDlhRjNxeFRKbTA5SXpaalpjTjRScWtnbTFEc3ZHN0I1R0NranZkeEhweDRCVmRkcVZ0Mlc3cXpCbVYrWkUwa3FZU3FjM1d1QnE3SnFZdkxMUVRBYm9ENDJjSVhwV2Y2Nk1Xdi9GVjhRMnIvRXp4WGJuOFJ1UXQ0UXM2K0Q3REF2VDJOQ0x6REROOU13V1M2Y2djY3RVYlZtMkFiVEtaeU9BQWdaVW9aM0dtelJETTJGb3c0V1A2Y1haakhTbUxJdjVjeHB5WUpnM0V4RHVDMk9nem1qSnVnRXozQTJBMHA5bEsvM21jdnc1MEQ3V2grNmlIZWc5MGZKWTdMdXBBLzNvN1pMckhDamhpVWgrNG1ERHpibUNseXE1Mm8rNWhBandBWDROWjdYbkFqNUIzUmZCK0IvdzhvWkpsOFpEcFg3dTJLMWJsZVdyVXJXMlhYSXp4dU9lQjdOUFVCTGY0SGpEckVUcWRkUzZaWGhKR24yRFRxZm4vaDA0T29JOUl0NjZyZzlyWm9lUHVIZjB4VjNSLzJ4c3U3YXRYOWhnZFZuWVA1SXRWTnZaZXF1dzBWd1BHL1hhSzI1dWNVTno0WXR5N1QrMDhXOUdGUmdqU0hOTUhTeHovTS9Fem5NeUllOFVqT1g3eHFWMk1NRmpVYjZpNEQyL3E1SExjZTE2V3pKQVR6SndBT3pmQkNYZ2JFZlU3a2Z0Q0U1SFd2MXM2SWVrSjNPblZnYnRSNmp2ZVRGNGhTbTdQSzZRb2hqSUpoRlNKaUVWRytWMnRObElTdFVNR1c2YStHL205WjYwZnBTZlRDMnU0akZuNmZvRlNPeHRwbWl0aFhnVlY4MzRVWXVuMC9IYnNUVjE3bHFuZkhwbTlIS2VpdDY1SEN1R0ZnNENpTWdaMXRGR1hyd1N1ejRYcjlncitncFkyNjdmOWUxL2pQeXJrN2c4PSZsdDsvZGlhZ3JhbSZndDsmbHQ7L214ZmlsZSZndDsiPjxkZWZzLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjUsMC41KSI+PGVsbGlwc2UgY3g9IjM2MSIgY3k9IjQ0NiIgcng9IjIyNSIgcnk9IjIyNSIgZmlsbC1vcGFjaXR5PSIwLjUiIGZpbGw9IiM2NmZmNjYiIHN0cm9rZT0iIzM2MzkzZCIgc3Ryb2tlLW9wYWNpdHk9IjAuNSIgc3Ryb2tlLXdpZHRoPSIyIiBwb2ludGVyLWV2ZW50cz0ibm9uZSIvPjxlbGxpcHNlIGN4PSIyMjYiIGN5PSIyMjYiIHJ4PSIyMjUiIHJ5PSIyMjUiIGZpbGwtb3BhY2l0eT0iMC41IiBmaWxsPSIjNjY2NmZmIiBzdHJva2U9IiMzNjM5M2QiIHN0cm9rZS1vcGFjaXR5PSIwLjUiIHN0cm9rZS13aWR0aD0iMiIgcG9pbnRlci1ldmVudHM9Im5vbmUiLz48ZWxsaXBzZSBjeD0iNDk2IiBjeT0iMjI2IiByeD0iMjI1IiByeT0iMjI1IiBmaWxsLW9wYWNpdHk9IjAuNSIgZmlsbD0iI2ZmNjY2NiIgc3Ryb2tlPSIjMzYzOTNkIiBzdHJva2Utb3BhY2l0eT0iMC41IiBzdHJva2Utd2lkdGg9IjIiIHBvaW50ZXItZXZlbnRzPSJub25lIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjc2LjUsNDk5LjUpIj48c3dpdGNoPjxmb3JlaWduT2JqZWN0IHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlOyIgcG9pbnRlci1ldmVudHM9ImFsbCIgd2lkdGg9IjE2OSIgaGVpZ2h0PSI4MiIgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTogaW5saW5lLWJsb2NrOyBmb250LXNpemU6IDM1cHg7IGZvbnQtZmFtaWx5OiBDb21pYyBTYW5zIE1TOyBjb2xvcjogcmdiKDAsIDAsIDApOyBsaW5lLWhlaWdodDogMS4yOyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB3aWR0aDogMTY5cHg7IHdoaXRlLXNwYWNlOiBub3JtYWw7IG92ZXJmbG93LXdyYXA6IG5vcm1hbDsgdGV4dC1hbGlnbjogY2VudGVyOyI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6aW5saW5lLWJsb2NrO3RleHQtYWxpZ246aW5oZXJpdDt0ZXh0LWRlY29yYXRpb246aW5oZXJpdDsiPkJvcnJvd2luZzwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSI4NSIgeT0iNTkiIGZpbGw9IiMwMDAwMDAiIHRleHQtYW5jaG9yPSJtaWRkbGUiIGZvbnQtc2l6ZT0iMzVweCIgZm9udC1mYW1pbHk9IkNvbWljIFNhbnMgTVMiPkJvcnJvd2luZzwvdGV4dD48L3N3aXRjaD48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzM3LjUsMzI3LjUpIj48c3dpdGNoPjxmb3JlaWduT2JqZWN0IHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlOyIgcG9pbnRlci1ldmVudHM9ImFsbCIgd2lkdGg9IjQ2IiBoZWlnaHQ9IjM2IiByZXF1aXJlZEZlYXR1cmVzPSJodHRwOi8vd3d3LnczLm9yZy9UUi9TVkcxMS9mZWF0dXJlI0V4dGVuc2liaWxpdHkiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMzJweDsgZm9udC1mYW1pbHk6IEhlbHZldGljYTsgY29sb3I6IHJnYigwLCAwLCAwKTsgbGluZS1oZWlnaHQ6IDEuMjsgdmVydGljYWwtYWxpZ246IHRvcDsgd2lkdGg6IDQ3cHg7IHdoaXRlLXNwYWNlOiBub3dyYXA7IG92ZXJmbG93LXdyYXA6IG5vcm1hbDsgZm9udC13ZWlnaHQ6IGJvbGQ7IHRleHQtYWxpZ246IGNlbnRlcjsiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OmlubGluZS1ibG9jazt0ZXh0LWFsaWduOmluaGVyaXQ7dGV4dC1kZWNvcmF0aW9uOmluaGVyaXQ7Ij5VQjwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSIyMyIgeT0iMzQiIGZpbGw9IiMwMDAwMDAiIHRleHQtYW5jaG9yPSJtaWRkbGUiIGZvbnQtc2l6ZT0iMzJweCIgZm9udC1mYW1pbHk9IkhlbHZldGljYSIgZm9udC13ZWlnaHQ9ImJvbGQiPlVCPC90ZXh0Pjwvc3dpdGNoPjwvZz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSg2MC41LDE2MC41KSI+PHN3aXRjaD48Zm9yZWlnbk9iamVjdCBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZTsiIHBvaW50ZXItZXZlbnRzPSJhbGwiIHdpZHRoPSIxMzAiIGhlaWdodD0iNDAiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGlubGluZS1ibG9jazsgZm9udC1zaXplOiAzNXB4OyBmb250LWZhbWlseTogQ29taWMgU2FucyBNUzsgY29sb3I6IHJnYigwLCAwLCAwKTsgbGluZS1oZWlnaHQ6IDEuMjsgdmVydGljYWwtYWxpZ246IHRvcDsgd2lkdGg6IDEzMXB4OyB3aGl0ZS1zcGFjZTogbm93cmFwOyBvdmVyZmxvdy13cmFwOiBub3JtYWw7IHRleHQtYWxpZ246IGNlbnRlcjsiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OmlubGluZS1ibG9jazt0ZXh0LWFsaWduOmluaGVyaXQ7dGV4dC1kZWNvcmF0aW9uOmluaGVyaXQ7Ij5TaGFyaW5nPC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjY1IiB5PSIzOCIgZmlsbD0iIzAwMDAwMCIgdGV4dC1hbmNob3I9Im1pZGRsZSIgZm9udC1zaXplPSIzNXB4IiBmb250LWZhbWlseT0iQ29taWMgU2FucyBNUyI+U2hhcmluZzwvdGV4dD48L3N3aXRjaD48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjI4LjUsMzUyLjUpIj48c3dpdGNoPjxmb3JlaWduT2JqZWN0IHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlOyIgcG9pbnRlci1ldmVudHM9ImFsbCIgd2lkdGg9IjI0IiBoZWlnaHQ9IjQ2IiByZXF1aXJlZEZlYXR1cmVzPSJodHRwOi8vd3d3LnczLm9yZy9UUi9TVkcxMS9mZWF0dXJlI0V4dGVuc2liaWxpdHkiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogNDBweDsgZm9udC1mYW1pbHk6IENvdXJpZXIgTmV3OyBjb2xvcjogcmdiKDAsIDAsIDApOyBsaW5lLWhlaWdodDogMS4yOyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB3aWR0aDogMjVweDsgd2hpdGUtc3BhY2U6IG5vd3JhcDsgb3ZlcmZsb3ctd3JhcDogbm9ybWFsOyBmb250LXdlaWdodDogYm9sZDsgdGV4dC1hbGlnbjogY2VudGVyOyI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6aW5saW5lLWJsb2NrO3RleHQtYWxpZ246aW5oZXJpdDt0ZXh0LWRlY29yYXRpb246aW5oZXJpdDsiPiZhbXA7PC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjEyIiB5PSI0MyIgZmlsbD0iIzAwMDAwMCIgdGV4dC1hbmNob3I9Im1pZGRsZSIgZm9udC1zaXplPSI0MHB4IiBmb250LWZhbWlseT0iQ291cmllciBOZXciIGZvbnQtd2VpZ2h0PSJib2xkIj5bTm90IHN1cHBvcnRlZCBieSB2aWV3ZXJdPC90ZXh0Pjwvc3dpdGNoPjwvZz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSg1MjkuNSwxNjUuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OnZpc2libGU7IiBwb2ludGVyLWV2ZW50cz0iYWxsIiB3aWR0aD0iMTUzIiBoZWlnaHQ9IjQwIiByZXF1aXJlZEZlYXR1cmVzPSJodHRwOi8vd3d3LnczLm9yZy9UUi9TVkcxMS9mZWF0dXJlI0V4dGVuc2liaWxpdHkiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMzVweDsgZm9udC1mYW1pbHk6IENvbWljIFNhbnMgTVM7IGNvbG9yOiByZ2IoMCwgMCwgMCk7IGxpbmUtaGVpZ2h0OiAxLjI7IHZlcnRpY2FsLWFsaWduOiB0b3A7IHdpZHRoOiAxNTRweDsgd2hpdGUtc3BhY2U6IG5vd3JhcDsgb3ZlcmZsb3ctd3JhcDogbm9ybWFsOyB0ZXh0LWFsaWduOiBjZW50ZXI7Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTppbmxpbmUtYmxvY2s7dGV4dC1hbGlnbjppbmhlcml0O3RleHQtZGVjb3JhdGlvbjppbmhlcml0OyI+TXV0YXRpb248L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iNzciIHk9IjM4IiBmaWxsPSIjMDAwMDAwIiB0ZXh0LWFuY2hvcj0ibWlkZGxlIiBmb250LXNpemU9IjM1cHgiIGZvbnQtZmFtaWx5PSJDb21pYyBTYW5zIE1TIj5NdXRhdGlvbjwvdGV4dD48L3N3aXRjaD48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNDMyLjUsMzUyLjUpIj48c3dpdGNoPjxmb3JlaWduT2JqZWN0IHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlOyIgcG9pbnRlci1ldmVudHM9ImFsbCIgd2lkdGg9Ijk2IiBoZWlnaHQ9IjQ2IiByZXF1aXJlZEZlYXR1cmVzPSJodHRwOi8vd3d3LnczLm9yZy9UUi9TVkcxMS9mZWF0dXJlI0V4dGVuc2liaWxpdHkiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogNDBweDsgZm9udC1mYW1pbHk6IENvdXJpZXIgTmV3OyBjb2xvcjogcmdiKDAsIDAsIDApOyBsaW5lLWhlaWdodDogMS4yOyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB3aWR0aDogOTdweDsgd2hpdGUtc3BhY2U6IG5vd3JhcDsgb3ZlcmZsb3ctd3JhcDogbm9ybWFsOyBmb250LXdlaWdodDogYm9sZDsgdGV4dC1hbGlnbjogY2VudGVyOyI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6aW5saW5lLWJsb2NrO3RleHQtYWxpZ246aW5oZXJpdDt0ZXh0LWRlY29yYXRpb246aW5oZXJpdDsiPiZhbXA7bXV0PC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjQ4IiB5PSI0MyIgZmlsbD0iIzAwMDAwMCIgdGV4dC1hbmNob3I9Im1pZGRsZSIgZm9udC1zaXplPSI0MHB4IiBmb250LWZhbWlseT0iQ291cmllciBOZXciIGZvbnQtd2VpZ2h0PSJib2xkIj4mYW1wO211dDwvdGV4dD48L3N3aXRjaD48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjg4LjUsMjM5LjUpIj48c3dpdGNoPjxmb3JlaWduT2JqZWN0IHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlOyIgcG9pbnRlci1ldmVudHM9ImFsbCIgd2lkdGg9IjE0NCIgaGVpZ2h0PSIyMiIgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTogaW5saW5lLWJsb2NrOyBmb250LXNpemU6IDIwcHg7IGZvbnQtZmFtaWx5OiBDb3VyaWVyIE5ldzsgY29sb3I6IHJnYigwLCAwLCAwKTsgbGluZS1oZWlnaHQ6IDEuMjsgdmVydGljYWwtYWxpZ246IHRvcDsgd2lkdGg6IDE0NXB4OyB3aGl0ZS1zcGFjZTogbm93cmFwOyBvdmVyZmxvdy13cmFwOiBub3JtYWw7IHRleHQtYWxpZ246IGNlbnRlcjsiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OmlubGluZS1ibG9jazt0ZXh0LWFsaWduOmluaGVyaXQ7dGV4dC1kZWNvcmF0aW9uOmluaGVyaXQ7Ij51bnNhZmVDb2VyY2U8L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iNzIiIHk9IjIxIiBmaWxsPSIjMDAwMDAwIiB0ZXh0LWFuY2hvcj0ibWlkZGxlIiBmb250LXNpemU9IjIwcHgiIGZvbnQtZmFtaWx5PSJDb3VyaWVyIE5ldyI+dW5zYWZlQ29lcmNlPC90ZXh0Pjwvc3dpdGNoPjwvZz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzMDAuNSwxNDcuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OnZpc2libGU7IiBwb2ludGVyLWV2ZW50cz0iYWxsIiB3aWR0aD0iMTIwIiBoZWlnaHQ9IjQ2IiByZXF1aXJlZEZlYXR1cmVzPSJodHRwOi8vd3d3LnczLm9yZy9UUi9TVkcxMS9mZWF0dXJlI0V4dGVuc2liaWxpdHkiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogNDBweDsgZm9udC1mYW1pbHk6IENvdXJpZXIgTmV3OyBjb2xvcjogcmdiKDAsIDAsIDApOyBsaW5lLWhlaWdodDogMS4yOyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyB3aWR0aDogMTIxcHg7IHdoaXRlLXNwYWNlOiBub3dyYXA7IG92ZXJmbG93LXdyYXA6IG5vcm1hbDsgZm9udC13ZWlnaHQ6IGJvbGQ7IHRleHQtYWxpZ246IGNlbnRlcjsiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OmlubGluZS1ibG9jazt0ZXh0LWFsaWduOmluaGVyaXQ7dGV4dC1kZWNvcmF0aW9uOmluaGVyaXQ7Ij4mYW1wO0NlbGw8L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iNjAiIHk9IjQzIiBmaWxsPSIjMDAwMDAwIiB0ZXh0LWFuY2hvcj0ibWlkZGxlIiBmb250LXNpemU9IjQwcHgiIGZvbnQtZmFtaWx5PSJDb3VyaWVyIE5ldyIgZm9udC13ZWlnaHQ9ImJvbGQiPiZhbXA7Q2VsbDwvdGV4dD48L3N3aXRjaD48L2c+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzQyLjUsMjU3LjUpIj48c3dpdGNoPjxmb3JlaWduT2JqZWN0IHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlOyIgcG9pbnRlci1ldmVudHM9ImFsbCIgd2lkdGg9IjYwIiBoZWlnaHQ9IjcwIiByZXF1aXJlZEZlYXR1cmVzPSJodHRwOi8vd3d3LnczLm9yZy9UUi9TVkcxMS9mZWF0dXJlI0V4dGVuc2liaWxpdHkiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogNjBweDsgZm9udC1mYW1pbHk6IFRhaG9tYTsgY29sb3I6IHJnYigwLCAwLCAwKTsgbGluZS1oZWlnaHQ6IDEuMjsgdmVydGljYWwtYWxpZ246IHRvcDsgd2hpdGUtc3BhY2U6IG5vd3JhcDsiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OmlubGluZS1ibG9jazt0ZXh0LWFsaWduOmluaGVyaXQ7dGV4dC1kZWNvcmF0aW9uOmluaGVyaXQ7Ij7wn5KpPC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjMwIiB5PSI2NSIgZmlsbD0iIzAwMDAwMCIgdGV4dC1hbmNob3I9Im1pZGRsZSIgZm9udC1zaXplPSI2MHB4IiBmb250LWZhbWlseT0iVGFob21hIj7wn5KpPC90ZXh0Pjwvc3dpdGNoPjwvZz48cGF0aCBkPSJNIDQ4MSAzODYgTCA0ODEgMzg2IiBmaWxsPSJub25lIiBzdHJva2U9IiMwMDAwMDAiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9Im5vbmUiLz48cGF0aCBkPSJNIDQ4MSAzODYgTCA0ODEgMzg2IEwgNDgxIDM4NiBMIDQ4MSAzODYgWiIgZmlsbD0iIzAwMDAwMCIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJub25lIi8+PC9nPjwvc3ZnPgo=

<small><small>`&mut` is Rust's raison d'etre.</small></small>

---

# Lifetimes

----

```rust
// These are equivalent:
fn get_width(size: &Size) -> &u64
fn get_width<'a>(size: &'a Size) -> &'a u64
```

```rust
// So are these:
fn foo(arg1: &String, arg2: &Vec<bool>)
fn foo<'a, 'b>(arg1: &'a String, arg2: &'b Vec<bool>)
```

This is called "lifetime elision".

```rust
// & and &mut can be thought of as having kind:
//     Lifetime -> Type -> Type
// Normally written e.g. Ref<'a, T>, but these have special syntax.
```

---

# Lifetimes

----

 * Similar to Haskell `ST` monad

 * More ergonomic (elision, ...), more capable (borrowing!)

 * Technical difference:

    * "Permission-in-reference", versus "permission-in-`ST`"


---

# Same checks avoid data races

----

 * Single-threaded analogue of concurrency: reentrancy

 * `Mutex<T>`: gives `&mut T` from `&Mutex<T>`; but only one at a time

 * Locking mutex twice in same thread: deadlock. Reentrancy!

 * Single-threaded mutex: `RefCell<T>`. Used for graphs.

---

# Function traits

----

```rust
// Lambdas desugar to an anonymous `struct` plus `impl`s of these
// "Closure conversion reified into the source language"

// Lambda may move from environment => may only be called once
trait FnOnce<Args> {
    type Ret;
    fn call_once(self, Args) -> Self::Ret;
}

// May mutate environment => requires unique reference to invoke
trait FnMut<Args>: FnOnce<Args> {
    fn call_mut(&mut self, Args) -> Self::Ret;
}

// Only reads from environment => may be shared
trait Fn<Args>: FnMut<Args> {
    fn call(&self, Args) -> Self::Ret;
}

// Inheritance order seems "backwards". Because of contravariance!
// Try it: `call_once` can be implemented using `call_mut`,
// and in turn `call_mut` can be implemented using `call`.

```

    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>
