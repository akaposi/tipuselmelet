"The groupoid interpretation of type theory" 1996

List of interesting axioms/principles:
- LEM:     Law of Excluded Middle
- AC:      Axiom of Choice
- UC:      Axiom of Unique Choice
- funext:  Function Extensionality

  f, g : Π A B
  Id_{Π A B} f g ≃ (Π (a : A) → Id (f a) (g a))

- UIP:     Uniqueness of Identity Proofs

  UIP : ∀ X → ((x,y : X)(p,q : Id x y) → Id p q)
  "every type is a set"

- propext: Propositional Extensionality

  Prop = (X : U) × ((x,y : X) → Id x y)

  A, B : Prop
  Id_{Prop} A B ≃ (A ↔ B)

- Univalence axiom for sets

  Set = (X : U) × ((x,y : X)(p,q : Id x y) → Id p q)

  A, B : Set
  Id_{Set} A B ≃ Iso A B

- UA:      Univalence Axiom

  A, B : U
  Id_{U} A B ≃ Equiv A B

# Set model = Standard model
Satisfies UIP
Satisfies LEM, AC, UC, funext, propext iff the metatheory does
Does not satisfy UA

# Presheaf model
Does not satisfy LEM, AC

# Setoid model
Satisfies UIP, funext, propext, (? UC)
Does not satisfy UA

A context is a setoid.
A type is a setoid fibration (fibrant dependent setoids).

# Groupoid model
Satisfies funext, propext
Refutes UIP

A context is a groupoid.
A type is a groupoid fibration.

# Cubical set model (and the other ∞-groupoid models)
Satisfies funext, propext, UA
Refutes UIP

A context is a cubical set.
A type is a cubical Kan fibration.


## Setoid model
A setoid A consists of:
  - A set `A₀` of objects    A₀  : Set
  - A binary relation `A₁`   A₁  : A₀ → A₀ → Prop
  - Reflexivity              _   : {x} → A₁ x x
  - Symmetry                 _   : {x,y} → A₁ x y → A₁ y x
  - Transitivity             _   : {x,y,z} → A₁ x y → A₁ y z → A₁ x z

A dependent setoid B over A consists of:
  - A family `B₀` of objects  B₀  : A₀ → Set
  - A binary relation `B₁`    B₁  : {x,y} → A₁ x y → B₀ x → B₀ y → Prop
  - Dependent reflexivity     _   : {x}{x' : B₀ x} → B₁ _ x' x'
  - Dependent symmetry        _   : {x,y}{x',y'} → B₁ _ x' y' → B₁ _ y' x'
  - Dependent transitivity    _   : {x,y,z}{x',y',z'} → B₁ _ x' y' → B₁ _ y' z' → B₁ _ x' z'

A dependent setoid B over A is fibrant if:
  for every x, y : A₀, such that (A₁ x y), and x' : B₀ x,
   we have y' : B₀ y, s.t. (B₁ _ x' y').

  x'                     x' ~  y'

  |                      |     |

  x  ~  y                x  ~  y

Given a setoid A, and x,y : A₀
  (Id A x y)₀ = A₁ x y
  (Id A x y)₁ = ⊤

Given X,Y : Setoid
 We define (X ⇒ Y) : Setoid
 (X ⇒ Y)₀             = (f : X₀ → Y₀) × (∀ (x₀ x₁ : X),  X₁ x₀ x₁ → Y₁ (f x₀) (f x₁))
 (X ⇒ Y)₁ (f,_) (g,_) = ∀ (x : X), Y₁ (f x) (g x)
 Funext holds in the setoid model

## Groupoid model
A groupoid A consists of:
  - A set `A₀` of objects         A₀  : Set
  - Sets `A₁ x y` of morphisms    A₁  : A₀ → A₀ → Set
  - Identities                    id  : {x} → A₁ x x
  - Inverses                      inv : {x,y} → A₁ x y → A₁ y x
  - Compositions                  _·_ : {x,y,z} → A₁ x y → A₁ y z → A₁ x z,
satisfying the equations
  - Associativity                 (f · g) · h = f · (g · h)
  - Inverse laws                  f · inv f = id, inv f · f = id
  - Identity laws                 id · f = f = f ̇ id

A dependent groupoid B over A consists of:
  - A family `B₀` of objects         B₀  : A₀ → Set
  - Families `B₁ x y` of morphisms   B₁  : {x,y : A₀} → A₁ x y → B₀ x → B₀ y → Set
  - ...

A dependent groupoid B over A is fibrant if:
  for every x, y : A₀, such that (f : A₁ x y), and x' : B₀ x,
   we have y' : B₀ y, s.t. (B₁ f x' y').

Given a groupoid A, and x,y : A₀
  (Id A x y)₀     = A₁ x y
  (Id A x y)₁ p q = (p ≡ q)

  We can compute
  (Id (Id A x y) p q)₀     = (p ≡ q)
  (Id (Id A x y) p q)₁ α β = ⊤

UIP: (Id (Id A x y) p q) is always inhabited.

(ℤ/2ℤ)₀     = {∙}
(ℤ/2ℤ)₁ ∙ ∙ = {0,1}
 `Id (Id (ℤ/2ℤ) ∙ ∙) 0 1` is not inhabited.
 UIP does not hold in the groupoid model.

A groupoid A is a set if it is a setoid.
                      if (A₁ x y) is a proposition.

Universe of sets in the groupoid model:
  Set      : Groupoid
  Set₀     = Setoid
  Set₁ A B = Iso A B

We need to check Set ≃ (X : U) × ((x,y : X)(p,q : Id x y) → Id p q)
where
  U      : Groupoid
  U₀     = Groupoid
  U₁ A B = Iso ∥A∥₀ ∥B∥₀

We also have an ill-behaved universe
  V      : Groupoid
  V₀     = Groupoid
  V₁ A B = (A ≡ B)

We do not have (U ≃ V)
We have V → U

## Truncated cubical set model
A 2-cubical set A consists of:
  - A₀ : Set
  - A₁ : (x₀ : A₀) → (x₁ : A₀) → Set
  - A₂ : (x₀₀ x₀₁ x₁₀ x₁₁ : A₀)
         (x₀₂ : A₁ x₀₀ x₀₁)
         (x₁₂ : A₁ x₁₀ x₁₁)
         (x₂₀ : A₁ x₀₀ x₁₀)
         (x₂₁ : A₁ x₀₁ x₁₁)
       → Set
  - Aₙ depends on 3^n-1 elements of Aᵢ for i<n.

         x₀₂
   x₀₀ ------- x₀₁
    |           |
x₂₀ |           | x₂₁
    |           |
   x₁₀ -------  x₁₁
         x₁₂

  - Degeneracies
      d₀ : (x : A₀) → A₁ x x

    horizontal degeneracy for squares:

         d₀
   x₀₀ ------- x₀₀
    |           |
x₂₀ |           | x₂₀
    |           |
   x₁₀ -------  x₁₀
         d₀

    vertical degeneracy for squares:

         x₀₂
   x₀₀ ------- x₀₁
    |           |
 d₀ |           | d₀
    |           |
   x₀₀ -------  x₀₁
         x₀₂

We have a notion of dependent cubical set

A cubical set A is fibrant if:
Given the data of:

   x₀₀         x₀₁
    |           |
x₂₀ |           | x₂₁
    |           |
   x₁₀ -------  x₁₁
         x₁₂

we have
          ∙
   x₀₀ -------- x₀₁
    |           |
x₂₀ |     ∙     | x₂₁
    |           |
   x₁₀ -------  x₁₁
         x₁₂

-> generalization to any dimension, any missing face.

-- Composition of   f : A₁ x y and g : A₁ y z

We construct this open box
  x       z
  |       |
 f|       | id
  v       v
  y------>z
      g

If A is fibrant, we obtain
    (f·g)
  x ----- z
  |       |
  |   ∙   |
  |       |
  y-------z

Other constructions:
      f
  x------>y
  |       |
id|       | g
  v       v
  x       z


  x       z
  |       |
id|       | inv g
  v       v
  x------>y
      f

-- Inverse of f : A₁ x y

    f
 x --> y
 |
 v
 x --> x

# Cubical sets are presheaves over the category □.
  □₀     = ℕ

  [n] = {0,1,...,n-1}

  □₁ n m = [n] → ([m] + {0,1})
           injective on the part from [n] to [m]

   . -> j
 i |
   v
         x₀₂
   x₀₀ ------- x₀₁
    |           |
x₂₀ |    x₂₂    | x₂₁
    |           |
   x₁₀ -------  x₁₁
         x₁₂

  n = {i,j}, m = {k}
                       x₂₂[{i -> 0, j -> k}] = x₀₂

  We have a category of presheaves Psh(□)
  We have an interval
    𝕀 : PshTy(□)
  We can use this 𝕀 to work with cubical sets in the language of Psh(□)

