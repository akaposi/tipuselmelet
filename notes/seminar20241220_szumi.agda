{-# OPTIONS --cubical --no-import-sorts #-}

module seminar20241220_szumi where

open import Cubical.Foundations.Prelude

postulate A : Type

_≡′_ : {A : Type} → A → A → Type
_≡′_ = _≡_
-- (x ≡ y) ≅ (f : I → A) where f i0 = x and f i1 = y

refl′ : {x : A} → x ≡ x
refl′ {x = x} i = x

sym′ : {x y : A} → x ≡ y → y ≡ x
sym′ p i = p (~ i)

cong′ : {A B : Type} (f : A → B) {x y : A} → x ≡ y → f x ≡ f y
cong′ f p i = f (p i)

cong₂′ :
  {A B C : Type} (f : A → B → C)
  {x y : A} {z w : B} → x ≡ y → z ≡ w → f x z ≡ f y w
cong₂′ f p q i = f (p i) (q i)

funext′ : {A B : Type} {f g : A → B} → ((x : A) → f x ≡ g x) → f ≡ g
funext′ p i x = p x i

_∙′_ : {x y z : A} → x ≡ y → y ≡ z → x ≡ z
_∙′_ {x = x} p q i =
  hcomp
    (λ j → λ where
      (i = i0) → x
      (i = i1) → q j)
    (p i)

-- _∙∙_∙∙_
trans3 : {x y z w : A} → x ≡ y → y ≡ z → z ≡ w → x ≡ w
trans3 p q r i =
  hcomp
    (λ j → λ where
      (i = i0) → p (~ j)
      (i = i1) → r j)
    (q i)

PathP′ : (A : I → Type) → A i0 → A i1 → Type
PathP′ = PathP
-- PathP (λ i → A i) x y ≅ (f : (i : I) → A i) where f i0 = x and f i1 = y

PathP″ : {A B : Type} → A ≡ B → A → B → Type
PathP″ p = PathP (λ i → p i)

symP′ :
  {A : I → Type} {x : A i0} {y : A i1} →
  PathP (λ i → A i) x y → PathP (λ i → A (~ i)) y x
symP′ p i = p (~ i)

Square′ :
  {a00 a01 a10 a11 : A} → a00 ≡ a01 → a10 ≡ a11 → a00 ≡ a10 → a01 ≡ a11 → Type
Square′ p q r s = PathP (λ i → r i ≡ s i) p q

module _ {x y : A} (p : x ≡ y) where
  drop-i : Square p p refl refl
  drop-i i j = p j

  drop-j : Square refl refl p p
  drop-j i j = p i

  ∧-conn : Square refl p refl p
  ∧-conn i j = p (i ∧ j)

  ∨-conn : Square p refl p refl
  ∨-conn i j = p (i ∨ j)

module _
  {a00 a01 a10 a11 : A}
  {p : a00 ≡ a01} {q : a10 ≡ a11} {r : a00 ≡ a10} {s : a01 ≡ a11}
  (sq : Square p q r s)
  where
  flip-i : Square q p (sym r) (sym s)
  flip-i i j = sq (~ i) j

  flip-j : Square (sym p) (sym q) s r
  flip-j i j = sq i (~ j)

  transpose : Square r s p q
  transpose i j = sq j i

  rotate : Square s r (sym p) (sym q)
  rotate i j = sq j (~ i)

module _ {x y z : A} (p : x ≡ y) (q : y ≡ x) where
  -- invSides-filler
  sqrefl : Square p q p q
  sqrefl i j =
    hcomp
      (λ k → λ where
        (i = i0) → p (j ∨ ~ k)
        (i = i1) → q (j ∧ k)
        (j = i0) → p (i ∨ ~ k)
        (j = i1) → q (i ∧ k))
      y

module _
  {a000 a001 a010 a011 a100 a101 a110 a111 : A}
  {a00x : a000 ≡ a001} {a01x : a010 ≡ a011} {a10x : a100 ≡ a101} {a11x : a110 ≡ a111}
  {a0x0 : a000 ≡ a010} {a0x1 : a001 ≡ a011} {a1x0 : a100 ≡ a110} {a1x1 : a101 ≡ a111}
  {ax00 : a000 ≡ a100} {ax01 : a001 ≡ a101} {ax10 : a010 ≡ a110} {ax11 : a011 ≡ a111}
  (a0xx : Square a00x a01x a0x0 a0x1) (a1xx : Square a10x a11x a1x0 a1x1)
  (ax0x : Square a00x a10x ax00 ax01) (ax1x : Square a01x a11x ax10 ax11)
  (axx0 : Square a0x0 a1x0 ax00 ax10)
  where
  sqcomp : Square a0x1 a1x1 ax01 ax11
  sqcomp i j =
    hcomp
      (λ k → λ where
        (i = i0) → a0xx j k
        (i = i1) → a1xx j k
        (j = i0) → ax0x i k
        (j = i1) → ax1x i k)
      (axx0 i j)

Triangle : {x y z : A} → x ≡ y → y ≡ z → x ≡ z → Type
Triangle p q r = Square p r refl q

module _ {x y z : A} (p : x ≡ y) (q : y ≡ z) (r : x ≡ z) where
  -- slideSquare
  slideTriangle : Square p r refl q → Square p refl r q
  slideTriangle sq i j =
    hcomp
      (λ k → λ where
        (i = i0) → p j
        (i = i1) → r (j ∨ k)
        (j = i0) → r (i ∧ k)
        (j = i1) → q i)
      (sq i j)

module _ {x y : A} (p : x ≡ y) where
  -- lUnit
  ∙-idl : refl ∙ p ≡ p -- Square (refl ∙ p) p refl refl
  ∙-idl i j =
    hcomp
      (λ k → λ where
        (i = i0) → compPath-filler refl p k j
        (i = i1) → p (j ∧ k)
        (j = i0) → x
        (j = i1) → p k)
      x

-- (p ∙ q) ∙ r ≡ p ∙ (q ∙ r)
-- p ∙ sym p ≡ refl
-- cong f (p ∙ q) ≡ cong f p ∙ cong f q

module _ {x y z w : A} (p : x ≡ y) (q : y ≡ z) (r : z ≡ w) where
  -- doubleCompPath-filler
  trans3-filler : Square q (trans3 p q r) (sym p) r
  trans3-filler j i =
    hfill
      (λ j → λ where
        (i = i0) → p (~ j)
        (i = i1) → r j)
      (inS (q i))
      j

module _ {x y z : A} (p : x ≡ y) (q : y ≡ z) where
  compPath-filler′ : Square p (p ∙ q) refl q
  compPath-filler′ j i =
    hfill
      (λ j → λ where
        (i = i0) → x
        (i = i1) → q j)
      (inS (p i))
      j

  compPath-filler'′ : Square q (p ∙ q) (sym p) refl
  compPath-filler'′ j i = slideTriangle p q (p ∙ q) compPath-filler′ i (~ j)

  -- Square refl (p ∙ q) (sym p) q

module _
  {x y z w : A} (p : x ≡ y) (q : y ≡ z) (r : x ≡ w) (s : w ≡ z)
  where
  Square→compPath : (sq : Square p s r q) → p ∙ q ≡ r ∙ s
  Square→compPath sq i j =
    hcomp
      (λ k → λ where
        (i = i0) → compPath-filler p q k j
        (i = i1) → compPath-filler' r s k j
        (j = i0) → r (i ∧ ~ k)
        (j = i1) → q (i ∨ k))
      (sq i j)

  -- p ∙ q ≡ r ∙ q → Square p s r q

Pentagon : {x y z u v : A} → x ≡ y → y ≡ z → x ≡ u → u ≡ v → v ≡ z → Type
Pentagon p q r s t = Square p t (r ∙ s) q

record MonoidalGroupoid : Type₁ where
  infixr 6 _⊗_
  field
    G : Type
    isGroupoidG : isGroupoid G

    _⊗_ : G → G → G
    assoc : (a b c : G) → (a ⊗ b) ⊗ c ≡ a ⊗ (b ⊗ c)
    pentagon :
      (a b c d : G) →
      Square
        (cong (_⊗ d) (assoc a b c))
        (assoc a b (c ⊗ d))
        (assoc (a ⊗ b) c d)
        (assoc a (b ⊗ c) d ∙ cong (a ⊗_) (assoc b c d))

    ε : G
    idl : (a : G) → ε ⊗ a ≡ a
    idr : (a : G) → a ⊗ ε ≡ a
    triangle :
      (a b : G) →
      Square (cong (_⊗ b) (idr a)) (cong (a ⊗_) (idl b)) (assoc a ε b) refl

  triangle′ :
    (a b : G) → Square (cong (_⊗ b) (idl a)) (idl (a ⊗ b)) (assoc ε a b) refl
  triangle′ a b i j =
    hcomp
      (λ k → λ where
        (i = i0) → idl (idl a j ⊗ b) k
        (i = i1) → idl (idl (a ⊗ b) j) k
        (j = i0) → idl (assoc ε a b i) k
        (j = i1) → idl (a ⊗ b) k)
      (hcomp
        (λ k → λ where
          (i = i0) → assoc ε (idl a j) b k
          (i = i1) → ε ⊗ idl (a ⊗ b) j
          (j = i0) →
            compPath-filler'
              (assoc ε (ε ⊗ a) b)
              (cong (ε ⊗_) (assoc ε a b))
              (~ k) i
          (j = i1) → assoc ε a b (i ∨ k))
        (hcomp
          (λ k → λ where
            (i = i0) → triangle ε a k j ⊗ b
            (i = i1) → triangle ε (a ⊗ b) k j
            (j = i0) → pentagon ε ε a b i k
            (j = i1) → assoc ε a b i)
          (assoc (idr ε j) a b i)))

  -- idl ε ≡ idr ε

  -- (f g h i : ε ≡ ε) → (f ∙ g) ⊗ (h ∙ i) ≡ (f ⊗ h) ∙ (g ⊗ i)
  -- (f g : ε ≡ ε) → Square (cong₂ _⊗_ f g) (f ∙ g) (idl ε) (idr ε)
  -- (f g : ε ≡ ε) → f ∙ g ≡ g ∙ f

module _
  {A B C : Type} {x : A} {y : B} {z : C}
  (p : A ≡ B) (q : B ≡ C)
  (r : PathP (λ i → p i) x y) (s : PathP (λ i → q i) y z)
  where
  compPathP′ : PathP (λ i → (p ∙ q) i) x z
  compPathP′ i =
    comp (λ j → compPath-filler p q j i)
      (λ j → λ where
        (i = i0) → x
        (i = i1) → s j)
      (r i)

transport′ : {A B : Type} → A ≡ B → A → B
transport′ = transport

transport-filler′ :
  {A B : Type} (p : A ≡ B) (x : A) → PathP (λ i → p i) x (transport p x)
transport-filler′ = transport-filler

transportRefl′ : (x : A) → transport refl x ≡ x
transportRefl′ = transportRefl

subst′ : {x y : A} → (B : A → Type) → x ≡ y → B x → B y
subst′ B p = transport (cong B p)

module _ {x y z : A} (B : A → Type) (p : x ≡ y) (q : y ≡ z) where
  subst-∙ :
    (w : B x) →
    transport (cong B (p ∙ q)) w ≡ transport (cong B q) (transport (cong B p) w)
  subst-∙ w i =
    comp (λ j → B (compPath-filler' p q (~ i) j))
      (λ j → λ where
        (i = i0) → transport-filler (cong B (p ∙ q)) w j
        (i = i1) → transport-filler (cong B q) (transport (cong B p) w) j)
      (transport-filler (cong B p) w i)
