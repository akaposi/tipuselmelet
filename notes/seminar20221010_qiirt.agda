{-# OPTIONS --cubical #-}
open import Agda.Primitive
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
open import Cubical.Data.Nat renaming (_+_ to _+ℕ_)
open import Cubical.Data.Sum
open import Cubical.Data.Unit

module seminar20221010_qiirt where

module QIT where
  data ℤ : Set where
    zero : ℤ
    suc  : ℤ → ℤ
    pred : ℤ → ℤ
    predsuc : ∀ a → pred (suc a) ≡ a
    sucpred : ∀ a → suc (pred a) ≡ a
    isset   : isSet ℤ

  _+_ : ℤ → ℤ → ℤ
  zero + b = b
  suc a + b = suc (a + b)
  pred a + b = pred (a + b)
  predsuc a i + b = predsuc (a + b) i  -- e : pred (suc (a + b)) ≡ a + b             e i
  sucpred a i + b = sucpred (a + b) i
  isset a a₁ x y i i₁ + b = {!!} -- easy boilerplate

module Nf where
  ℤ = (ℕ ⊎ Unit) ⊎ ℕ

  _-_ : ℕ → ℕ → ℤ
  _-_ = {!!}

  _+_ : ℤ → ℤ → ℤ
  inl (inl a)  + inl (inl b)  = {!!}        -- -a-1  +  -b-1
  inl (inl a)  + inl (inr tt) = {!!}        -- -a-1  +  0
  inl (inl a)  + inr b        = {!!}        -- ...
  inl (inr tt) + inl (inl b)  = {!!}
  inl (inr tt) + inl (inr tt) = {!!}
  inl (inr tt) + inr b        = {!!}
  inr a        + inl (inl b)  = a - b
  inr a        + inl (inr tt) = inr a
  inr a        + inr b        = inr (a +ℕ b)

  -- predsuc, sucpred equations we can prove

  module I where
    Zero : ℤ
    Suc Pred : ℤ → ℤ
    Zero = inl (inr tt)
    Suc (inl (inl zero)) = inl (inr tt)   -- -1   -> 0
    Suc (inl (inl (suc n))) = inl (inl n) -- -n-1 -> -n
    Suc (inl (inr tt)) = inr 0 -- +1      -- 0    -> +1
    Suc (inr n) = inr (suc n)             -- +n+1 -> +n+2
    Pred (inl (inl n)) = inl (inl (suc n))
    Pred (inl (inr tt)) = inl (inl 0)
    Pred (inr zero) = inl (inr tt)
    Pred (inr (suc n)) = inr n

  record Alg : Set₁ where field
    Z    : Set
    Zero : Z
    Suc  : Z → Z
    Pred : Z → Z
    predsuc : ∀ a → Pred (Suc a) ≡ a
    sucpred : ∀ a → Suc (Pred a) ≡ a
  open Alg

  Int : Alg
  Alg.Z Int = ℤ
  Alg.Zero Int = I.Zero
  Alg.Suc Int = I.Suc
  Alg.Pred Int = I.Pred
  Alg.predsuc Int = {!!} 
  Alg.sucpred Int = {!!}

  _^_ : ∀{A : Set} → (A → A) → ℕ → (A → A)
  f ^ zero = λ a → a
  f ^ (suc n) = λ a → f ((f ^ n) a)

  recInt : (A : Alg) → ℤ → A .Z
  recInt A (inl (inl n))  = (A .Pred ^ suc n) (A .Zero)  -- -n-1
  recInt A (inl (inr tt)) = A .Zero
  recInt A (inr n)        = (A .Suc ^ suc n) (A .Zero)

  _nice+_ : ℤ → ℤ → ℤ
  i nice+ j = recInt
    (record
       { Z = ℤ → ℤ
       ; Zero = λ b → b
       ; Suc = λ f b → I.Suc (f b)
       ; Pred = λ f b → I.Pred (f b)
       ; predsuc = λ f → {!!}
       ; sucpred = {!!}
       })
    i j

  +5 : ℤ
  +5 = I.Suc (I.Suc (I.Suc (I.Suc (I.Suc (I.Zero)))))

  n = {!+5 nice+ +5!}

-- *defined* (PhD thesis of Li Nuo, "definable quotient")
-- integers: definable QIT, we don't get much from the definition (we can use QITs as well as the *defined* QIT)
-- syntax of first order logic: definable QITs, some equations in the *defined* QIT are definitional
-- syntax of simple type theory: definable QIT (very complicated)
-- syntax of type theories: we don't yet know whether they are definable, we don't really care, but we want *some*
--   equations to be definitional
--   -- with presyntax this is very natural
--   -- intrinsic syntax without quotienting (see MSc thesis of Andras)
--   -- Viktor Bense: formalisation of canonicity for simple type theory
--      -- very easy for a strict syntax (cheating, "shallow embedding" tric
--      -- very hard for a completely QIIT syntax (the proper way)
--      -- maybe there is a cheaper proper way: use QIIRT syntax (_[_] substitution is recursive, cubical Agda supports this)
--   remark: it would be nice to have all equations definitional (strict QITs)
--   -- semiring-solver
